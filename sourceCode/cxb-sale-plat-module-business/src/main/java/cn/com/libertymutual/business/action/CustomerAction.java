package cn.com.libertymutual.business.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.libertymutual.business.service.api.ICustomerService;
import cn.com.libertymutual.core.annotation.SystemLog;
import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.web.ServiceResult;

@Controller("CustomerAction")
@RequestMapping("customer")
public class CustomerAction {
	
	@Autowired
	private ICustomerService  customerService;
	private Logger log = LoggerFactory.getLogger(getClass());
	 
	@RequestMapping(value="find", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@SystemLog(description = "查询")
	@ResponseBody
	public List findList( HttpServletRequest request, Integer fmenuId ) {
		customerService.find();

		return null;
	}
}
