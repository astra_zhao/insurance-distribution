package cn.com.libertymutual.core.redis.config;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
//import org.springframework.session.data.redis.config..ExpiringSession;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RedisTemplateService {

	@Autowired
	@Qualifier("jedisConnectionFactory")
	private JedisConnectionFactory cluster;
	
	@Bean(name="stringRedis")
	public StringRedisTemplate newStringRedisTegmplate() {
		StringRedisTemplate srt = new StringRedisTemplate( cluster );
		srt.setKeySerializer( new StringRedisSerializer(Charset.forName("UTF-8")));
		srt.setValueSerializer( getRedisSerializer() );
		srt.setHashKeySerializer( new StringRedisSerializer(Charset.forName("UTF-8")));
		srt.setHashValueSerializer(getRedisSerializer() );
		
		return srt;
	}
	

	@Bean(name="redisTemplate")
	public RedisTemplate<?, ?> newRedisTemplate() {
		// 使用Jackson2JsonRedisSerialize 替换默认序列化
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
		
		
		@SuppressWarnings("rawtypes")
		RedisTemplate<?, ?> redisTemplate = new RedisTemplate( );
		
		redisTemplate.setConnectionFactory(cluster);
		redisTemplate.setKeySerializer( new StringRedisSerializer(Charset.forName("UTF-8")));
		redisTemplate.setValueSerializer( getRedisSerializer() );
		redisTemplate.setValueSerializer( jackson2JsonRedisSerializer);
		redisTemplate.setHashKeySerializer( new StringRedisSerializer(Charset.forName("UTF-8")));
		redisTemplate.setHashValueSerializer( getRedisSerializer());

		///redisTemplate.setValueSerializer( new LdapFailAwareRedisObjectSerializer() );
		///redisTemplate.setHashValueSerializer(new LdapFailAwareRedisObjectSerializer());
	    

		return redisTemplate;
	}
	/**
	 * 获取序列化方案,使用Jackson2JsonRedisSerialize 替换默认序列化JdkSerializationRedisSerializer
	 * @return
	 */
	private RedisSerializer getRedisSerializer(){
		//	return  new JdkSerializationRedisSerializer();
		
		// 使用Jackson2JsonRedisSerialize 替换默认序列化
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
		
        return jackson2JsonRedisSerializer;
        
	}
	
	
	///@Bean
	/*public RedisTemplate<String,ExpiringSession> redisTemplate() {
	    RedisTemplate<String, ExpiringSession> template = new RedisTemplate<String, ExpiringSession>();

	    template.setKeySerializer(new StringRedisSerializer());
	    template.setHashKeySerializer(new StringRedisSerializer());
	    template.setHashValueSerializer(new LdapFailAwareRedisObjectSerializer());

	    template.setConnectionFactory( cluster );
	    return template;
	  }*/
}
