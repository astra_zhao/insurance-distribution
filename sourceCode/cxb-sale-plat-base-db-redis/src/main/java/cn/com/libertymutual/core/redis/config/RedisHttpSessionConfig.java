package cn.com.libertymutual.core.redis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession//(maxInactiveIntervalInSeconds=3600)
public class RedisHttpSessionConfig implements ApplicationListener<ApplicationEvent> {

	@Autowired
	private RedisProperties redisProperties;
	
	@Autowired
    private RedisOperationsSessionRepository redisOperation;
	@Cacheable
	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		// TODO Auto-generated method stub
		if( event instanceof ContextRefreshedEvent ) {
			redisOperation.setDefaultMaxInactiveInterval( redisProperties.getMaxSessionInactiveIntervalInSeconds() );
		}
		
		/*
		// 在这里可以监听到Spring Boot的生命周期
        if (event instanceof ApplicationEnvironmentPreparedEvent) { // 初始化环境变量 }
        else if (event instanceof ApplicationPreparedEvent) { // 初始化完成 }
        else if (event instanceof ContextRefreshedEvent) { // 应用刷新 }
        else if (event instanceof ApplicationReadyEvent) {// 应用已启动完成 }
        else if (event instanceof ContextStartedEvent) { // 应用启动，需要在代码动态添加监听器才可捕获 }
        else if (event instanceof ContextStoppedEvent) { // 应用停止 }
        else if (event instanceof ContextClosedEvent) { // 应用关闭 }
        else {}*/
	}
	
}
