package cn.com.libertymutual.sys.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_branch",  catalog = "")
public class SysBranch  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -687084041317695725L;
	private String branchNo;
    private String branchCname;
    private String branchEname;
    private String addressCname;
    private String addressEname;
    private String postcode;
    private String phoneNumber;
    private String upperBranchNo;
    private String branchType;
    private String branchLevel;
    private String validStatus;
    private String remark;
    private Double rate;
    private Double issueRate;
    private Double saleRate;

    @Id
    @Column(name = "BRANCH_NO", nullable = false, length = 24)
    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }

    @Basic
    @Column(name = "BRANCH_CNAME", nullable = false, length = 240)
    public String getBranchCname() {
        return branchCname;
    }

    public void setBranchCname(String branchCname) {
        this.branchCname = branchCname;
    }

    @Basic
    @Column(name = "BRANCH_ENAME", nullable = true, length = 240)
    public String getBranchEname() {
        return branchEname;
    }

    public void setBranchEname(String branchEname) {
        this.branchEname = branchEname;
    }

    @Basic
    @Column(name = "ISSUE_RATE")
    public Double getIssueRate() {
		return issueRate;
	}

	public void setIssueRate(Double issueRate) {
		this.issueRate = issueRate;
	}
	@Basic
    @Column(name = "SALE_RATE")
	public Double getSaleRate() {
		return saleRate;
	}

	public void setSaleRate(Double saleRate) {
		this.saleRate = saleRate;
	}

	@Basic
    @Column(name = "ADDRESS_CNAME", nullable = true, length = 765)
    public String getAddressCname() {
        return addressCname;
    }

    public void setAddressCname(String addressCname) {
        this.addressCname = addressCname;
    }

    @Basic
    @Column(name = "ADDRESS_ENAME", nullable = true, length = 240)
    public String getAddressEname() {
        return addressEname;
    }

    public void setAddressEname(String addressEname) {
        this.addressEname = addressEname;
    }

    @Basic
    @Column(name = "POSTCODE", nullable = true, length = 18)
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Basic
    @Column(name = "PHONE_NUMBER", nullable = true, length = 90)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "UPPER_BRANCH_NO", nullable = false, length = 24)
    public String getUpperBranchNo() {
        return upperBranchNo;
    }

    public void setUpperBranchNo(String upperBranchNo) {
        this.upperBranchNo = upperBranchNo;
    }

    @Basic
    @Column(name = "BRANCH_TYPE", nullable = true, length = 30)
    public String getBranchType() {
        return branchType;
    }

    public void setBranchType(String branchType) {
        this.branchType = branchType;
    }

    @Basic
    @Column(name = "BRANCH_LEVEL", nullable = true, length = 3)
    public String getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        this.branchLevel = branchLevel;
    }

    @Basic
    @Column(name = "VALID_STATUS", nullable = false, length = 3)
    public String getValidStatus() {
        return validStatus;
    }

    public void setValidStatus(String validStatus) {
        this.validStatus = validStatus;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 200)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "RATE", nullable = true, length = 10)
    public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}


	private List<SysBranch> nextBranch;
    
    @Transient
    public List<SysBranch> getNextBranch() {
		return nextBranch;
	}

	public void setNextBranch(List<SysBranch> nextBranch) {
		this.nextBranch = nextBranch;
	}

	private String addOrUpdate;
	
	@Transient
	public String getAddOrUpdate() {
		return addOrUpdate;
	}

	public void setAddOrUpdate(String addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressCname == null) ? 0 : addressCname.hashCode());
		result = prime * result + ((addressEname == null) ? 0 : addressEname.hashCode());
		result = prime * result + ((branchCname == null) ? 0 : branchCname.hashCode());
		result = prime * result + ((branchEname == null) ? 0 : branchEname.hashCode());
		result = prime * result + ((branchLevel == null) ? 0 : branchLevel.hashCode());
		result = prime * result + ((branchNo == null) ? 0 : branchNo.hashCode());
		result = prime * result + ((branchType == null) ? 0 : branchType.hashCode());
		result = prime * result + ((nextBranch == null) ? 0 : nextBranch.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((postcode == null) ? 0 : postcode.hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((upperBranchNo == null) ? 0 : upperBranchNo.hashCode());
		result = prime * result + ((validStatus == null) ? 0 : validStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SysBranch other = (SysBranch) obj;
		if (addressCname == null) {
			if (other.addressCname != null)
				return false;
		} else if (!addressCname.equals(other.addressCname))
			return false;
		if (addressEname == null) {
			if (other.addressEname != null)
				return false;
		} else if (!addressEname.equals(other.addressEname))
			return false;
		if (branchCname == null) {
			if (other.branchCname != null)
				return false;
		} else if (!branchCname.equals(other.branchCname))
			return false;
		if (branchEname == null) {
			if (other.branchEname != null)
				return false;
		} else if (!branchEname.equals(other.branchEname))
			return false;
		if (branchLevel == null) {
			if (other.branchLevel != null)
				return false;
		} else if (!branchLevel.equals(other.branchLevel))
			return false;
		if (branchNo == null) {
			if (other.branchNo != null)
				return false;
		} else if (!branchNo.equals(other.branchNo))
			return false;
		if (branchType == null) {
			if (other.branchType != null)
				return false;
		} else if (!branchType.equals(other.branchType))
			return false;
		if (nextBranch == null) {
			if (other.nextBranch != null)
				return false;
		} else if (!nextBranch.equals(other.nextBranch))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (postcode == null) {
			if (other.postcode != null)
				return false;
		} else if (!postcode.equals(other.postcode))
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (upperBranchNo == null) {
			if (other.upperBranchNo != null)
				return false;
		} else if (!upperBranchNo.equals(other.upperBranchNo))
			return false;
		if (validStatus == null) {
			if (other.validStatus != null)
				return false;
		} else if (!validStatus.equals(other.validStatus))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SysBranch [branchNo=" + branchNo + ", branchCname=" + branchCname + ", branchEname=" + branchEname + ", addressCname=" + addressCname
				+ ", addressEname=" + addressEname + ", postcode=" + postcode + ", phoneNumber=" + phoneNumber + ", upperBranchNo=" + upperBranchNo
				+ ", branchType=" + branchType + ", branchLevel=" + branchLevel + ", validStatus=" + validStatus + ", remark=" + remark + ", rate="
				+ rate + ", nextBranch=" + nextBranch + "]";
	}
    
    
}
