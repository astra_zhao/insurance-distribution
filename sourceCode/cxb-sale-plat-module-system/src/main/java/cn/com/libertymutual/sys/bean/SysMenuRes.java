package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_menu_res", catalog = "")
@IdClass(SysMenuResPK.class)
public class SysMenuRes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5743897123330674815L;
	private int menuid;
	private String resid;
	private String resname;
	private Integer restype;
//	@Transient
//	private String isleaf;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MENUID", nullable = false)
	public int getMenuid() {
		return menuid;
	}

	public void setMenuid(int menuid) {
		this.menuid = menuid;
	}

	@Id
	@Column(name = "RESID", nullable = false, length = 32)
	public String getResid() {
		return resid;
	}

	public void setResid(String resid) {
		this.resid = resid;
	}

	@Basic
	@Column(name = "RESNAME", nullable = false, length = 32)
	public String getResname() {
		return resname;
	}

	public void setResname(String resname) {
		this.resname = resname;
	}

	@Basic
	@Column(name = "RESTYPE", nullable = true)
	public Integer getRestype() {
		return restype;
	}

	public void setRestype(Integer restype) {
		this.restype = restype;
	}

//	@Transient
//	public String getIsleaf() {
//		return isleaf;
//	}
//
//	public void setIsleaf(String isleaf) {
//		this.isleaf = isleaf;
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SysMenuRes that = (SysMenuRes) o;

		if (menuid != that.menuid)
			return false;
		if (resid != null ? !resid.equals(that.resid) : that.resid != null)
			return false;
		if (resname != null ? !resname.equals(that.resname) : that.resname != null)
			return false;
		if (restype != null ? !restype.equals(that.restype) : that.restype != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = menuid;
		result = 31 * result + (resid != null ? resid.hashCode() : 0);
		result = 31 * result + (resname != null ? resname.hashCode() : 0);
		result = 31 * result + (restype != null ? restype.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "SysMenuRes [menuid=" + menuid + ", resid=" + resid + ", resname=" + resname + ", restype=" + restype
				+ "]";
	}

	/*
	 * @Transient public Class getEntityClass() { return SysMenuRes.class; }
	 */
}
