package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sys.bean.SysUserMenu;
import cn.com.libertymutual.sys.bean.SysUserMenuPK;


@Repository
public interface ISysUserMenuDao  extends JpaRepository<SysUserMenu, SysUserMenuPK> 
{
	
	
	//@Query( "SELECT DISTINCT t1.menuid FROM tb_sys_role_menu t1 INNER JOIN tb_sys_menu t2 ON t1.menuid=t2.menuid WHERE t1.roleid IN (?1) GROUP BY t1.menuid ORDER BY t2.orderid")
	//List<Integer> findRoleMenu(List<String> roleList, Integer fmenuId) ;
	
	
	@Query( "SELECT DISTINCT t1.menuid FROM SysUserMenu t1 , SysMenu t2 where t1.menuid=t2.menuid and t1.userid = ?1 ")
	List<Integer> findUserMenu(String userId) ;
}
