package cn.com.libertymutual.sys.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.libertymutual.core.annotation.SystemLog;
import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.service.api.ISystemService;

@Controller("SystemAction")
@RequestMapping("system")
public class SystemAction {
	
	@Autowired
	private ISystemService  systemService;
	private Logger log = LoggerFactory.getLogger(getClass());
	/**
	 * 
	* @Title: initLogin 
	* @Description: 初始化登录数据
	* @param request 请求数据
	* @param appFlag 如果传值,则说明是后台应用,如果不传,则说明为业务处理端登录
	* @param fmenuId 父菜单id 
	* @return    ServiceResult 返回的统一Action对象
	* @throws
	 */
	@RequestMapping(value="initLogin", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@SystemLog(description = "初始化登陆数据")
	@ResponseBody
	public ServiceResult initLogin( HttpServletRequest request, Integer fmenuId ) {
		
//   如果无法使用 cas 进行连接,可以将用户信息模拟放到这里进行开发.生产情况这里是需要注释掉
/*  	UserInfo userInfo = new UserInfo();
		userInfo.setUserId(UUID.getUUIDString() );
		userInfo.setTokenId( UUID.getUUIDString() );
		userInfo.setUserName("况尤波");
		List<String> useRiskCode = Lists.newArrayList();
		useRiskCode.add("0502");
		useRiskCode.add("0503");
		List<String> roleId = Lists.newArrayList();
		roleId.add("0001");
		roleId.add("0002");
		userInfo.setBranchNo("HQ");
		userInfo.setBranchName("利宝保险总公司");
		userInfo.setRoleId(roleId);
		userInfo.setUseRiskCode(useRiskCode);
		request.getSession(true).setAttribute(Constants.USER_INFO_KEY, userInfo);
		*/
		
		ServiceResult r = new ServiceResult();
		try{
			systemService.login(request, fmenuId, r);
		//如果是自定义类型直接抓取
		}catch(CustomLangException e){
			log.error(e.getErrorMessage(), e); 
			//返回失败信息给前端
			r.setAppFail(e);
			
		
		}
		return r;
	}
}
