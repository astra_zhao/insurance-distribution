package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysMenuRes;
import cn.com.libertymutual.sys.bean.SysRoleMenuPK;

@Repository
public interface ISysMenuResDao extends JpaRepository<SysMenuRes, SysRoleMenuPK> {

	List<SysMenuRes> findByMenuid(int menuid);
	// @Query( "select DISTINCT MENUID , RESID from tb_sys_role_menu_res where
	// roleid in (:roleIds) order by menuid")
	// List<Map<String,Object>> findMenuResIdList( @Param( "roleIds")
	// List<String> roleIds );
	// List<Map<String,Object>> findMenuResIdList( List<String> roleIds );

	SysMenuRes findByResid(String resid);
	// @Query( "select DISTINCT MENUID , RESID from tb_sys_role_menu_res where
	// roleid in (:roleIds) order by menuid")
	// List<Map<String,Object>> findMenuResIdList( @Param( "roleIds")
	// List<String> roleIds );
	// List<Map<String,Object>> findMenuResIdList( List<String> roleIds );

	// @Query( "select MENUID , RESID from tb_sys_role_menu_res where
	// roleid=:roleId order by menuid")
	// List<Map<String,Object>> findMenuResIdList(@Param( "roleId") String
	// roleId );
	// List<Map<String,Object>> findMenuResIdList(String roleId );

	@Modifying
	@Transactional
	@Query("delete from SysMenuRes where resid = ?1")
	void deleteByResId(String resid);

	@Modifying
	@Transactional
	@Query("delete from SysMenuRes where menuid = ?1")
	void deleteByMenuId(Integer menuId);

	@Query( " from SysMenuRes t where t.menuid=?1 and resid=?2")
	SysMenuRes findByMenuIdAndResid(Integer fmenuId, String resName);

	
//	@Modifying
//	@Transactional
//	@Query("update SysMenuRes set resname = ?1 where resid = ?2")
//	void updateResName(String name, Integer resid);

	// @Query( "select MENUID , RESID from tb_sys_role_menu_res where
	// roleid=:roleId order by menuid")
	// List<Map<String,Object>> findMenuResIdList(@Param( "roleId") String
	// roleId );
	// List<Map<String,Object>> findMenuResIdList(String roleId );
}
