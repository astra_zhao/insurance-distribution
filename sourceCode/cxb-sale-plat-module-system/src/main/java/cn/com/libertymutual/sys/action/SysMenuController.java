package cn.com.libertymutual.sys.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysMenu;
import cn.com.libertymutual.sys.bean.SysRoleMenu;
import cn.com.libertymutual.sys.service.api.SysMenuService;

@RefreshScope
@RestController
@RequestMapping("/systemCfg/")
public class SysMenuController {
	@Autowired
	private SysMenuService sysMenuService;
	
	@RequestMapping(value="sysMenuFindAll")
	public ServiceResult  findAll(boolean isLookup,String lookupDataOne,String lookupDataTwo,int pageNumber, int pageSize, String sortfield,String sorttype){
		return sysMenuService.findAll(isLookup, lookupDataOne, lookupDataTwo, pageNumber, pageSize, sortfield, sorttype);
	}
	@ResponseBody
	@RequestMapping(value="saveSysMenu")
	public ServiceResult saveSysMenu(@RequestBody SysMenu sysMenu){
		return sysMenuService.saveSysMenu(sysMenu);
	}
	
	@RequestMapping(value="updateSysMenu")
	public ServiceResult  updateSysMenu(SysRoleMenu sysRoleMenu){
		return sysMenuService.updateSysMenu(sysRoleMenu);
	}
	
	
	
}
