package cn.com.libertymutual.sys.quartz.config;




public class QuartzJobStoreProperties {
	private String className;		//: org.quartz.impl.jdbcjobstore.JobStoreTX
	private String driverDelegateClass;		//: org.quartz.impl.jdbcjobstore.StdJDBCDelegate
    //#使用系统的连接，这里不需要
    //#dataSource: myDS
	private String tablePrefix;		//: tb_qrtz_
	private boolean isClustered;		//: true
	private String clusterCheckinInterval = "20000";		//: 2000
	private String maxMisfiresToHandleAtATime = "1";		//: 1
	private String misfireThreshold = "120000";		//: 120000
	private boolean txIsolationLevelSerializable;		//: true
	private String selectWithLockSQL;		//: SELECT * FROM {0}LOCKS WHERE LOCK_NAME = ? FOR UPDATE
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return the driverDelegateClass
	 */
	public String getDriverDelegateClass() {
		return driverDelegateClass;
	}
	/**
	 * @param driverDelegateClass the driverDelegateClass to set
	 */
	public void setDriverDelegateClass(String driverDelegateClass) {
		this.driverDelegateClass = driverDelegateClass;
	}
	/**
	 * @return the tablePrefix
	 */
	public String getTablePrefix() {
		return tablePrefix;
	}
	/**
	 * @param tablePrefix the tablePrefix to set
	 */
	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}
	/**
	 * @return the isClustered
	 */
	public boolean isClustered() {
		return isClustered;
	}
	/**
	 * @param isClustered the isClustered to set
	 */
	public void setClustered(boolean isClustered) {
		this.isClustered = isClustered;
	}
	/**
	 * @return the clusterCheckinInterval
	 */
	public String getClusterCheckinInterval() {
		return clusterCheckinInterval;
	}
	/**
	 * @param clusterCheckinInterval the clusterCheckinInterval to set
	 */
	public void setClusterCheckinInterval(String clusterCheckinInterval) {
		this.clusterCheckinInterval = clusterCheckinInterval;
	}
	/**
	 * @return the maxMisfiresToHandleAtATime
	 */
	public String getMaxMisfiresToHandleAtATime() {
		return maxMisfiresToHandleAtATime;
	}
	/**
	 * @param maxMisfiresToHandleAtATime the maxMisfiresToHandleAtATime to set
	 */
	public void setMaxMisfiresToHandleAtATime(String maxMisfiresToHandleAtATime) {
		this.maxMisfiresToHandleAtATime = maxMisfiresToHandleAtATime;
	}
	/**
	 * @return the misfireThreshold
	 */
	public String getMisfireThreshold() {
		return misfireThreshold;
	}
	/**
	 * @param misfireThreshold the misfireThreshold to set
	 */
	public void setMisfireThreshold(String misfireThreshold) {
		this.misfireThreshold = misfireThreshold;
	}
	/**
	 * @return the txIsolationLevelSerializable
	 */
	public boolean isTxIsolationLevelSerializable() {
		return txIsolationLevelSerializable;
	}
	/**
	 * @param txIsolationLevelSerializable the txIsolationLevelSerializable to set
	 */
	public void setTxIsolationLevelSerializable(boolean txIsolationLevelSerializable) {
		this.txIsolationLevelSerializable = txIsolationLevelSerializable;
	}
	/**
	 * @return the selectWithLockSQL
	 */
	public String getSelectWithLockSQL() {
		return selectWithLockSQL;
	}
	/**
	 * @param selectWithLockSQL the selectWithLockSQL to set
	 */
	public void setSelectWithLockSQL(String selectWithLockSQL) {
		this.selectWithLockSQL = selectWithLockSQL;
	}
	
	
}
