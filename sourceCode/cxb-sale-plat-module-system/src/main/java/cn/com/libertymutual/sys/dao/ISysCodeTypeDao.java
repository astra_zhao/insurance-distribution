package cn.com.libertymutual.sys.dao;

import cn.com.libertymutual.sys.bean.SysCodeType;
public interface ISysCodeTypeDao   extends org.springframework.data.repository.CrudRepository
<SysCodeType, Integer>{

}
