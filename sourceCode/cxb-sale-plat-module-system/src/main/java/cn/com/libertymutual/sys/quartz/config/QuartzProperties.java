package cn.com.libertymutual.sys.quartz.config;

import java.util.Properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration("quartzProperties")
@ConfigurationProperties(prefix = "org.quartz")
public class QuartzProperties {
	private QuartzTaskObjectProperties taskObject;

	private QuartzSchedulerProperties scheduler;

	private QuartzJobStoreProperties jobStore;
	private QuartzThreadPoolProperties threadPool;

	private QuartzPluginProperties plugin;

	/**
	 * @return the taskObject
	 */
	public QuartzTaskObjectProperties getTaskObject() {
		return taskObject;
	}

	/**
	 * @param taskObject the taskObject to set
	 */
	public void setTaskObject(QuartzTaskObjectProperties taskObject) {
		this.taskObject = taskObject;
	}

	/**
	 * @return the scheduler
	 */
	public QuartzSchedulerProperties getScheduler() {
		return scheduler;
	}

	/**
	 * @param scheduler the scheduler to set
	 */
	public void setScheduler(QuartzSchedulerProperties scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * @return the jobStore
	 */
	public QuartzJobStoreProperties getJobStore() {
		return jobStore;
	}

	/**
	 * @param jobStore the jobStore to set
	 */
	public void setJobStore(QuartzJobStoreProperties jobStore) {
		this.jobStore = jobStore;
	}

	/**
	 * @return the threadPool
	 */
	public QuartzThreadPoolProperties getThreadPool() {
		return threadPool;
	}

	/**
	 * @param threadPool the threadPool to set
	 */
	public void setThreadPool(QuartzThreadPoolProperties threadPool) {
		this.threadPool = threadPool;
	}

	/**
	 * @return the plugin
	 */
	public QuartzPluginProperties getPlugin() {
		return plugin;
	}

	/**
	 * @param plugin the plugin to set
	 */
	public void setPlugin(QuartzPluginProperties plugin) {
		this.plugin = plugin;
	}

	// @PostConstruct
	public void init() {
		Properties prop = new Properties();
		prop.put("quartz.scheduler.instanceName", "quartzInstanceName");
		prop.put("org.quartz.scheduler.instanceId", "AUTO");
		prop.put("org.quartz.scheduler.skipUpdateCheck", "true");
		prop.put("org.quartz.scheduler.jmx.export", "true");

		prop.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
		prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
		prop.put("org.quartz.jobStore.dataSource", "quartzDataSource");
		prop.put("org.quartz.jobStore.tablePrefix", "qrtz_");
		prop.put("org.quartz.jobStore.isClustered", "true");

		prop.put("org.quartz.jobStore.clusterCheckinInterval", "20000");
		prop.put("org.quartz.jobStore.dataSource", "myDS");
		prop.put("org.quartz.jobStore.maxMisfiresToHandleAtATime", "1");
		prop.put("org.quartz.jobStore.misfireThreshold", "120000");
		prop.put("org.quartz.jobStore.txIsolationLevelSerializable", "true");
		prop.put("org.quartz.jobStore.selectWithLockSQL", "SELECT * FROM {0}LOCKS WHERE LOCK_NAME = ? FOR UPDATE");

		prop.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
		prop.put("org.quartz.threadPool.threadCount", "10");
		prop.put("org.quartz.threadPool.threadPriority", "5");
		prop.put("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread", "true");

		/**
		prop.put("org.quartz.dataSource.myDS.driver", myDSDriver);
		prop.put("org.quartz.dataSource.myDS.URL", myDSURL);
		prop.put("org.quartz.dataSource.myDS.user", myDSUser);
		prop.put("org.quartz.dataSource.myDS.password", myDSPassword);
		System.out.println("myDSMaxConnections:" + myDSMaxConnections);
		prop.put("org.quartz.dataSource.myDS.maxConnections", myDSMaxConnections);
		**/

		prop.put("org.quartz.plugin.triggHistory.class", "org.quartz.plugins.history.LoggingJobHistoryPlugin");
		prop.put("org.quartz.plugin.shutdownhook.class", "org.quartz.plugins.management.ShutdownHookPlugin");
		prop.put("org.quartz.plugin.shutdownhook.cleanShutdown", "true");
	}
}
