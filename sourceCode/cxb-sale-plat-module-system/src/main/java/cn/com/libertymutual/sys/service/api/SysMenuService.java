package cn.com.libertymutual.sys.service.api;

import java.util.List;

import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysMenu;
import cn.com.libertymutual.sys.bean.SysRoleMenu;
import cn.com.libertymutual.sys.vo.MenuInfoVO;

public interface SysMenuService {
	/**
	 * 获取菜单数据 以及查找菜单数据
	 * @param isLookup 是否查找  
	 * @param lookupDataOne   查找内容
	 * @param lookupDataTwo   查找内容
	 * @param pageNumber   当前页数
	 * @param pageSize   页数大小
	 * @param sortfield   排序字段
	 * @param sorttype   排序类型
	 * @return
	 */
	ServiceResult  findAll(boolean isLookup,String lookupDataOne,String lookupDataTwo,int pageNumber, int pageSize, String sortfield,String sorttype);
	/**
	 * 添加/修改一条菜单信息
	 * @param sysMenu  菜单信息
	 * @return
	 */
	ServiceResult saveSysMenu(SysMenu sysMenu); 	

	/** 
	 * 修改菜单状态
	 * @param sysRoleMenu
	 * @return
	 */
	ServiceResult updateSysMenu(SysRoleMenu sysRoleMenu);
	List<MenuInfoVO> getSysMenuInfo(Integer fmenuId) throws CustomLangException; 
	
	
	
}
