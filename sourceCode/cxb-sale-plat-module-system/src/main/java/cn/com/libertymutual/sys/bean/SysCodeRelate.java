package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_code_relate", catalog = "")
public class SysCodeRelate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5987688613139669385L;
	private int id;
	private String code;
	private String codeType;
	private String codeType1;
	private String code1;
	private String codeEname1;
	private String codeCname1;
	private String codeType2;
	private String code2;
	private String codeEname2;
	private String codeCname2;
	private String kindCode;
	private String riskCode;
	private String validStatus;
	private Integer serialNo;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "CODE", nullable = true, length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Basic
	@Column(name = "CODE_TYPE", nullable = true, length = 50)
	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	@Basic
	@Column(name = "CODE_TYPE1", nullable = true, length = 50)
	public String getCodeType1() {
		return codeType1;
	}

	public void setCodeType1(String codeType1) {
		this.codeType1 = codeType1;
	}

	@Basic
	@Column(name = "CODE1", nullable = true, length = 50)
	public String getCode1() {
		return code1;
	}

	public void setCode1(String code1) {
		this.code1 = code1;
	}

	@Basic
	@Column(name = "CODE_ENAME1", nullable = true, length = 100)
	public String getCodeEname1() {
		return codeEname1;
	}

	public void setCodeEname1(String codeEname1) {
		this.codeEname1 = codeEname1;
	}

	@Basic
	@Column(name = "CODE_CNAME1", nullable = true, length = 200)
	public String getCodeCname1() {
		return codeCname1;
	}

	public void setCodeCname1(String codeCname1) {
		this.codeCname1 = codeCname1;
	}

	@Basic
	@Column(name = "CODE_TYPE2", nullable = true, length = 50)
	public String getCodeType2() {
		return codeType2;
	}

	public void setCodeType2(String codeType2) {
		this.codeType2 = codeType2;
	}

	@Basic
	@Column(name = "CODE2", nullable = true, length = 50)
	public String getCode2() {
		return code2;
	}

	public void setCode2(String code2) {
		this.code2 = code2;
	}

	@Basic
	@Column(name = "CODE_ENAME2", nullable = true, length = 100)
	public String getCodeEname2() {
		return codeEname2;
	}

	public void setCodeEname2(String codeEname2) {
		this.codeEname2 = codeEname2;
	}

	@Basic
	@Column(name = "CODE_CNAME2", nullable = true, length = 200)
	public String getCodeCname2() {
		return codeCname2;
	}

	public void setCodeCname2(String codeCname2) {
		this.codeCname2 = codeCname2;
	}

	@Basic
	@Column(name = "KIND_CODE", nullable = true, length = 10)
	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	@Basic
	@Column(name = "RISK_CODE", nullable = false, length = 4)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Basic
	@Column(name = "VALID_STATUS", nullable = false, length = 1)
	public String getValidStatus() {
		return validStatus;
	}

	public void setValidStatus(String validStatus) {
		this.validStatus = validStatus;
	}

	@Basic
	@Column(name = "SERIAL_NO", nullable = false)
	public Integer getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SysCodeRelate that = (SysCodeRelate) o;

		if (id != that.id)
			return false;
		if (serialNo != null ? !serialNo.equals(that.serialNo) : that.serialNo != null)
			return false;
		if (code != null ? !code.equals(that.code) : that.code != null)
			return false;
		if (codeType != null ? !codeType.equals(that.codeType) : that.codeType != null)
			return false;
		if (codeType1 != null ? !codeType1.equals(that.codeType1) : that.codeType1 != null)
			return false;
		if (code1 != null ? !code1.equals(that.code1) : that.code1 != null)
			return false;
		if (codeEname1 != null ? !codeEname1.equals(that.codeEname1) : that.codeEname1 != null)
			return false;
		if (codeCname1 != null ? !codeCname1.equals(that.codeCname1) : that.codeCname1 != null)
			return false;
		if (codeType2 != null ? !codeType2.equals(that.codeType2) : that.codeType2 != null)
			return false;
		if (code2 != null ? !code2.equals(that.code2) : that.code2 != null)
			return false;
		if (codeEname2 != null ? !codeEname2.equals(that.codeEname2) : that.codeEname2 != null)
			return false;
		if (codeCname2 != null ? !codeCname2.equals(that.codeCname2) : that.codeCname2 != null)
			return false;
		if (kindCode != null ? !kindCode.equals(that.kindCode) : that.kindCode != null)
			return false;
		if (riskCode != null ? !riskCode.equals(that.riskCode) : that.riskCode != null)
			return false;
		if (validStatus != null ? !validStatus.equals(that.validStatus) : that.validStatus != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (codeType != null ? codeType.hashCode() : 0);
		result = 31 * result + (codeType1 != null ? codeType1.hashCode() : 0);
		result = 31 * result + (code1 != null ? code1.hashCode() : 0);
		result = 31 * result + (codeEname1 != null ? codeEname1.hashCode() : 0);
		result = 31 * result + (codeCname1 != null ? codeCname1.hashCode() : 0);
		result = 31 * result + (codeType2 != null ? codeType2.hashCode() : 0);
		result = 31 * result + (code2 != null ? code2.hashCode() : 0);
		result = 31 * result + (codeEname2 != null ? codeEname2.hashCode() : 0);
		result = 31 * result + (codeCname2 != null ? codeCname2.hashCode() : 0);
		result = 31 * result + (kindCode != null ? kindCode.hashCode() : 0);
		result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
		result = 31 * result + (validStatus != null ? validStatus.hashCode() : 0);
		result = 31 * result + serialNo;
		return result;
	}

	@Override
	public String toString() {
		return "SysCodeRelate [id=" + id + ", code=" + code + ", codeType=" + codeType + ", codeType1=" + codeType1 + ", code1=" + code1
				+ ", codeEname1=" + codeEname1 + ", codeCname1=" + codeCname1 + ", codeType2=" + codeType2 + ", code2=" + code2 + ", codeEname2="
				+ codeEname2 + ", codeCname2=" + codeCname2 + ", kindCode=" + kindCode + ", riskCode=" + riskCode + ", validStatus=" + validStatus
				+ ", serialNo=" + serialNo + "]";
	}
}
