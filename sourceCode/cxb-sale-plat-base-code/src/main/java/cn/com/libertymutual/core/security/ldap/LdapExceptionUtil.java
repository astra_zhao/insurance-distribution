package cn.com.libertymutual.core.security.ldap;

public class LdapExceptionUtil {
	

	private int errorCode = 0;
	private String errorMessage;

	public LdapExceptionUtil() {
	}
	
	public LdapExceptionUtil( Throwable t ) {
		
		String message = t.getMessage();
		
		if (message.indexOf("775") >= 0) {
			errorCode = -775;
			errorMessage = "账号被锁定";
		}else if (message.indexOf("525") >= 0) {
			errorCode = -525;
			errorMessage = "你的账号错误";
		}else if (message.indexOf("52e") >= 0) {
			errorCode = -526;
			errorMessage = "你的密码错误";
		} else if (message.indexOf("530") >= 0) {
			errorCode = -530;
			//case -530: throw new Exception("not permitted to logon at this time");
			errorMessage = "not permitted to logon at this time";
		} else if (message.indexOf("531") >= 0) {
			errorCode = -531;
			//case -530: throw new Exception("not permitted to logon at this time");
			errorMessage = "not permitted to logon at this workstation";
		} else if (message.indexOf("532") >= 0) {
			errorCode = -532;
			errorMessage = "密码到期,请更改密码";
		} else if (message.indexOf("533") >= 0) {
			errorCode = -533;
			errorMessage = "你的密码错误";
		} else if (message.indexOf("701") >= 0) {
			errorCode = -701;
			errorMessage = "账户期满";
		} else if (message.indexOf("773") >= 0) {
			errorCode = -773;
			errorMessage = "用户必须重设密码";
		} else {
			errorCode = -800;
			t.printStackTrace();
			errorMessage = t.getLocalizedMessage()+"Invalid User "+message;
		}
	}
	
	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
