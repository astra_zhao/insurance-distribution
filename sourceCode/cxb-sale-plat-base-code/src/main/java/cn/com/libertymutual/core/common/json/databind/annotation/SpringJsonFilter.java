package cn.com.libertymutual.core.common.json.databind.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 定义jackjson的字段过滤
 * 
 * @date 2015-03-22 00:53
 * @author bob
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SpringJsonFilter {

	Class<?> mixin() default Object.class;  
	Class<?> target() default Object.class;
	
}
