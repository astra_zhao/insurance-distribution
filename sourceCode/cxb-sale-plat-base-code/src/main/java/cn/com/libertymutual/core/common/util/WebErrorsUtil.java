package cn.com.libertymutual.core.common.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import cn.com.libertymutual.core.common.web.springmvc.WebErrors;



@Component
public class WebErrorsUtil extends WebErrors {
	/**
	 * 默认错误页面
	 */
	public static final String ERROR_PAGE = "/jsp/error_all";
	/**
	 * 默认错误信息属性名称
	 */
	public static final String ERROR_ATTR_NAME = "errors";

	/**
	 * 通过HttpServletRequest创建WebErrors
	 * 
	 * @param request
	 *            从request中获得MessageSource和Locale，如果存在的话。
	 * @return 如果LocaleResolver存在则返回国际化WebErrors
	 */
	public static WebErrorsUtil create(HttpServletRequest request) {
		return new WebErrorsUtil(request);
	}

	public WebErrorsUtil() {
	}

	public WebErrorsUtil(HttpServletRequest request) {
		super(request);
	}

	/**
	 * 构造器
	 * 
	 * @param messageSource
	 * @param locale
	 */
	public WebErrorsUtil(MessageSource messageSource, Locale locale) {
		super(messageSource, locale);
	}

	@Override
	protected String getErrorAttrName() {
		return ERROR_ATTR_NAME;
	}

	@Override
	protected String getErrorPage() {
		return ERROR_PAGE;
	}
}
