package cn.com.libertymutual.core.util;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.util.DateUtil;

public class InsureUtils {
	private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",
		"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	
	private final static String[] baseStrs = { "0", "1", "2", "3", "4", "5",
		"6", "7", "8", "9", "A", "B", "C", "D", "E", "F","G","H","I","J","K","L","M","N","O","P","Q","R",
		"S","T","U","V","W","X","Y","Z"};
	
	private final static String[] baseStrs2 = {  "1", "2", "3", "4", "5",
		"6", "7", "8", "9", "A", "B", "C", "D", "E", "F","G","H","I","J","K","L","M","N","O","P","Q","R",
		"S","T","U","V","W","X","Y","Z"};
/**
 * 根据身份证获取 年龄
 * @param idCard
 * @return
 */
	public static int  getAgeByIDCard(String idCard){
		int age=0;
		try {
			String sYear,sMonth,sDay;
			if(idCard.length()==15){
				sYear="19" + idCard.substring(6,8);
				sMonth=idCard.substring(8,10);
				sDay=idCard.substring(10,12);
			}else if(idCard.length() ==18){
			    sYear=idCard.substring(6,10);
				sMonth=idCard.substring(10,12);
				sDay=idCard.substring(12,14);
			}else{
			    return 0;
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int currentYear=cal.get(Calendar.YEAR);//当前年份
			int month = cal.get(Calendar.MONTH) + 1;//当前月
			int day = cal.get(Calendar.DAY_OF_MONTH);//得到天
			age = currentYear - Integer.valueOf(sYear) - 1;
			if (Integer.valueOf(sMonth) < month || Integer.valueOf(sMonth) == month && Integer.valueOf(sDay) <= day)
				age++;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
	    return age;
	}
/**
 * 根据身份证获取 性别  1：男，2：女
 * @param identifyNo
 * @return
 */
	public static String getSexByIDCard(String identifyNo){
		String sexFlag="1";
		if(!"".equals(identifyNo)){
			if(identifyNo.length()==15){
		        if(Integer.valueOf(identifyNo.substring(14,15))%2==0){   
		        	sexFlag = "2";   
		        }else{
		        	sexFlag = "1";
		        }
		    }else if(identifyNo.length() ==18){
		        if(Integer.valueOf(identifyNo.substring(16,17))%2==0){
		        	sexFlag = "2";
		        }else{
		        	sexFlag = "1";
		        }
		    }
		}
		return sexFlag;
	}
	//通过身份证来计算出生日期
	public static Date getBirdayByIDCard(String idCard){
		String sYear,sMonth,sDay,birday;
	    if(idCard.length()==15){
	    	sYear="19" + idCard.substring(6,8);
	    	sMonth=idCard.substring(8,10);
	    	sDay=idCard.substring(10,12);
	    	birday=sYear+"-"+sMonth+"-"+sDay;
		}else if(idCard.length() ==18){
		    sYear=idCard.substring(6,10);
	    	sMonth=idCard.substring(10,12);
	    	sDay=idCard.substring(12,14);
	    	birday=sYear+"-"+sMonth+"-"+sDay;
		}else{
		    return new Date();
		}
	    
	    Date date = null;
		try {
			date = DateUtil.convertStringToDate(birday);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return date;
	}
	
	/**
	 * 参数排序重组
	 * @param input
	 * @return
	 * @throws CustomLangException 
	 */
	public static String sortByName(String input) throws CustomLangException {
		
		try{		
			String retString = "";
			String[] arrs = input.split("&");
			Arrays.sort(arrs);//数组升序排序
			String key = "";
			String value = "";
			for (int i = 0; i < arrs.length; i++) {
				key = arrs[i].substring(0, arrs[i].indexOf("="));
				value = arrs[i].substring(arrs[i].indexOf("=") + 1);
				if(i==arrs.length-1){
					retString += key + "=" + value;
				}else{
					retString += key + "=" + value + "&";
				}
			}
			return retString;			
		//如果不是自定义异常转换为自定义异常
		}catch(Exception e){
			throw new  CustomLangException(e);
		}
	}
	/**
	 * MD5
	 * @param originString
	 * @return
	 * @throws CustomLangException 
	 */
	public static String encodeByMD5(String originString) throws CustomLangException{
		try{		
			if (originString != null) {
				// 创建具有指定算法名称的信息摘要
				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] data = originString.getBytes("UTF-8");
				// 使用指定的字节数组对摘要进行最后更新，然后完成摘要计算
				byte[] results = md.digest(data);
				// 将得到的字节数组变成字符串返回
				String resultString = byteArrayToHexString(results);
				// return resultString.toUpperCase();
				return resultString;
			}
			return null;
			
		//如果不是自定义异常转换为自定义异常
		}catch(Exception e){
			throw new  CustomLangException(e);
		}
	}

	private static String byteArrayToHexString(byte[] b) throws CustomLangException {
		
		try{		
			StringBuffer resultSb = new StringBuffer();
			for (int i = 0; i < b.length; i++) {
				resultSb.append(byteToHexString(b[i]));
			}
			return resultSb.toString();			
		//如果不是自定义异常转换为自定义异常
		}catch(Exception e){
			throw new  CustomLangException(e);
		}
	}

	private static String byteToHexString(byte b) throws CustomLangException {

		try{		
			int n = b;
			if (n < 0)
				n = 256 + n;
			int d1 = n / 16;
			int d2 = n % 16;
			return hexDigits[d1] + hexDigits[d2];			
		//如果不是自定义异常转换为自定义异常
		}catch(Exception e){
			throw new  CustomLangException(e);
		}
	}
	
	/**
	 * 生成6位验车码
	 * @return
	 */
	public static   String checkCode(int length){
		StringBuilder sb =new StringBuilder();
		for(int i=0;i<length;i++){
			Random rand=new Random();
			int index=rand.nextInt(35);
			sb.append(baseStrs2[index]);
		}
		
		return sb.toString();
	} 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(getAgeByIDCard("500243198808082914"));
		//System.out.println(getSexByIDCard("500243198808082914"));
		BigDecimal b1 = new BigDecimal("2775");
		BigDecimal b2 = new BigDecimal("100");
		double cash = b1.divide(b2).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		//System.out.println(cash);
		//System.out.println(encodeByMD5("amount=28.00&businessNo=WX00002080201802071518000263704B&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13806531378&property01=6228480332080590513&property02=薛仙珠&property03=33032519700422272X&property04=农业银行&property05=瑞安市玉海支行&property06=浙江省&property07=温州市&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		//System.out.println(encodeByMD5("amount=30.50&branchCode=1100&businessNo=WX00000536201802071517987748177B&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13661331742&property01=4367420014670285024&property02=刘悦&property03=650103197702165123&property04=中国建设银行&property05=中国建设银行北京市分行金融支行专柜&property06=北京市&property07=北京&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		//System.out.println(encodeByMD5("amount=24.39&branchCode=1100&businessNo=BS00002130201802091518156794685B&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13752795280&property01=6214830124278291&property02=姚頔&property03=120104198912163844&property04=招商银行&property05=建国路支行&property06=北京市&property07=北京&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		//System.out.println(encodeByMD5("amount=287.44&branchCode=1100&businessNo=WX00000209201802071517977388403B&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13601282455&property01=4367420015910047900&property02=梁爽&property03=110108198310074928&property04=建设银行&property05=北京建行华贸支行&property06=北京市&property07=北京&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		System.out.println(encodeByMD5("amount=1.26&branchCode=00&businessNo=WX00000613201802111518310550334B&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13426154973&property01=6227000011590035044&property02=金雪&property03=110102198809020429&property04=中国建设银行&property05=中国建设银行股份有限公司北京东四支行&property06=北京市&property07=北京市&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		System.out.println(encodeByMD5("amount=184.00&branchCode=00&businessNo=WX00000613201802071517969184580C&businessSource=cnp&payType=online&platformId=bill-cnp&property00=13426154973&property01=6227000011590035044&property02=金雪&property03=110102198809020429&property04=中国建设银行&property05=中国建设银行股份有限公司北京东四支行&property06=北京市&property07=北京市&remarks=销售平台积分提现"+"v56878yb9s6dwghm99999bc3tvp6abbb"));
		int i=0;
		while(i<10){
		System.out.println(checkCode(6));
		i++;
		}
		//{"ID":605,"USER_CODE":"WX00000613","NUMBER":"6227000011590035044","MOBILE":"13426154973","USER_NAME":"金雪","ID_NUMBER":"110102198809020429","FULL_NAME":"中国建设银行","FULL_NAME_PIN_YIN":"zhongguojiansheyinhang","TYPE_NAME":"龙卡储蓄卡","TYPE_NAME_PIN_YIN":"longkachuxuka","PRODUCT_NAME":null,"BRANCH_NAME":"中国建设银行股份有限公司北京东四支行","PROVINCE":"北京市","CITY":"北京市","AREA":null,"DETAILED_ADDRESS":null,"ABBREVIATED_LETTERS":null,"CODE":null,"TEL_BANK":null,"URL_BANK":null,"BACKGROUND_COLOR":"#0464b3","IS_DEFAULT":1,"CREATE_TIME":1518421820000,"UPDATE_TIME":1518421820000,"UNBUNDING_TIME":null,"STATE":1}	
		/*
 * {"ID":53,"USER_CODE":"WX00000536","NUMBER":"4367420014670285024","MOBILE":"13661331742","USER_NAME":"刘悦","ID_NUMBER":"650103197702165123","FULL_NAME":"中国建设银行","FULL_NAME_PIN_YIN":"zhongguojiansheyinhang","TYPE_NAME":"龙卡储蓄卡","TYPE_NAME_PIN_YIN":"longkachuxuka","PRODUCT_NAME":null,"BRANCH_NAME":"中国建设银行北京市分行金融支行专柜","PROVINCE":"北京市","CITY":"北京市","AREA":null,"DETAILED_ADDRESS":null,"ABBREVIATED_LETTERS":null,"CODE":null,"TEL_BANK":null,"URL_BANK":null,"BACKGROUND_COLOR":"#0464b3","IS_DEFAULT":1,"CREATE_TIME":1513324604000,"UPDATE_TIME":1518405449000,"UNBUNDING_TIME":1518405254000,"STATE":1}
 * */
	}
}
