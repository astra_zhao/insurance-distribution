package cn.com.libertymutual.core.common.logback;

public class AppPlatInfo {

	private static String serviceName;
	private static String localHostIP;
	private static String localHostName;

	public static String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		AppPlatInfo.serviceName = serviceName;
	}
	public static String getLocalHostIP() {
		return localHostIP;
	}
	public void setLocalHostIP(String localHostIP) {
		AppPlatInfo.localHostIP = localHostIP;
	}
	public static String getLocalHostName() {
		return localHostName;
	}
	public void setLocalHostName(String localHostName) {
		AppPlatInfo.localHostName = localHostName;
	}
}
