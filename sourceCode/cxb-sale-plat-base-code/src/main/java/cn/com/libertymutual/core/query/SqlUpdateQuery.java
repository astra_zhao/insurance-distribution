package cn.com.libertymutual.core.query;

/**
 * 用于执行的SQL语句,也可以用于SELECT语句
 * 
 * @author LuoGang
 *
 */
public class SqlUpdateQuery extends SqlWhere {

	public SqlUpdateQuery() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 通过执行语句构造
	 * 
	 * @param update
	 */
	public SqlUpdateQuery(String update) {
		super();
		this.setUpdate(update);
	}

//	public SqlUpdateQuery(String sql, List<Object> parameters) {
//		super(sql, parameters);
//	}

	/**
	 * 返回要执行的SQL语句，格式如：UPDATE/DELETE ... WHERE xxx
	 * 
	 * @return
	 */
	public String toSql() {
		StringBuilder sql = new StringBuilder(this.update);
		if (this.hasWhere()) {
			sql.append(" WHERE ").append(this.where());
		}
		return sql.toString();
	}

	/** 执行语句 */
	String update;

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}
}
