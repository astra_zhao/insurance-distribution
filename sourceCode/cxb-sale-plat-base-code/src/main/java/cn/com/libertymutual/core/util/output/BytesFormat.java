package cn.com.libertymutual.core.util.output;

public class BytesFormat {
	private int columns;
	private String seprator;
	private String linePrompt;
	private String header;
	private String charset;

	public BytesFormat() {
		setColumns(16);
	    setSeprator(" ");
	    setLinePrompt("%4s: ");
	    setCharset("GBK");
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public String getSeprator() {
		return seprator;
	}

	public void setSeprator(String seprator) {
		this.seprator = seprator;
	}

	public String getLinePrompt() {
		return linePrompt;
	}

	public void setLinePrompt(String linePrompt) {
		this.linePrompt = linePrompt;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	
}
