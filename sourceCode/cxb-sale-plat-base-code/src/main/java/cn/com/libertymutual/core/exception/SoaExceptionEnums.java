/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.core.exception
 *  Create Date 2016年4月29日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

/**
 * 接口异常分类 5XXX错误
 * @author tracy.liao
 * @date 2016年4月29日
 */
public enum SoaExceptionEnums implements ExceptionEnums {
	RENEWAL_QUERY_FALLBACK_EXCEPTION(6001,"续保查询失败"),
	QUERY_VEHICLEPLATE_FALLBACK_EXCEPTION(6002,"平台交互查询失败"),
	WEBSERVICE_TRANSPORT_EXCEPTION(6003,"接口异常"),
	PERON_NOT_FOUND_EXCEPTION(6004,"人员未找到"),
	RECALL_POLICY_FAIL_EXCEPTION(6005,"未查询到保单数据"),
	MANAGEMENT_QUERY_FAIL_EXCEPTION(6006,"签单查询数据超过1000条，无法正常显示，请添加查询条件"),
	CUSTOMER_QUERY_FAIL_EXCEPTION(6007,"未查询到客户信息"),
	CALL_PRINT_INTERFACE_EXCEPTION(6008,"未查询到打印数据"),
	MANAGEMENT_NOT_FIND_DATA_EXCEPTION(6009,"未取得单证信息"),
	VEHICLE_TYPE_QUERY_EXCEPTION(6010,"未查询到车型数据"),
	VIN_QUERY_EXCEPTION(6011,"未查询到VIN数据"),
	GET_CONNECTION_LOCAL_ERROR(6012,"未能获取到正确的接口地址"),
	GET_PAYMENT_QUERY_ERROR(6013,"获取支付状态失败"),
	GET_PAYMENT_ERROR(6014,"获取支付方式失败"),
	GET_PAYMENT_CODE_ERROR(6015,"获取支付信息失败"),
	RENCALL_POLICY_ERROR(6016,"撤回保单失败"),

	GET_DATA_ERROR_BY_FLOWID(6017,"未根据FlowId查询到相关的业务数据,请重新进行保费计算"),
	COVERAGE_CALCULATION_ERROR(6018,"计算车辆实际价值失败"),
	GET_DATA_ERROR_ACCOUNT(6019,"未查询到对应的合作伙伴代码"), 
	GET_DATA_ERROR_TPAGREEMENTCONF(6020,"未查询到对应的业务关系代码"),
	GET_DATA_ERROR_TPPREDEFINE(6021,"未查询到对应的配置信息"),
	GET_DATA_ERROR_AGENTCODE(6031,"未获取到代理人信息,业务关系代码配置有误"),
	QueryCommission_EEROR(6022,"查询佣金失败"),
	NEWCARRECORD_ERROR(6023,"新车备案失败"),
	MotorModelQuery_ERROR(6024,"北京车型查询失败"),
	VerifyPROPOSAL_ERROR(6025,"转保失败"),
	PAYMENT_STATUS_ERROR(6026,"支付状态不是等待支付,无法进行支付操作"),
	PAYMENT_FLOWID_ERROR(6032,"未根据FlowId查询到投保信息,无法进行支付操作"),
	DISTRIBUT_STATUS_ERROR(6027,"配送信息发送给第三方失败"),
	PAYMENT_MONEY_ERROR(6028,"支付金额不正确,无法生成保单"),
	PAYMENT_DATE_ERROR(6032,"支付时间已超过起保时间,无法生成保单"),
	PAYMENT_INPUT_DATE_ERROR(6033,"支付时间已超过最晚支付时间,无法生成保单!请重新进行算费提核"),
	PAYMENT_JD_ERROR(6029,"京东支付参数有误"),
	PROPOSAL_RE_ERROR(6030,"车辆发生重复投保"),
	MotorModelQuery_DATA_ERROR(6031,"保费计算选择的车型,与平台返回的车型信息不一致,请重新选择"),
	AGREEMENTQuery_DATA_ERROR(6032,"无效的业务关系代码,请确认该业务关系代码状态,以及是否在有效期范围内"),
	KINDCODE_DATA_ERROR(6033,"险别代码不正确,请确认"),
	BLACK_CUSTOMER_ERROR(6034,"人员黑名单提醒:"),
	MOTOR_TRANSFERPRP_ERROR(6035,"商业险转保失败"),
	MTPL_TRANSFERPRP_ERROR(6036,"交强险转保失败"),
	LOGIN_VALIDATE_ERROR_EXCEPTION(6000,"用户信息校验失败,请使用正确的合作伙伴代码和备案号"), 
	QUERY_DOCUMENTLIST_MAX_EXCEPTION(6037,"清单查询次数超过规定的最大值"), 
	QUERY_DOCUMENTLIST_MAX_DAYS_EXCEPTION(6038,"清单查询时间不在有效范围内,只能查询限制天数内的数据"), 
	QUERY_LBEXPRESS_DATA_EXCEPTION(6040,"物流记录查询失败"), 
	QUERY_DOCUMENT_DATA_ERROR(6041,"录入的投保单号有误,无法进行后续操作,请确保投保单号的是否正确"), 
	REQUEST_DATA_WHITE_LIST_ERROR(6042,"请求Ip不在配置白名单中"), 
	VERIFYDATA_ERROR( 6039,"转保请求不正确,数据错误!");
	;

	public int code;
	public String message;

	SoaExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}
