import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: '*',
        component: Home,
        hidden: true
    },
    {
        path: '/index',
        component: Home,
        children: [
            { path: '/index', component: Main },
        ]
    }
];

export default routes;