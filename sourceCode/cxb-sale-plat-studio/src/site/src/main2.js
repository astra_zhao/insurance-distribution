import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();
// import axios from 'axios';
// axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
import 'font-awesome/css/font-awesome.min.css'
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });
// alert(store.state.count);
// store.state.count=11;
// alert(store.state.count);
const router = new VueRouter({
   //mode: 'history',
   routes
})
let _this=Vue.prototype;
router.beforeEach((to, from, next) => {
  if (to.path == '/login') {
    sessionStorage.removeItem('user');
  }
  let user = JSON.parse(sessionStorage.getItem('user'));
  if (!user && to.path != '/login') {
    next({ path: '/login' })
  } else {
    if(to.path == '/login'||from.path=='/login'||(to.path == '/'&&from.path=='/')||store.state.count==1){
      next()
    }else {
      sessionStorage.setItem("isjump", true);
      sessionStorage.setItem("url", to.path);
      // console.log("topath:"+to.path);
      // console.log("formpath:"+from.path);
      next({ path: '/' })
    }

    // if(store.state.count==0||store.state.count==1){
    //   store.state.count=2
    //   next()
    // }

    next()
  }
})

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
  //el: '#app',
  //template: '<App/>',
  router,
  store,
  //components: { App }
  render: h => h(App)
}).$mount('#app')
