package cn.com.libertymutual.production.pojo.request;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PrpdKindLibraryRequest extends Request {
	private String kindcode;

	private String kindversion;

	private List<String> riskcodes;

	private Date startdate;

	private Date enddate;

	private Date createdate;

	private String kindcname;

	private String kindename;

	private String clausecode;

	private String relyonkindcode;

	private Date relyonstartdate;

	private Date relyonenddate;

	private String calculateflag;

	private String newkindcode;

	private String validstatus;

	private String flag;

	private String ownerriskcode;

	private String id;

	private String efilestatus;

	private String kindheight;

	private String kindwidth;

	private String waysofcalc;

	private String ilogflag;

	private BigDecimal agreementrate;

	private String pluskindcode;

	private BigDecimal plusrate;

	private String powagentcode;

	private String remark;

	private String shortratetype;

	private PrpdRiskRequest risk;

	private int kindType;

	public String getKindcode() {
		return kindcode;
	}

	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}

	public String getKindversion() {
		return kindversion;
	}

	public void setKindversion(String kindversion) {
		this.kindversion = kindversion;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getKindcname() {
		return kindcname;
	}

	public void setKindcname(String kindcname) {
		this.kindcname = kindcname;
	}

	public String getKindename() {
		return kindename;
	}

	public void setKindename(String kindename) {
		this.kindename = kindename;
	}

	public String getClausecode() {
		return clausecode;
	}

	public void setClausecode(String clausecode) {
		this.clausecode = clausecode;
	}

	public String getRelyonkindcode() {
		return relyonkindcode;
	}

	public void setRelyonkindcode(String relyonkindcode) {
		this.relyonkindcode = relyonkindcode;
	}

	public Date getRelyonstartdate() {
		return relyonstartdate;
	}

	public void setRelyonstartdate(Date relyonstartdate) {
		this.relyonstartdate = relyonstartdate;
	}

	public Date getRelyonenddate() {
		return relyonenddate;
	}

	public void setRelyonenddate(Date relyonenddate) {
		this.relyonenddate = relyonenddate;
	}

	public String getCalculateflag() {
		return calculateflag;
	}

	public void setCalculateflag(String calculateflag) {
		this.calculateflag = calculateflag;
	}

	public String getNewkindcode() {
		return newkindcode;
	}

	public void setNewkindcode(String newkindcode) {
		this.newkindcode = newkindcode;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getOwnerriskcode() {
		return ownerriskcode;
	}

	public void setOwnerriskcode(String ownerriskcode) {
		this.ownerriskcode = ownerriskcode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEfilestatus() {
		return efilestatus;
	}

	public void setEfilestatus(String efilestatus) {
		this.efilestatus = efilestatus;
	}

	public String getKindheight() {
		return kindheight;
	}

	public void setKindheight(String kindheight) {
		this.kindheight = kindheight;
	}

	public String getKindwidth() {
		return kindwidth;
	}

	public void setKindwidth(String kindwidth) {
		this.kindwidth = kindwidth;
	}

	public String getWaysofcalc() {
		return waysofcalc;
	}

	public void setWaysofcalc(String waysofcalc) {
		this.waysofcalc = waysofcalc;
	}

	public String getIlogflag() {
		return ilogflag;
	}

	public void setIlogflag(String ilogflag) {
		this.ilogflag = ilogflag;
	}

	public BigDecimal getAgreementrate() {
		return agreementrate;
	}

	public void setAgreementrate(BigDecimal agreementrate) {
		this.agreementrate = agreementrate;
	}

	public String getPluskindcode() {
		return pluskindcode;
	}

	public void setPluskindcode(String pluskindcode) {
		this.pluskindcode = pluskindcode;
	}

	public BigDecimal getPlusrate() {
		return plusrate;
	}

	public void setPlusrate(BigDecimal plusrate) {
		this.plusrate = plusrate;
	}

	public String getPowagentcode() {
		return powagentcode;
	}

	public void setPowagentcode(String powagentcode) {
		this.powagentcode = powagentcode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<String> getRiskcodes() {
		return riskcodes;
	}

	public void setRiskcodes(List<String> riskcodes) {
		this.riskcodes = riskcodes;
	}

	public String getShortratetype() {
		return shortratetype;
	}

	public void setShortratetype(String shortratetype) {
		this.shortratetype = shortratetype;
	}

	public PrpdRiskRequest getRisk() {
		return risk;
	}

	public void setRisk(PrpdRiskRequest risk) {
		this.risk = risk;
	}

	public int getKindType() {
		return kindType;
	}

	public void setKindType(int kindType) {
		this.kindType = kindType;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

}
