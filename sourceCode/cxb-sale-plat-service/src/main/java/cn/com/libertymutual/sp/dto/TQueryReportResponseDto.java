package cn.com.libertymutual.sp.dto;

import java.util.ArrayList;
import java.util.List;



import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

public class TQueryReportResponseDto extends ResponseBaseDto{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<TReportDto> reports;
	public List<TReportDto> getReports() {
		if(null==reports){
			return new ArrayList<TReportDto>();
		}
		return reports;
	}
	public void setReports(List<TReportDto> reports) {
		this.reports = reports;
	}
	
	
}

