package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;


public class TPayInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7675294073354909418L;

	private String paymentNo;//  唯一识别码/支付号
	private String certino;//主业务单号
	private double sumPremium;//支付总金额
	private String paySatus;//支付状态：0 可以支付,1 不可支付
	private String message;//不可支付的原因
	private String callBackUrl;//回调地址
	private String startdate;//起保时间
	private String enddate;//终保时间
	
	
	private List<TPolicyDto>  documentList;//单据列表
	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}



	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public double getSumPremium() {
		return sumPremium;
	}

	public void setSumPremium(double sumPremium) {
		this.sumPremium = sumPremium;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getCertino() {
		return certino;
	}

	public void setCertino(String certino) {
		this.certino = certino;
	}

	public List<TPolicyDto> getDocumentList() {
		if(null==documentList){
			documentList=Lists.newArrayList();
		}
		return documentList;
	}

	public void setDocumentList(List<TPolicyDto> documentList) {
		this.documentList = documentList;
	}

	public String getPaySatus() {
		return paySatus;
	}

	public void setPaySatus(String paySatus) {
		this.paySatus = paySatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
