package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpFlowDetail;
@Repository
public interface FlowDetailDao extends PagingAndSortingRepository<TbSpFlowDetail, Integer>, JpaSpecificationExecutor<TbSpFlowDetail> {

	@Query("from TbSpFlowDetail where flowNo=?1 and parentNode=?2 ")
	List<TbSpFlowDetail> findByParentNode(String flowNo,String parentNode);

	
	@Query("from TbSpFlowDetail where flowNo=?1 and flowNode=?2 ")
	TbSpFlowDetail findByNoAndNode(String flowNo,String flowNode);

	@Query("from TbSpFlowDetail where flowNo=?1 ")
	List<TbSpFlowDetail> findByFlowNo(String flowNo);
	
}
