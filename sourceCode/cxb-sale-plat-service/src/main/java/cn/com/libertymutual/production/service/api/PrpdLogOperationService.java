package cn.com.libertymutual.production.service.api;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogoperation;

public interface PrpdLogOperationService {

	public void insert(Prpdlogoperation record);
	
	public Prpdlogoperation findLogDetail(String logId);
	
}
