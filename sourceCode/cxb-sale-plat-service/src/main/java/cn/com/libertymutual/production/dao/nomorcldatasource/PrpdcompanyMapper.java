package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdcompany;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyWithBLOBs;
@Mapper
public interface PrpdcompanyMapper {
    int countByExample(PrpdcompanyExample example);

    int deleteByExample(PrpdcompanyExample example);

    int insert(PrpdcompanyWithBLOBs record);

    int insertSelective(PrpdcompanyWithBLOBs record);

    List<PrpdcompanyWithBLOBs> selectByExampleWithBLOBs(PrpdcompanyExample example);

    List<Prpdcompany> selectByExample(PrpdcompanyExample example);

    int updateByExampleSelective(@Param("record") PrpdcompanyWithBLOBs record, @Param("example") PrpdcompanyExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdcompanyWithBLOBs record, @Param("example") PrpdcompanyExample example);

    int updateByExample(@Param("record") Prpdcompany record, @Param("example") PrpdcompanyExample example);
}