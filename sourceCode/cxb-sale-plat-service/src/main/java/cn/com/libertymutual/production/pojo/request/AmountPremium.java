package cn.com.libertymutual.production.pojo.request;

public class AmountPremium {

	private long serialno;
	private String kindcode;
	private String kindversion;
	private String kindcname;
	private String kindename;
	private String itemcode;
	private String itemcname;
	private String itemename;
	private String itemflag;
	private String quantity;
	private String currency;
	private String amount;
	private String unitamount;
	private String rate;
	private String discount;
	private String premium;
	private String unitpremium;
	private String calculateflag;
	private String ownerFlag;

	public long getSerialno() {
		return serialno;
	}

	public void setSerialno(long serialno) {
		this.serialno = serialno;
	}

	public String getKindcode() {
		return kindcode;
	}

	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}

	public String getItemcode() {
		return itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUnitamount() {
		return unitamount;
	}

	public void setUnitamount(String unitamount) {
		this.unitamount = unitamount;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getUnitpremium() {
		return unitpremium;
	}

	public void setUnitpremium(String unitpremium) {
		this.unitpremium = unitpremium;
	}

	public String getItemcname() {
		return itemcname;
	}

	public void setItemcname(String itemcname) {
		this.itemcname = itemcname;
	}

	public String getItemename() {
		return itemename;
	}

	public void setItemename(String itemename) {
		this.itemename = itemename;
	}

	public String getItemflag() {
		return itemflag;
	}

	public void setItemflag(String itemflag) {
		this.itemflag = itemflag;
	}

	public String getKindversion() {
		return kindversion;
	}

	public void setKindversion(String kindversion) {
		this.kindversion = kindversion;
	}

	public String getKindcname() {
		return kindcname;
	}

	public void setKindcname(String kindcname) {
		this.kindcname = kindcname;
	}

	public String getKindename() {
		return kindename;
	}

	public void setKindename(String kindename) {
		this.kindename = kindename;
	}

	public String getCalculateflag() {
		return calculateflag;
	}

	public void setCalculateflag(String calculateflag) {
		this.calculateflag = calculateflag;
	}

	public String getOwnerFlag() {
		return ownerFlag;
	}

	public void setOwnerFlag(String ownerFlag) {
		this.ownerFlag = ownerFlag;
	}

}
