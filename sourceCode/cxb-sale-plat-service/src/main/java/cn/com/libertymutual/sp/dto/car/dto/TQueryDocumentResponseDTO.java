package cn.com.libertymutual.sp.dto.car.dto;

import java.util.List;

import cn.com.libertymutual.sp.dto.car.bean.TpTitemApplicant;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemCar;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemEngage;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemKind;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemTax;
import cn.com.libertymutual.sp.dto.car.bean.TpTmain;

public class TQueryDocumentResponseDTO extends ResponseBaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5410513158299945211L;
	
	private String renewalFlag;//是否允许续保标识
	private String renewalProposalnos;//续保生成的投保单，商业险交强险投保单分别用 逗号,隔开
	
	
	private TpTmain tprpTmainDto;// 投保主信息
	private String policyNo; //商业险保单号
	private String mtplPolicyNo; //交强险保单号
	private TpTitemCar        tprpTitemCarDto; //车辆信息
	private TpTitemApplicant  tprptCarOwnerDto;  //车主信息
	private TpTitemApplicant  tprptApplicantDto; //投保人信息
	private TpTitemApplicant  tprptInsuredDto;   //被保险人信息
	private TpTitemTax PrpTcarshipTaxDto;// 车船税信息
	private List<TpTitemKind> tprpTitemKindListDto; // 投保险别列表
	private List<TpTitemEngage> tprptEngageDTOList; // 特别约定列表
	public String getRenewalFlag() {
		return renewalFlag;
	}
	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	public String getRenewalProposalnos() {
		return renewalProposalnos;
	}
	public void setRenewalProposalnos(String renewalProposalnos) {
		this.renewalProposalnos = renewalProposalnos;
	}
	public String getMtplPolicyNo() {
		return mtplPolicyNo;
	}
	public void setMtplPolicyNo(String mtplPolicyNo) {
		this.mtplPolicyNo = mtplPolicyNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public TpTmain getTprpTmainDto() {
		return tprpTmainDto;
	}
	public TpTitemCar getTprpTitemCarDto() {
		return tprpTitemCarDto;
	}
	public TpTitemApplicant getTprptCarOwnerDto() {
		return tprptCarOwnerDto;
	}
	public TpTitemApplicant getTprptApplicantDto() {
		return tprptApplicantDto;
	}
	public TpTitemApplicant getTprptInsuredDto() {
		return tprptInsuredDto;
	}
	public TpTitemTax getPrpTcarshipTaxDto() {
		return PrpTcarshipTaxDto;
	}
	public List<TpTitemKind> getTprpTitemKindListDto() {
		return tprpTitemKindListDto;
	}
	public List<TpTitemEngage> getTprptEngageDTOList() {
		return tprptEngageDTOList;
	}
	public void setTprpTmainDto(TpTmain tprpTmainDto) {
		this.tprpTmainDto = tprpTmainDto;
	}
	public void setTprpTitemCarDto(TpTitemCar tprpTitemCarDto) {
		this.tprpTitemCarDto = tprpTitemCarDto;
	}
	public void setTprptCarOwnerDto(TpTitemApplicant tprptCarOwnerDto) {
		this.tprptCarOwnerDto = tprptCarOwnerDto;
	}
	public void setTprptApplicantDto(TpTitemApplicant tprptApplicantDto) {
		this.tprptApplicantDto = tprptApplicantDto;
	}
	public void setTprptInsuredDto(TpTitemApplicant tprptInsuredDto) {
		this.tprptInsuredDto = tprptInsuredDto;
	}
	public void setPrpTcarshipTaxDto(TpTitemTax prpTcarshipTaxDto) {
		PrpTcarshipTaxDto = prpTcarshipTaxDto;
	}
	public void setTprpTitemKindListDto(List<TpTitemKind> tprpTitemKindListDto) {
		this.tprpTitemKindListDto = tprpTitemKindListDto;
	}
	public void setTprptEngageDTOList(List<TpTitemEngage> tprptEngageDTOList) {
		this.tprptEngageDTOList = tprptEngageDTOList;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((PrpTcarshipTaxDto == null) ? 0 : PrpTcarshipTaxDto
						.hashCode());
		result = prime * result
				+ ((mtplPolicyNo == null) ? 0 : mtplPolicyNo.hashCode());
		result = prime * result
				+ ((policyNo == null) ? 0 : policyNo.hashCode());
		result = prime * result
				+ ((renewalProposalnos == null) ? 0 : renewalProposalnos.hashCode());
		result = prime * result
				+ ((renewalFlag == null) ? 0 : renewalFlag.hashCode());
		result = prime * result
				+ ((tprpTitemCarDto == null) ? 0 : tprpTitemCarDto.hashCode());
		result = prime
				* result
				+ ((tprpTitemKindListDto == null) ? 0 : tprpTitemKindListDto
						.hashCode());
		result = prime * result
				+ ((tprpTmainDto == null) ? 0 : tprpTmainDto.hashCode());
		result = prime
				* result
				+ ((tprptApplicantDto == null) ? 0 : tprptApplicantDto
						.hashCode());
		result = prime
				* result
				+ ((tprptCarOwnerDto == null) ? 0 : tprptCarOwnerDto.hashCode());
		result = prime
				* result
				+ ((tprptEngageDTOList == null) ? 0 : tprptEngageDTOList
						.hashCode());
		result = prime * result
				+ ((tprptInsuredDto == null) ? 0 : tprptInsuredDto.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "TQueryDocumentResponseDto [renewalFlag=" + renewalFlag
				+ ", renewalProposalnos=" + renewalProposalnos + ", tprpTmainDto="
				+ tprpTmainDto + ", policyNo=" + policyNo + ", mtplPolicyNo="
				+ mtplPolicyNo + ", tprpTitemCarDto=" + tprpTitemCarDto
				+ ", tprptCarOwnerDto=" + tprptCarOwnerDto
				+ ", tprptApplicantDto=" + tprptApplicantDto
				+ ", tprptInsuredDto=" + tprptInsuredDto
				+ ", PrpTcarshipTaxDto=" + PrpTcarshipTaxDto
				+ ", tprpTitemKindListDto=" + tprpTitemKindListDto
				+ ", tprptEngageDTOList=" + tprptEngageDTOList + "]";
	}
	
}