package cn.com.libertymutual.production.model.nomorcldatasource;

public class FhexitemkindKey {
    private String treatyno;

    private String sectionno;

    private String riskcode;

    private String itemkind;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public String getSectionno() {
        return sectionno;
    }

    public void setSectionno(String sectionno) {
        this.sectionno = sectionno == null ? null : sectionno.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getItemkind() {
        return itemkind;
    }

    public void setItemkind(String itemkind) {
        this.itemkind = itemkind == null ? null : itemkind.trim();
    }
}