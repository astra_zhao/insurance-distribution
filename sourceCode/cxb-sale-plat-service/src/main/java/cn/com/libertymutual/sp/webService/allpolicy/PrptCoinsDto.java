
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptCoinsDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptCoinsDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chiefFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coinsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coinsName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coinsRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coinsType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proportionFlag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proportionFlag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sameToPolicyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptCoinsDto", propOrder = {
    "chiefFlag",
    "coinsCode",
    "coinsName",
    "coinsRate",
    "coinsType",
    "proportionFlag1",
    "proportionFlag3",
    "sameToPolicyNo"
})
public class PrptCoinsDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String chiefFlag;
    protected String coinsCode;
    protected String coinsName;
    protected String coinsRate;
    protected String coinsType;
    protected String proportionFlag1;
    protected String proportionFlag3;
    protected String sameToPolicyNo;

    /**
     * Gets the value of the chiefFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiefFlag() {
        return chiefFlag;
    }

    /**
     * Sets the value of the chiefFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiefFlag(String value) {
        this.chiefFlag = value;
    }

    /**
     * Gets the value of the coinsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsCode() {
        return coinsCode;
    }

    /**
     * Sets the value of the coinsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsCode(String value) {
        this.coinsCode = value;
    }

    /**
     * Gets the value of the coinsName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsName() {
        return coinsName;
    }

    /**
     * Sets the value of the coinsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsName(String value) {
        this.coinsName = value;
    }

    /**
     * Gets the value of the coinsRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsRate() {
        return coinsRate;
    }

    /**
     * Sets the value of the coinsRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsRate(String value) {
        this.coinsRate = value;
    }

    /**
     * Gets the value of the coinsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoinsType() {
        return coinsType;
    }

    /**
     * Sets the value of the coinsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoinsType(String value) {
        this.coinsType = value;
    }

    /**
     * Gets the value of the proportionFlag1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProportionFlag1() {
        return proportionFlag1;
    }

    /**
     * Sets the value of the proportionFlag1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProportionFlag1(String value) {
        this.proportionFlag1 = value;
    }

    /**
     * Gets the value of the proportionFlag3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProportionFlag3() {
        return proportionFlag3;
    }

    /**
     * Sets the value of the proportionFlag3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProportionFlag3(String value) {
        this.proportionFlag3 = value;
    }

    /**
     * Gets the value of the sameToPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSameToPolicyNo() {
        return sameToPolicyNo;
    }

    /**
     * Sets the value of the sameToPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSameToPolicyNo(String value) {
        this.sameToPolicyNo = value;
    }

}
