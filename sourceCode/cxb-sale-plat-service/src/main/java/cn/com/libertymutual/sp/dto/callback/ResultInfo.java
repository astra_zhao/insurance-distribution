package cn.com.libertymutual.sp.dto.callback;

public class ResultInfo {
	private String orderId;
	private String circPaymentNo;
	private String status;
	private String resultDate;
	private String message;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCircPaymentNo() {
		return circPaymentNo;
	}
	public void setCircPaymentNo(String circPaymentNo) {
		this.circPaymentNo = circPaymentNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResultDate() {
		return resultDate;
	}
	public void setResultDate(String resultDate) {
		this.resultDate = resultDate;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
