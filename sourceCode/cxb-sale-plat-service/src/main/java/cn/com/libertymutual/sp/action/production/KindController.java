package cn.com.libertymutual.sp.action.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.pojo.request.LinkItemRequest;
import cn.com.libertymutual.production.pojo.request.LinkRiskRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemRequest;
import cn.com.libertymutual.production.pojo.request.PrpdKindLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdKindRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.KindBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("kind")
public class KindController {
	
	@Autowired
	protected KindBusinessService kindBussinessService;
	
	/**
	 * 获取条款基本信息
	 * @param PrpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("queryPrpdKindLibrary")
	public Response queryPrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result =  kindBussinessService.findPrpdKindLibrary(prpdKindLibraryRequest);
		return result;
	}
	
	/**
	 * 条款管理-获取标的责任
	 * @param PrpdRiskPlanRequest
	 * @return
	 */
	@RequestMapping("queryPrpdItemLibrary")
	public Response queryPrpdItemLibrary(@RequestBody PrpdItemLibraryRequest prpdItemLibraryRequest) {
		Response result = kindBussinessService.findPrpdItemLibrary(prpdItemLibraryRequest);
		return result;
	}
	
	/**
	 * 生成条款代码
	 * @param ownerriskcode
	 * @param kindType
	 * @return
	 */
	@RequestMapping("generateKindCode")
	public Response generateKindCode(String ownerriskcode, int kindType) {
		Response result = new Response();
		try {
			result = kindBussinessService.getNewKindSerialno(ownerriskcode, kindType);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 新增条款
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("addKind")
	public Response addKind(PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		try {
			String kindcode = kindBussinessService.getNewKindSerialno(
											prpdKindLibraryRequest.getOwnerriskcode(),
											prpdKindLibraryRequest.getKindType()).getResult().toString();
			prpdKindLibraryRequest.setKindcode(kindcode);
			result = kindBussinessService.insertPrpdKindLibrary(prpdKindLibraryRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 修改条款
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("updateKind")
	public Response updateKind(PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		try {
			result = kindBussinessService.updatePrpdKindLibrary(prpdKindLibraryRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 检验最高版本
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("isLastKindVersion")
	public Response isLastKindVersion(@RequestBody PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = kindBussinessService.isLastKindVersion(prpdKindLibraryRequest);
		return result;
	}
	
	/**
	 * 注销条款
	 * @param list
	 * @return
	 */
	@RequestMapping("inActiveKindVersion")
	public Response inActiveKindVersion(@RequestBody PrpdKindRequest request) {
		Response result = new Response();
		try {
			result = kindBussinessService.inActiveKindVersion(request.getSelectedRows());
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 关联条款-标的/责任
	 * @param linkItemRequestPrpdKindItemRequest.java
	 * @return
	 */
	@RequestMapping("linkItem")
	public Response linkItem(@RequestBody LinkItemRequest linkItemRequest) {
		Response result = new Response();
		try {
			result = kindBussinessService.linkItem(linkItemRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 生成标的代码
	 * @return
	 */
	@RequestMapping("generateItemCode")
	public Response generateItemCode() {
		Response result = new Response();
		try {
			result = kindBussinessService.getNewItemCode();
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 新增标的
	 * @param prpdItemLibraryRequest
	 * @return
	 */
	@RequestMapping("addItem")
	public Response addItem(@RequestBody PrpdItemLibraryRequest prpdItemLibraryRequest) {
		Response result = new Response();
		try {
			String itemcode = kindBussinessService.getNewItemCode().getResult().toString();
			prpdItemLibraryRequest.setItemcode(itemcode);
			result = kindBussinessService.insertPrpdItemLibrary(prpdItemLibraryRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 条款关联险种
	 * @param request
	 * @return
	 */
	@RequestMapping("linkRisk")
	public Response linkRisk(@RequestBody LinkRiskRequest request) {
		Response result = new Response();
		try {
			result = kindBussinessService.linkRisk(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 查看已关联标的
	 * @param prpdKindItemRequest
	 * @return
	 */
	@RequestMapping("getKindItem")
	public Response getKindItem(@RequestBody LinkItemRequest linkItemRequest) {
		return kindBussinessService.findItemLinked(linkItemRequest);
	}
	
	/**
	 * 删除已关联标的
	 * @param linkItemRequest
	 * @return
	 */
	@RequestMapping("delItemLinked")
	public Response delItemLinked(@RequestBody LinkItemRequest linkItemRequest) {
		Response result = new Response();
		try {
			result = kindBussinessService.delItemLinked(linkItemRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 查看已关联险种
	 * @param prpdKindItemRequest
	 * @return
	 */
	@RequestMapping("getPrpdKind")
	public Response getPrpdKind(@RequestBody LinkRiskRequest linkRiskRequest) {
		return kindBussinessService.findRiskLinked(linkRiskRequest);
	}
	
	/**
	 * 删除已关联的险种
	 * @param linkRiskRequest
	 * @return
	 */
	@RequestMapping("delRiskLinked")
	public Response delRiskLinked(@RequestBody LinkRiskRequest linkRiskRequest) {
		Response result = new Response();
		try {
			result = kindBussinessService.delRiskLinked(linkRiskRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 条款关联标的时，验证是否已关联险种
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("checkRiskLinked")
	public Response checkRiskLinked(@RequestBody PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		result = kindBussinessService.checkRiskLinked(prpdKindLibraryRequest);
		return result;
	}
	
	/**
	 * 条款关联标的时，验证已关联险种下是否有标的
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	@RequestMapping("checkItemLinked")
	public Response checkItemLinked(@RequestBody PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		result = kindBussinessService.checkItemLinked(prpdKindLibraryRequest);
		return result;
	}
	
	/**
	 * 获取还未关联险种的标的责任
	 * @param prpdItemRequest
	 * @return
	 */
	@RequestMapping("findItemNotRiskLinked")
	public Response findItemNotRiskLinked(@RequestBody PrpdItemRequest prpdItemRequest) {
		Response result = new Response();
		result = kindBussinessService.findItemNotRiskLinked(prpdItemRequest);
		return result;
	}

	/**
	 * 险种关联标的/责任
	 * @param prpdItemRequest
	 * @return
	 */
	@RequestMapping("linkItem2Risk")
	public Response linkItem2Risk(@RequestBody PrpdItemRequest prpdItemRequest) {
		Response result = new Response();
		try {
			result = kindBussinessService.linkItem2Risk(prpdItemRequest);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 条款管理-取消已关联险种时，校验条款/险种是否被方案使用
	 * @param prpdItemRequest
	 * @return
	 */
	@RequestMapping("checkRiskLinkedDeletable")
	public Response checkRiskLinkedDeletable(@RequestBody LinkRiskRequest request) {
		return kindBussinessService.checkRiskLinkedDeletable(request);
	}
	
	/**
	 * 条款管理-取消已关联责任时，校验条款/责任是否被方案使用
	 * @param prpdItemRequest
	 * @return
	 */
	@RequestMapping("checkItemLinkedDeletable")
	public Response checkItemLinkedDeletable(@RequestBody LinkItemRequest request) {
		return kindBussinessService.checkItemLinkedDeletable(request);
	}
	
	/**
	 * 条款管理-校验是否有方案在使用该条款
	 * @param prpdItemRequest
	 * @return
	 */
	@RequestMapping("checkKindDeletable")
	public Response checkKindDeletable(@RequestBody PrpdKindRequest request) {
		return kindBussinessService.checkKindDeletable(request.getSelectedRows());
	}
	
	/**
	 * 条款关联险种-获取短期费率信息
	 * @return
	 */
	@RequestMapping("queryShortRate")
	public Response queryShortRate() {
		Response result = new Response();
		result = kindBussinessService.findAllShortRate();
		return result;
	}
	
	/**
	 * 根据归属险类查询对应的条款
	 * @param request
	 * @return
	 */
	@RequestMapping("queryByOwnerRisk")
	public Response queryByOwnerRisk(@RequestBody PrpdKindLibraryRequest request) {
		Response result = kindBussinessService.findByOwnerRisk(request);
		return result;
	}
	
}
