package cn.com.libertymutual.production.service.api;


import cn.com.libertymutual.production.model.nomorcldatasource.Prpdpolicypay;
import cn.com.libertymutual.production.pojo.request.PrpdPolicyPayRequest;
import com.github.pagehelper.PageInfo;

public interface PrpdPolicyPayService {
	
	/**
	 * 新增见费出单
	 * @param record
	 */
	void insert(Prpdpolicypay record);

	/**
	 * 修改见费出单
	 * @param record
	 */
	void update(Prpdpolicypay record);

	/**
	 * 删除见费出单
	 * @param record
	 */
	void delete(Prpdpolicypay criteria);

	PageInfo<Prpdpolicypay> selectByPage(PrpdPolicyPayRequest prpdPolicyPayRequest);
}
