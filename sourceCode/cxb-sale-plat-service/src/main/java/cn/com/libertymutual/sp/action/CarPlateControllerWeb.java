package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.CarPlateService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/carPlate")
public class CarPlateControllerWeb {

	@Autowired
	private CarPlateService carPlateService;
	
	
	/*
	 * 限制车牌查询
	 */
	@ApiOperation(value = "限制车牌查询", notes = "限制车牌查询")
	@PostMapping(value = "/plateList")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult plateList(Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = carPlateService.plateList(pageNumber, pageSize);
		return sr;

	}
	
	
	/*
	 * 添加限制车牌查询
	 */
	@ApiOperation(value = "添加限制车牌查询", notes = "添加限制车牌查询")
	@PostMapping(value = "/addPlate")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "plateNo", value = "机构", required = true, paramType = "query", dataType = "String"),
	})
	public ServiceResult addPlate(String plateNo) {
		ServiceResult sr = new ServiceResult();

		sr = carPlateService.addPlate(plateNo);
		return sr;

	}
	
	
	/*
	 * 删除限制车牌
	 */
	@ApiOperation(value = "删除限制车牌", notes = "删除限制车牌")
	@PostMapping(value = "/removePlate")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "plateNo", value = "机构", required = true, paramType = "query", dataType = "String"),
	})
	public ServiceResult removePlate(String plateNo) {
		ServiceResult sr = new ServiceResult();

		sr = carPlateService.removePlate(plateNo);
		return sr;

	}
	
	
	
	
}
