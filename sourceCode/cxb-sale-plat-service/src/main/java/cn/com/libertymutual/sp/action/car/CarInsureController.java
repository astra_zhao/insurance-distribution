package cn.com.libertymutual.sp.action.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.car.dto.QueryVehicleRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TRenewalQueryRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TVerifyRequstDTO;
import cn.com.libertymutual.sp.dto.queryplans.QuerySPPlansRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;
import cn.com.libertymutual.sp.service.api.car.CarInsureService;

@RestController
@RequestMapping(value = "/nol/car")
public class CarInsureController {
	@Autowired
	private CarInsureService carInsureService;

	/**
	 * 续保车辆查询
	 * @param trRequest
	 */
	@RequestMapping(value = "/saleRenewalQuery")
	public ServiceResult saleRenewalQuery(@RequestBody TRenewalQueryRequestDto trRequest) {
		return carInsureService.renewalQuery(trRequest);
	}

	/**
	 * 投保车辆信息查询
	 * @param trRequest
	 */
	@RequestMapping(value = "/saleQueryVehicle")
	public ServiceResult saleQueryVehicle(@RequestBody QueryVehicleRequestDto venDto) {
		return carInsureService.queryVehicle(venDto);
	}

	/**
	 * VIN码查询
	 * @param trRequest
	 */
	@RequestMapping(value = "/saleQueryVin")
	public ServiceResult saleQueryVin(@RequestBody QueryVehicleRequestDto venDto) {
		return carInsureService.queryVin(venDto);
	}

	/**
	 * 保费计算接口
	 */
	@RequestMapping(value = "/combinecalculate")
	public ServiceResult combinecalculate(@RequestBody PropsalSaveRequestDto requestDto) {
		return carInsureService.combinecalculate(requestDto);
	}

	/**
	 * 提交核保 preminumCommit
	 */
	@RequestMapping(value = "/preminumCommit")
	public ServiceResult preminumCommit(@RequestBody PropsalSaveRequestDto requestDto) {
		return carInsureService.preminumCommit(requestDto);
	}

	/**
	 * 交叉销售计划查询
	 */
	@RequestMapping(value = "/crossSalePlan")
	public ServiceResult crossSalePlan(Integer seatCount, Integer age, String agreementCode) {
		return carInsureService.CrossSalePlan(seatCount, age, agreementCode);
	}

	/**
	 * 交叉销售计划保存
	 */
	@RequestMapping(value = "/crossSalePlanSave")
	public ServiceResult crossSalePlanSave(@RequestBody QuerySPPlansRequestDTO requestDto) {
		return carInsureService.CrossSalePlanSave(requestDto);
	}

	/**
	 * 转保
	 */
	@RequestMapping(value = "/verify")
	public ServiceResult verify(@RequestBody TVerifyRequstDTO tVerifyRequstDTO) {
		return carInsureService.Verify(tVerifyRequstDTO);
	}

	/**
	 * 报价页面初始化
	 */
	@RequestMapping(value = "/initOffer")
	public ServiceResult initOffer() {
		return null;
	}

}
