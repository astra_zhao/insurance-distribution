package cn.com.libertymutual.sp.service.api.car;

import cn.com.libertymutual.core.web.ServiceResult;

public interface CarInitService {
	/**
	 * 报价页面初始化信息
	 * @param venDto
	 * @return
	 */
	ServiceResult getInitOffer(String riskCode);

	/**
	 * 获得选择初始化信息
	 * @param riskCode
	 * @return
	 */
	ServiceResult querySeleInit(String riskCode, String branchCode);

	/**
	 * 根据车主类型.车辆类型,险种,来查询车辆使用性质
	 * @param insuredNature
	 * @param carKind
	 * @param riskCode
	 * @return
	 */
	ServiceResult queryUseNature(String insuredNature, String carKind, String riskCode);
}
