package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseWithBLOBs;

public interface PrpdRiskClauseService {

	/**
	 * 新增特约时保存默认带出险种，默认带出分公司，适用方案
	 * @param record
	 */
	public void insert(PrpdriskclauseWithBLOBs record);
	
	/**
	 * 根据特约查询默认带出险种、默认带出分公司、适用计划
	 * @param clauseCode
	 * @return
	 */
	public List<PrpdriskclauseWithBLOBs> findByCluaseCode(String clauseCode);
	
	public void update(PrpdriskclauseWithBLOBs record);
	
	public void delete(Prpdriskclause record);
	
	public List<PrpdriskclauseWithBLOBs> findRiskClauseByCondition(String clausecode);
}
