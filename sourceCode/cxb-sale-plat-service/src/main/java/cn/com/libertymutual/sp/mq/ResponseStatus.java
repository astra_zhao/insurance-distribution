package cn.com.libertymutual.sp.mq;

public enum ResponseStatus {
	OK("0000"),
	ERROR("5000");
	

	
	private String responseStatus;
	private ResponseStatus( String responseStatus ) {
		this.responseStatus = responseStatus;
	}
	
	public String getResponseStatus() {
		return responseStatus;
	}
	
}
