package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.PosterService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/poster")
public class PosterController {
	@Autowired
	private PosterService posterService;

	/*
	 * 海报查询
	 */
	@ApiOperation(value = "海报查询", notes = "海报查询")
	@PostMapping(value = "/allPoster")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "branchCode", value = "机构", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "serchKey", value = "文章名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "文章类型", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult allPoster(String branchCode, String serchKey, String type, Integer pageNumber, Integer pageSize, String userCode) {
		ServiceResult sr = new ServiceResult();

		sr = posterService.allPoster(branchCode, serchKey, type, pageNumber, pageSize, Current.userCode.get());
		return sr;

	}

	@ApiOperation(value = "海报查询", notes = "海报查询")
	@PostMapping(value = "/findOnePoster")
	public ServiceResult findOnePoster(@RequestParam("id") Integer id) {
		ServiceResult sr = new ServiceResult();
		sr = posterService.findPoster(id);
		return sr;
	}
}
