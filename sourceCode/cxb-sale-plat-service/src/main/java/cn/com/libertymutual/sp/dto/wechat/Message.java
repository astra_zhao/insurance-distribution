package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entity - 接收到的微信消息
 * 
 * @author Ze.Li
 *
 */
@Entity
@Table(name = "t_message")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class Message implements Serializable{

	private static final long serialVersionUID = 3906529505361737405L;
	  
	
	private long id;
	/** 开发者微信号 */
	private String toUserName;
	
	/** 发送方帐号（一个OpenID） */
	private String fromUserName;
	
	/** 消息创建时间 （整型） */
	private Date createTime;
	
	/** 
	 * 消息类型(text-文本消息，image-图片消息, voice-语音消息, 
	 * video-视频消息, location-地理位置消息, link-链接消息, event-事件消息) 
	 */
	private String msgType;
	
	/** 消息id，64位整型 */
	private String msgId;
	
	/** 文本消息内容 */
	private String content;
	
	/** 消息媒体id，可以调用多媒体文件下载接口拉取数据 */
	private String mediaId;
	
	/** 消息媒体文件路径，接收媒体消息时需要把文件下载到本地 */
	private String mediaPath;
	
	/** 图片链接 */
	private String picUrl;
	
	/** 语音格式，如amr，speex等 */
	private String format;
	
	/** 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据 */
	private String thumbMediaId;
	
	/** 地理位置维度 */
	private Double location_x;
	
	/** 地理位置经度 */
	private Double location_y;
	
	/** 地图缩放大小 */
	private Integer scale;
	
	/** 地理位置信息 */
	private String label;
	
	/** 消息标题 */
	private String title;
	
	/** 消息描述 */
	private String description;
	
	/** 消息链接 */
	private String url;
	
	/** 
	 * 事件类型
	 * subscribe-订阅，
	 * unsubscribe-取消订阅， 
	 * SCAN-已关注扫描二维码事件推送， 
	 * LOCATION-上报地理位置事件，
	 * CLICK-点击菜单拉取消息时的事件推送，
	 * VIEW-点击菜单跳转链接时的事件推送) 
	 */
	private String event;
	
	/**
	 * 事件KEY值
	 * event: eventKey
	 * subscribe: qrscene_为前缀，后面为二维码的参数值，
	 * SCAN: 是一个32位无符号整数，即创建二维码时的二维码scene_id 
	 * CLICK: 与自定义菜单接口中KEY值对应
	 * VIEW: 设置的跳转URL 
	 */
	private String eventKey;
	
	/** 二维码的ticket，可用来换取二维码图片 */
	private String ticket;
	
	/** 地理位置纬度 */
	private Double Latitude;
	
	/** 地理位置经度 */
	private Double Longitude;
	
	/** 地理位置精度 */
	private Double Precision;
	
	/** 消息状态(0-未回复，1-已回复) */
	private Integer status;
	
	/** 是否星标消息(0-否，1-是) */
	private Integer isStar;
	
	/** 消息是否自动回复(0-否，1-是) */
	private Integer isAutoReply;

	/** 微信发消息用户 */
	private CommonUserInfo user;
	
	/** 公众号ID */
	private Long publicId;

	@NotNull
	@Column(nullable = false, length = 100)
	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	@NotNull
	@Column(nullable = false, length = 100)
	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	@NotNull
	@Column(nullable = false)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Id
	 @Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@NotNull
	@Column(nullable = false, length = 50)
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	@NotNull
	@Column(nullable = false, length = 255)
	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Column(length = 500)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(length = 255)
	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Column(length = 255)
	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}
	
	@Column(length = 255)
	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	@Column(length = 20)
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Column(length = 255)
	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public Double getLocation_x() {
		return location_x;
	}

	public void setLocation_x(Double location_x) {
		this.location_x = location_x;
	}

	public Double getLocation_y() {
		return location_y;
	}

	public void setLocation_y(Double location_y) {
		this.location_y = location_y;
	}

	public Integer getScale() {
		return scale;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	@Column(length = 255)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Column(length = 255)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(length = 500)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length = 255)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(length = 20)
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	@Column(length = 255)
	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	@Column(length = 255)
	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Double getLatitude() {
		return Latitude;
	}

	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}

	public Double getLongitude() {
		return Longitude;
	}

	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}

	public Double getPrecision() {
		return Precision;
	}

	public void setPrecision(Double precision) {
		Precision = precision;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsStar() {
		return isStar;
	}

	public void setIsStar(Integer isStar) {
		this.isStar = isStar;
	}

	public Integer getIsAutoReply() {
		return isAutoReply;
	}

	public void setIsAutoReply(Integer isAutoReply) {
		this.isAutoReply = isAutoReply;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "userId")
	public CommonUserInfo getUser() {
		return user;
	}

	public void setUser(CommonUserInfo user) {
		this.user = user;
	}

	public Long getPublicId() {
		return publicId;
	}

	public void setPublicId(Long publicId) {
		this.publicId = publicId;
	}
	
}
