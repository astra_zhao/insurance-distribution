package cn.com.libertymutual.core.security.jwt;

import cn.com.libertymutual.sp.bean.TbSpUser;


public final class JwtUserFactory {

    private JwtUserFactory() {
    }
    
    public static JwtUser create(TbSpUser user) {
        return new JwtUser(
                (long)user.getId(),
                user.getUserCode(),
                user.getUserCodeBs(),
                user.getWxOpenId(),
                user.getUserName(),
                user.getNickName(),
                user.getPassword(),
                user.getEmail(),
                user.getUserType(),
                null,
                "1".equals(user.getState()) ? true : false,
                user.getLastloginDate()
        );
    }
    

    
    public static JwtUser create(String userCode, String userCodeBs) {
        return new JwtUser(
                0l,
                userCode,
                userCodeBs,
                null,	//user.getWxOpenId(),
                userCodeBs,	//user.getUserName(),
                null,	//user.getNickName(),
                null,	//user.getPassword(),
                null,	//user.getEmail(),
                null,	//user.getUserType(),
                null,
                true,	//"1".equals(user.getState()) ? true : false,
                null	//user.getLastloginDate()
        );
    }

    /*private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
                .collect(Collectors.toList());
    }*/
}
