
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mtplPolicyDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mtplPolicyDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prpTcarDeviceDto" type="{http://service.liberty.com/common/bean}prpTcarDeviceDto" minOccurs="0"/>
 *         &lt;element name="prpTcarOwnerDto" type="{http://service.liberty.com/common/bean}prpTcarOwnerDto" minOccurs="0"/>
 *         &lt;element name="prpTcarShipTaxDto" type="{http://service.liberty.com/common/bean}prpTcarShipTaxDto" minOccurs="0"/>
 *         &lt;element name="prpTitemCarDto" type="{http://service.liberty.com/common/bean}prpTitemCarDto" minOccurs="0"/>
 *         &lt;element name="prpTitemCarExtDto" type="{http://service.liberty.com/common/bean}prpTitemCarExtDto" minOccurs="0"/>
 *         &lt;element name="prpTmainSubDto" type="{http://service.liberty.com/common/bean}prpTmainSubDto" minOccurs="0"/>
 *         &lt;element name="prpTprofitDetailDto" type="{http://service.liberty.com/common/bean}prpTprofitDetailDto" minOccurs="0"/>
 *         &lt;element name="prptAdjustDtoList" type="{http://service.liberty.com/common/bean}prptAdjustDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptApplicantDto" type="{http://service.liberty.com/common/bean}prptApplicantDto" minOccurs="0"/>
 *         &lt;element name="prptBeneficiaryDto" type="{http://service.liberty.com/common/bean}prptBeneficiaryDto" minOccurs="0"/>
 *         &lt;element name="prptCoinsDto" type="{http://service.liberty.com/common/bean}prptCoinsDto" minOccurs="0"/>
 *         &lt;element name="prptCommissionDtoList" type="{http://service.liberty.com/common/bean}prptCommissionDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptEngageDtoList" type="{http://service.liberty.com/common/bean}prptEngageDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptFeeDto" type="{http://service.liberty.com/common/bean}prptFeeDto" minOccurs="0"/>
 *         &lt;element name="prptInsuredDtoList" type="{http://service.liberty.com/common/bean}prptInsuredDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptItemKindDtoList" type="{http://service.liberty.com/common/bean}prptItemKindDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptLimitList" type="{http://service.liberty.com/common/bean}prptLimitDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptMainDto" type="{http://service.liberty.com/common/bean}prptMainDto" minOccurs="0"/>
 *         &lt;element name="prptMainpropDtoList" type="{http://service.liberty.com/common/bean}prptMainPropDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptPlanDtoList" type="{http://service.liberty.com/common/bean}prptPlanDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptReinscededDto" type="{http://service.liberty.com/common/bean}prptReinscededDto" minOccurs="0"/>
 *         &lt;element name="prptcarDriverDto" type="{http://service.liberty.com/common/bean}prptcarDriverDto" minOccurs="0"/>
 *         &lt;element name="prptexpCarInfoDto" type="{http://service.liberty.com/common/bean}prptexpCarInfoDto" minOccurs="0"/>
 *         &lt;element name="prptinsurednatureDto" type="{http://service.liberty.com/common/bean}prptinsurednatureDto" minOccurs="0"/>
 *         &lt;element name="prptprofitDto" type="{http://service.liberty.com/common/bean}prptprofitDto" minOccurs="0"/>
 *         &lt;element name="requestHeadDto" type="{http://service.liberty.com/common/bean}requestHeadDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mtplPolicyDto", propOrder = {
    "prpTcarDeviceDto",
    "prpTcarOwnerDto",
    "prpTcarShipTaxDto",
    "prpTitemCarDto",
    "prpTitemCarExtDto",
    "prpTmainSubDto",
    "prpTprofitDetailDto",
    "prptAdjustDtoList",
    "prptApplicantDto",
    "prptBeneficiaryDto",
    "prptCoinsDto",
    "prptCommissionDtoList",
    "prptEngageDtoList",
    "prptFeeDto",
    "prptInsuredDtoList",
    "prptItemKindDtoList",
    "prptLimitList",
    "prptMainDto",
    "prptMainpropDtoList",
    "prptPlanDtoList",
    "prptReinscededDto",
    "prptcarDriverDto",
    "prptexpCarInfoDto",
    "prptinsurednatureDto",
    "prptprofitDto",
    "requestHeadDto"
})
public class MtplPolicyDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected PrpTcarDeviceDto prpTcarDeviceDto;
    protected PrpTcarOwnerDto prpTcarOwnerDto;
    protected PrpTcarShipTaxDto prpTcarShipTaxDto;
    protected PrpTitemCarDto prpTitemCarDto;
    protected PrpTitemCarExtDto prpTitemCarExtDto;
    protected PrpTmainSubDto prpTmainSubDto;
    protected PrpTprofitDetailDto prpTprofitDetailDto;
    @XmlElement(nillable = true)
    protected List<PrptAdjustDto> prptAdjustDtoList;
    protected PrptApplicantDto prptApplicantDto;
    protected PrptBeneficiaryDto prptBeneficiaryDto;
    protected PrptCoinsDto prptCoinsDto;
    @XmlElement(nillable = true)
    protected List<PrptCommissionDto> prptCommissionDtoList;
    @XmlElement(nillable = true)
    protected List<PrptEngageDto> prptEngageDtoList;
    protected PrptFeeDto prptFeeDto;
    @XmlElement(nillable = true)
    protected List<PrptInsuredDto> prptInsuredDtoList;
    @XmlElement(nillable = true)
    protected List<PrptItemKindDto> prptItemKindDtoList;
    @XmlElement(nillable = true)
    protected List<PrptLimitDto> prptLimitList;
    protected PrptMainDto prptMainDto;
    @XmlElement(nillable = true)
    protected List<PrptMainPropDto> prptMainpropDtoList;
    @XmlElement(nillable = true)
    protected List<PrptPlanDto> prptPlanDtoList;
    protected PrptReinscededDto prptReinscededDto;
    protected PrptcarDriverDto prptcarDriverDto;
    protected PrptexpCarInfoDto prptexpCarInfoDto;
    protected PrptinsurednatureDto prptinsurednatureDto;
    protected PrptprofitDto prptprofitDto;
    protected RequestHeadDto requestHeadDto;

    /**
     * Gets the value of the prpTcarDeviceDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTcarDeviceDto }
     *     
     */
    public PrpTcarDeviceDto getPrpTcarDeviceDto() {
        return prpTcarDeviceDto;
    }

    /**
     * Sets the value of the prpTcarDeviceDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTcarDeviceDto }
     *     
     */
    public void setPrpTcarDeviceDto(PrpTcarDeviceDto value) {
        this.prpTcarDeviceDto = value;
    }

    /**
     * Gets the value of the prpTcarOwnerDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTcarOwnerDto }
     *     
     */
    public PrpTcarOwnerDto getPrpTcarOwnerDto() {
        return prpTcarOwnerDto;
    }

    /**
     * Sets the value of the prpTcarOwnerDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTcarOwnerDto }
     *     
     */
    public void setPrpTcarOwnerDto(PrpTcarOwnerDto value) {
        this.prpTcarOwnerDto = value;
    }

    /**
     * Gets the value of the prpTcarShipTaxDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTcarShipTaxDto }
     *     
     */
    public PrpTcarShipTaxDto getPrpTcarShipTaxDto() {
        return prpTcarShipTaxDto;
    }

    /**
     * Sets the value of the prpTcarShipTaxDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTcarShipTaxDto }
     *     
     */
    public void setPrpTcarShipTaxDto(PrpTcarShipTaxDto value) {
        this.prpTcarShipTaxDto = value;
    }

    /**
     * Gets the value of the prpTitemCarDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTitemCarDto }
     *     
     */
    public PrpTitemCarDto getPrpTitemCarDto() {
        return prpTitemCarDto;
    }

    /**
     * Sets the value of the prpTitemCarDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTitemCarDto }
     *     
     */
    public void setPrpTitemCarDto(PrpTitemCarDto value) {
        this.prpTitemCarDto = value;
    }

    /**
     * Gets the value of the prpTitemCarExtDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTitemCarExtDto }
     *     
     */
    public PrpTitemCarExtDto getPrpTitemCarExtDto() {
        return prpTitemCarExtDto;
    }

    /**
     * Sets the value of the prpTitemCarExtDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTitemCarExtDto }
     *     
     */
    public void setPrpTitemCarExtDto(PrpTitemCarExtDto value) {
        this.prpTitemCarExtDto = value;
    }

    /**
     * Gets the value of the prpTmainSubDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTmainSubDto }
     *     
     */
    public PrpTmainSubDto getPrpTmainSubDto() {
        return prpTmainSubDto;
    }

    /**
     * Sets the value of the prpTmainSubDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTmainSubDto }
     *     
     */
    public void setPrpTmainSubDto(PrpTmainSubDto value) {
        this.prpTmainSubDto = value;
    }

    /**
     * Gets the value of the prpTprofitDetailDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTprofitDetailDto }
     *     
     */
    public PrpTprofitDetailDto getPrpTprofitDetailDto() {
        return prpTprofitDetailDto;
    }

    /**
     * Sets the value of the prpTprofitDetailDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTprofitDetailDto }
     *     
     */
    public void setPrpTprofitDetailDto(PrpTprofitDetailDto value) {
        this.prpTprofitDetailDto = value;
    }

    /**
     * Gets the value of the prptAdjustDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptAdjustDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptAdjustDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptAdjustDto }
     * 
     * 
     */
    public List<PrptAdjustDto> getPrptAdjustDtoList() {
        if (prptAdjustDtoList == null) {
            prptAdjustDtoList = new ArrayList<PrptAdjustDto>();
        }
        return this.prptAdjustDtoList;
    }

    /**
     * Gets the value of the prptApplicantDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptApplicantDto }
     *     
     */
    public PrptApplicantDto getPrptApplicantDto() {
        return prptApplicantDto;
    }

    /**
     * Sets the value of the prptApplicantDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptApplicantDto }
     *     
     */
    public void setPrptApplicantDto(PrptApplicantDto value) {
        this.prptApplicantDto = value;
    }

    /**
     * Gets the value of the prptBeneficiaryDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptBeneficiaryDto }
     *     
     */
    public PrptBeneficiaryDto getPrptBeneficiaryDto() {
        return prptBeneficiaryDto;
    }

    /**
     * Sets the value of the prptBeneficiaryDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptBeneficiaryDto }
     *     
     */
    public void setPrptBeneficiaryDto(PrptBeneficiaryDto value) {
        this.prptBeneficiaryDto = value;
    }

    /**
     * Gets the value of the prptCoinsDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptCoinsDto }
     *     
     */
    public PrptCoinsDto getPrptCoinsDto() {
        return prptCoinsDto;
    }

    /**
     * Sets the value of the prptCoinsDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptCoinsDto }
     *     
     */
    public void setPrptCoinsDto(PrptCoinsDto value) {
        this.prptCoinsDto = value;
    }

    /**
     * Gets the value of the prptCommissionDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptCommissionDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptCommissionDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptCommissionDto }
     * 
     * 
     */
    public List<PrptCommissionDto> getPrptCommissionDtoList() {
        if (prptCommissionDtoList == null) {
            prptCommissionDtoList = new ArrayList<PrptCommissionDto>();
        }
        return this.prptCommissionDtoList;
    }

    /**
     * Gets the value of the prptEngageDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptEngageDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptEngageDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptEngageDto }
     * 
     * 
     */
    public List<PrptEngageDto> getPrptEngageDtoList() {
        if (prptEngageDtoList == null) {
            prptEngageDtoList = new ArrayList<PrptEngageDto>();
        }
        return this.prptEngageDtoList;
    }

    /**
     * Gets the value of the prptFeeDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptFeeDto }
     *     
     */
    public PrptFeeDto getPrptFeeDto() {
        return prptFeeDto;
    }

    /**
     * Sets the value of the prptFeeDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptFeeDto }
     *     
     */
    public void setPrptFeeDto(PrptFeeDto value) {
        this.prptFeeDto = value;
    }

    /**
     * Gets the value of the prptInsuredDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptInsuredDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptInsuredDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptInsuredDto }
     * 
     * 
     */
    public List<PrptInsuredDto> getPrptInsuredDtoList() {
        if (prptInsuredDtoList == null) {
            prptInsuredDtoList = new ArrayList<PrptInsuredDto>();
        }
        return this.prptInsuredDtoList;
    }

    /**
     * Gets the value of the prptItemKindDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptItemKindDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptItemKindDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptItemKindDto }
     * 
     * 
     */
    public List<PrptItemKindDto> getPrptItemKindDtoList() {
        if (prptItemKindDtoList == null) {
            prptItemKindDtoList = new ArrayList<PrptItemKindDto>();
        }
        return this.prptItemKindDtoList;
    }

    /**
     * Gets the value of the prptLimitList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptLimitList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptLimitList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptLimitDto }
     * 
     * 
     */
    public List<PrptLimitDto> getPrptLimitList() {
        if (prptLimitList == null) {
            prptLimitList = new ArrayList<PrptLimitDto>();
        }
        return this.prptLimitList;
    }

    /**
     * Gets the value of the prptMainDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptMainDto }
     *     
     */
    public PrptMainDto getPrptMainDto() {
        return prptMainDto;
    }

    /**
     * Sets the value of the prptMainDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptMainDto }
     *     
     */
    public void setPrptMainDto(PrptMainDto value) {
        this.prptMainDto = value;
    }

    /**
     * Gets the value of the prptMainpropDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptMainpropDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptMainpropDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptMainPropDto }
     * 
     * 
     */
    public List<PrptMainPropDto> getPrptMainpropDtoList() {
        if (prptMainpropDtoList == null) {
            prptMainpropDtoList = new ArrayList<PrptMainPropDto>();
        }
        return this.prptMainpropDtoList;
    }

    /**
     * Gets the value of the prptPlanDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptPlanDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptPlanDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptPlanDto }
     * 
     * 
     */
    public List<PrptPlanDto> getPrptPlanDtoList() {
        if (prptPlanDtoList == null) {
            prptPlanDtoList = new ArrayList<PrptPlanDto>();
        }
        return this.prptPlanDtoList;
    }

    /**
     * Gets the value of the prptReinscededDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptReinscededDto }
     *     
     */
    public PrptReinscededDto getPrptReinscededDto() {
        return prptReinscededDto;
    }

    /**
     * Sets the value of the prptReinscededDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptReinscededDto }
     *     
     */
    public void setPrptReinscededDto(PrptReinscededDto value) {
        this.prptReinscededDto = value;
    }

    /**
     * Gets the value of the prptcarDriverDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptcarDriverDto }
     *     
     */
    public PrptcarDriverDto getPrptcarDriverDto() {
        return prptcarDriverDto;
    }

    /**
     * Sets the value of the prptcarDriverDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptcarDriverDto }
     *     
     */
    public void setPrptcarDriverDto(PrptcarDriverDto value) {
        this.prptcarDriverDto = value;
    }

    /**
     * Gets the value of the prptexpCarInfoDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptexpCarInfoDto }
     *     
     */
    public PrptexpCarInfoDto getPrptexpCarInfoDto() {
        return prptexpCarInfoDto;
    }

    /**
     * Sets the value of the prptexpCarInfoDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptexpCarInfoDto }
     *     
     */
    public void setPrptexpCarInfoDto(PrptexpCarInfoDto value) {
        this.prptexpCarInfoDto = value;
    }

    /**
     * Gets the value of the prptinsurednatureDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptinsurednatureDto }
     *     
     */
    public PrptinsurednatureDto getPrptinsurednatureDto() {
        return prptinsurednatureDto;
    }

    /**
     * Sets the value of the prptinsurednatureDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptinsurednatureDto }
     *     
     */
    public void setPrptinsurednatureDto(PrptinsurednatureDto value) {
        this.prptinsurednatureDto = value;
    }

    /**
     * Gets the value of the prptprofitDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptprofitDto }
     *     
     */
    public PrptprofitDto getPrptprofitDto() {
        return prptprofitDto;
    }

    /**
     * Sets the value of the prptprofitDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptprofitDto }
     *     
     */
    public void setPrptprofitDto(PrptprofitDto value) {
        this.prptprofitDto = value;
    }

    /**
     * Gets the value of the requestHeadDto property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeadDto }
     *     
     */
    public RequestHeadDto getRequestHeadDto() {
        return requestHeadDto;
    }

    /**
     * Sets the value of the requestHeadDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeadDto }
     *     
     */
    public void setRequestHeadDto(RequestHeadDto value) {
        this.requestHeadDto = value;
    }

}
