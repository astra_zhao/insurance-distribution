package cn.com.libertymutual.sp.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpKeywordWeChat;
import cn.com.libertymutual.sp.dao.KeywordWeChatDao;
import cn.com.libertymutual.sp.service.api.KeywordWeChatService;

@Service("KeywordWeChatService")
public class KeywordWeChatServiceImpl implements KeywordWeChatService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private KeywordWeChatDao keywordWeChatDao;

	@Resource
	private RedisUtils redisUtils;

	@Override
	public ServiceResult insertKeyWordwechat(TbSpKeywordWeChat keywordWeChat) throws Exception {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		if (keywordWeChat == null) {
			sr.setAppFail("数据为空");
			return sr;
		}
		if (!checkData(keywordWeChat, sr)) {
			return sr;
		}

		keywordWeChatDao.save(keywordWeChat);

		sr.setSuccess();
		sr.setResult(keywordWeChat);
		return sr;
	}

	@Override
	public ServiceResult updateKeyWordwechat(TbSpKeywordWeChat keywordWeChat) throws Exception {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		if (keywordWeChat == null) {
			sr.setAppFail("数据为空");
			return sr;
		}
		if (keywordWeChat.getId() == null || keywordWeChat.getId().compareTo(0) < 0) {
			sr.setAppFail("ID不能为空");
			return sr;
		}
//		TbSpKeywordWeChat oldKeyword = keywordWeChatDao.findByStateAndId(Constants.TRUE, keywordWeChat.getId());
//		if (oldKeyword == null) {
//			sr.setAppFail("该记录已失效");
//			return sr;
//		}
		if (!checkData(keywordWeChat, sr)) {
			return sr;
		}

		keywordWeChatDao.save(keywordWeChat);

		sr.setSuccess();
		sr.setResult(keywordWeChat);
		return sr;
	}

	private boolean checkData(TbSpKeywordWeChat keywordWeChat, ServiceResult sr) {
		if (StringUtils.isBlank(keywordWeChat.getKeyName())) {
			sr.setAppFail("关键词标识不能为空");
			return false;
		}

		if (StringUtils.isBlank(keywordWeChat.getKeyWord())) {
			sr.setAppFail("关键词内容不能为空");
			return false;
		}
		if (StringUtils.isBlank(keywordWeChat.getReplyText())) {
			sr.setAppFail("关键词对应的回复内容不能为空");
			return false;
		}
		if (keywordWeChat.getType() == null || keywordWeChat.getType().compareTo("0") < 0) {
			sr.setAppFail("类型不能为空");
			return false;
		}
		if (keywordWeChat.getState() == null || keywordWeChat.getState().compareTo("0") < 0) {
			sr.setAppFail("状态不能为空");
			return false;
		}
		return true;
	}

	@Override
	public ServiceResult weChatKeywords(String keyName, String keyWord, String type, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		Sort sort = new Sort(Direction.DESC, "id");
		sr.setResult(keywordWeChatDao.findAll(new Specification<TbSpKeywordWeChat>() {
			public Predicate toPredicate(Root<TbSpKeywordWeChat> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicate = new ArrayList<Predicate>();
				if (StringUtils.isNotEmpty(keyName)) {
					log.info(keyName);
					predicate.add(cb.like(root.get("keyName").as(String.class), "%"+ keyName+"%" ));
				}
				if (StringUtils.isNotEmpty(keyWord)) {
					log.info(keyWord);
					predicate.add(cb.like(root.get("type").as(String.class),  "%"+ keyWord+"%"  ));
				}
				if (StringUtils.isNotEmpty(type)) {
					log.info(type);
					predicate.add(cb.equal(root.get("type").as(String.class),  type ));
				}
				Predicate[] pre = new Predicate[predicate.size()];
				return query.where(predicate.toArray(pre)).getRestriction();
			}
		}, PageRequest.of(pageNumber - 1, pageSize, sort)));
		
		return sr;
	}
}
