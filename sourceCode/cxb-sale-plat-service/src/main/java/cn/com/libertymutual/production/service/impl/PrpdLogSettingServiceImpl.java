package cn.com.libertymutual.production.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdlogsettingMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogsetting;
import cn.com.libertymutual.production.pojo.request.LogRequest;
import cn.com.libertymutual.production.service.api.PrpdLogSettingService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class PrpdLogSettingServiceImpl implements PrpdLogSettingService {
	
	@Autowired
	private PrpdlogsettingMapper prpdlogsettingMapper;

	@Override
	public Long insert(Prpdlogsetting record) {
		
		prpdlogsettingMapper.insertSelective(record);
		
		return record.getRowidObject();
	}

	@Override
	public PageInfo<Prpdlogsetting> findLogByKeyValue(LogRequest logRequest) {
		PageHelper.startPage(logRequest.getCurrentPage(), logRequest.getPageSize());
		PageInfo<Prpdlogsetting> pageInfo = new PageInfo<>(prpdlogsettingMapper.selectByBusinessKey(logRequest.getBusinessKeyValue()));
		return pageInfo;
	}

}
