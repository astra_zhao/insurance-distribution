package cn.com.libertymutual.sp.bean.car;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tb_mng_package_main", catalog = "")
@IdClass(MngPackageMain.class)
public class MngPackageMain implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pkId;
	private String pkPackageName;
	private String pkBranchNo;
	private String packRisk;
	private String packName;

	@Id
	@Column(name = "PK_ID")
	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	@Transient
	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}

	@Column(name = "PACK_RISK")
	public String getPackRisk() {
		return packRisk;
	}

	public void setPackRisk(String packRisk) {
		this.packRisk = packRisk;
	}

	@Column(name = "PK_BRANCH_NO")
	public String getPkBranchNo() {
		return pkBranchNo;
	}

	public void setPkBranchNo(String pkBranchNo) {
		this.pkBranchNo = pkBranchNo;
	}

	@Column(name = "PK_PACKAGE_NAME")
	public String getPkPackageName() {
		return pkPackageName;
	}

	public void setPkPackageName(String pkPackageName) {
		this.pkPackageName = pkPackageName;
	}

}
