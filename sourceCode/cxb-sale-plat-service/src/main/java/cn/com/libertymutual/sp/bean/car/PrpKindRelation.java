package cn.com.libertymutual.sp.bean.car;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_prp_kind_relation", catalog = "")
@IdClass(PrpKindRelationPK.class)
public class PrpKindRelation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1810090287907500468L;
	private String kindCode;
	private String riskCode;
	private String nonDeductibleKindCode;
	private int serialNo;
	private String mainKindCode;
	private String isShow;
	private String remark;
	private String isDefault;
	private String excludeCode;

	@Id
	@Column(name = "KIND_CODE", nullable = false, length = 10)
	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	@Id
	@Column(name = "RISK_CODE", nullable = false, length = 4)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Column(name = "IS_DEFAULT")
	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	@Column(name = "EXCLUDE_CODE")
	public String getExcludeCode() {
		return excludeCode;
	}

	public void setExcludeCode(String excludeCode) {
		this.excludeCode = excludeCode;
	}

	@Basic
	@Column(name = "NON_DEDUCTIBLE_KIND_CODE", nullable = true, length = 10)
	public String getNonDeductibleKindCode() {
		return nonDeductibleKindCode;
	}

	public void setNonDeductibleKindCode(String nonDeductibleKindCode) {
		this.nonDeductibleKindCode = nonDeductibleKindCode;
	}

	@Basic
	@Column(name = "SERIAL_NO", nullable = false)
	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	@Basic
	@Column(name = "MAIN_KIND_CODE", nullable = true, length = 50)
	public String getMainKindCode() {
		return mainKindCode;
	}

	public void setMainKindCode(String mainKindCode) {
		this.mainKindCode = mainKindCode;
	}

	@Basic
	@Column(name = "IS_SHOW", nullable = false, length = 1)
	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	@Basic
	@Column(name = "REMARK", nullable = true, length = 300)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PrpKindRelation that = (PrpKindRelation) o;
		if (serialNo != that.serialNo)
			return false;
		if (kindCode != null ? !kindCode.equals(that.kindCode) : that.kindCode != null)
			return false;
		if (riskCode != null ? !riskCode.equals(that.riskCode) : that.riskCode != null)
			return false;
		if (nonDeductibleKindCode != null ? !nonDeductibleKindCode.equals(that.nonDeductibleKindCode) : that.nonDeductibleKindCode != null)
			return false;
		if (mainKindCode != null ? !mainKindCode.equals(that.mainKindCode) : that.mainKindCode != null)
			return false;
		if (isShow != null ? !isShow.equals(that.isShow) : that.isShow != null)
			return false;
		if (remark != null ? !remark.equals(that.remark) : that.remark != null)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = kindCode != null ? kindCode.hashCode() : 0;
		result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
		result = 31 * result + (nonDeductibleKindCode != null ? nonDeductibleKindCode.hashCode() : 0);
		result = 31 * result + serialNo;
		result = 31 * result + (mainKindCode != null ? mainKindCode.hashCode() : 0);
		result = 31 * result + (isShow != null ? isShow.hashCode() : 0);
		result = 31 * result + (remark != null ? remark.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "PrpKindRelation [kindCode=" + kindCode + ", riskCode=" + riskCode + ", nonDeductibleKindCode=" + nonDeductibleKindCode + ", serialNo="
				+ serialNo + ", mainKindCode=" + mainKindCode + ", isShow=" + isShow + ", remark=" + remark + "]";
	}
}