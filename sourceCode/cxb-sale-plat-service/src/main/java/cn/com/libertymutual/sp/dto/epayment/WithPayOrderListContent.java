package cn.com.libertymutual.sp.dto.epayment;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.google.common.collect.Lists;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WithPayOrderListContent", propOrder = {
    "withPayOrder"
})
public class WithPayOrderListContent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3560892786528548584L;

	 @XmlElement(name = "WithPayOrder")
	private List<WithPayOrder> withPayOrder;


	public List<WithPayOrder> getWithPayOrder() {
		if(null==withPayOrder){
			withPayOrder=Lists.newArrayList();
		}
		return withPayOrder;
	}


	public void setWithPayOrder(List<WithPayOrder> withPayOrder) {
		this.withPayOrder = withPayOrder;
	}
	
	
}
