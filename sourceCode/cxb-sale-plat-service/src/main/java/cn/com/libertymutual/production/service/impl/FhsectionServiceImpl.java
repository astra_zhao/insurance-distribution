package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhsectionMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhsection;
import cn.com.libertymutual.production.model.nomorcldatasource.FhsectionExample;
import cn.com.libertymutual.production.service.api.FhsectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author steven.li
 * @create 2017/12/20
 */
@Service
public class FhsectionServiceImpl implements FhsectionService {

    @Autowired
    private FhsectionMapper fhsectionMapper;

    @Override
    public void insert(Fhsection record) {
        fhsectionMapper.insertSelective(record);
    }

    @Override
    public List<Fhsection> findByTreatyNo(String treatyno) {
        FhsectionExample criteria = new FhsectionExample();
        criteria.createCriteria().andTreatynoEqualTo(treatyno);
        criteria.setOrderByClause("SECTIONNO ASC");
        return fhsectionMapper.selectByExample(criteria);
    }

}
