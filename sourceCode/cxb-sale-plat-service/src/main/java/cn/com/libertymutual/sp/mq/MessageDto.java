package cn.com.libertymutual.sp.mq;

import java.io.Serializable;
import java.nio.charset.Charset;

import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.util.enums.CommunicationMode;

import com.alibaba.fastjson.JSONObject;


public class MessageDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6580284194333506756L;
	private String topic;
	private String tag;
	private String key;	//key用于标识业务的唯一性，我们系统会返回给消息生产者
	private CommunicationMode mode;	//SYNC,    ASYNC,    ONEWAY,

	private long timeout;
	
	private String charset;
	private JSONObject body;


	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public CommunicationMode getMode() {
		return mode == null ? CommunicationMode.ONEWAY : mode;
	}

	public void setMode(CommunicationMode mode) {
		this.mode = mode;
	}

	public JSONObject getBody() {
		return body;
	}
	public byte[] getBodys() {
		return JSONObject.toJSONString(body).getBytes( StringUtil.isEmpty(charset) ? Charset.defaultCharset() : Charset.forName(charset) );
	}

	public void setBody(JSONObject body) {
		this.body = body;
	}

	public long getTimeout() {
		return timeout < 1 ? 6000 : timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	@Override
	public String toString() {
		return "MessageDto{" +
				"topic='" + topic + '\'' +
				", tag='" + tag + '\'' +
				", key='" + key + '\'' +
				", mode=" + mode +
				", timeout=" + timeout +
				", charset='" + charset + '\'' +
				", body=" + body +
				'}';
	}
}
