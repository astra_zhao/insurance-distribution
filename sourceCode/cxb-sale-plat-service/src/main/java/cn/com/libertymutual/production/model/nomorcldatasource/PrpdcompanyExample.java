package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PrpdcompanyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdcompanyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andComcodeIsNull() {
            addCriterion("COMCODE is null");
            return (Criteria) this;
        }

        public Criteria andComcodeIsNotNull() {
            addCriterion("COMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andComcodeEqualTo(String value) {
            addCriterion("COMCODE =", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotEqualTo(String value) {
            addCriterion("COMCODE <>", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeGreaterThan(String value) {
            addCriterion("COMCODE >", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeGreaterThanOrEqualTo(String value) {
            addCriterion("COMCODE >=", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLessThan(String value) {
            addCriterion("COMCODE <", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLessThanOrEqualTo(String value) {
            addCriterion("COMCODE <=", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLike(String value) {
            addCriterion("COMCODE like", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotLike(String value) {
            addCriterion("COMCODE not like", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeIn(List<String> values) {
            addCriterion("COMCODE in", values, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotIn(List<String> values) {
            addCriterion("COMCODE not in", values, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeBetween(String value1, String value2) {
            addCriterion("COMCODE between", value1, value2, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotBetween(String value1, String value2) {
            addCriterion("COMCODE not between", value1, value2, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcnameIsNull() {
            addCriterion("COMCNAME is null");
            return (Criteria) this;
        }

        public Criteria andComcnameIsNotNull() {
            addCriterion("COMCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andComcnameEqualTo(String value) {
            addCriterion("COMCNAME =", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameNotEqualTo(String value) {
            addCriterion("COMCNAME <>", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameGreaterThan(String value) {
            addCriterion("COMCNAME >", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameGreaterThanOrEqualTo(String value) {
            addCriterion("COMCNAME >=", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameLessThan(String value) {
            addCriterion("COMCNAME <", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameLessThanOrEqualTo(String value) {
            addCriterion("COMCNAME <=", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameLike(String value) {
            addCriterion("COMCNAME like", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameNotLike(String value) {
            addCriterion("COMCNAME not like", value, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameIn(List<String> values) {
            addCriterion("COMCNAME in", values, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameNotIn(List<String> values) {
            addCriterion("COMCNAME not in", values, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameBetween(String value1, String value2) {
            addCriterion("COMCNAME between", value1, value2, "comcname");
            return (Criteria) this;
        }

        public Criteria andComcnameNotBetween(String value1, String value2) {
            addCriterion("COMCNAME not between", value1, value2, "comcname");
            return (Criteria) this;
        }

        public Criteria andComenameIsNull() {
            addCriterion("COMENAME is null");
            return (Criteria) this;
        }

        public Criteria andComenameIsNotNull() {
            addCriterion("COMENAME is not null");
            return (Criteria) this;
        }

        public Criteria andComenameEqualTo(String value) {
            addCriterion("COMENAME =", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameNotEqualTo(String value) {
            addCriterion("COMENAME <>", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameGreaterThan(String value) {
            addCriterion("COMENAME >", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameGreaterThanOrEqualTo(String value) {
            addCriterion("COMENAME >=", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameLessThan(String value) {
            addCriterion("COMENAME <", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameLessThanOrEqualTo(String value) {
            addCriterion("COMENAME <=", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameLike(String value) {
            addCriterion("COMENAME like", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameNotLike(String value) {
            addCriterion("COMENAME not like", value, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameIn(List<String> values) {
            addCriterion("COMENAME in", values, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameNotIn(List<String> values) {
            addCriterion("COMENAME not in", values, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameBetween(String value1, String value2) {
            addCriterion("COMENAME between", value1, value2, "comename");
            return (Criteria) this;
        }

        public Criteria andComenameNotBetween(String value1, String value2) {
            addCriterion("COMENAME not between", value1, value2, "comename");
            return (Criteria) this;
        }

        public Criteria andAddressenameIsNull() {
            addCriterion("ADDRESSENAME is null");
            return (Criteria) this;
        }

        public Criteria andAddressenameIsNotNull() {
            addCriterion("ADDRESSENAME is not null");
            return (Criteria) this;
        }

        public Criteria andAddressenameEqualTo(String value) {
            addCriterion("ADDRESSENAME =", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameNotEqualTo(String value) {
            addCriterion("ADDRESSENAME <>", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameGreaterThan(String value) {
            addCriterion("ADDRESSENAME >", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESSENAME >=", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameLessThan(String value) {
            addCriterion("ADDRESSENAME <", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameLessThanOrEqualTo(String value) {
            addCriterion("ADDRESSENAME <=", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameLike(String value) {
            addCriterion("ADDRESSENAME like", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameNotLike(String value) {
            addCriterion("ADDRESSENAME not like", value, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameIn(List<String> values) {
            addCriterion("ADDRESSENAME in", values, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameNotIn(List<String> values) {
            addCriterion("ADDRESSENAME not in", values, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameBetween(String value1, String value2) {
            addCriterion("ADDRESSENAME between", value1, value2, "addressename");
            return (Criteria) this;
        }

        public Criteria andAddressenameNotBetween(String value1, String value2) {
            addCriterion("ADDRESSENAME not between", value1, value2, "addressename");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNull() {
            addCriterion("POSTCODE is null");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNotNull() {
            addCriterion("POSTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPostcodeEqualTo(String value) {
            addCriterion("POSTCODE =", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotEqualTo(String value) {
            addCriterion("POSTCODE <>", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThan(String value) {
            addCriterion("POSTCODE >", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThanOrEqualTo(String value) {
            addCriterion("POSTCODE >=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThan(String value) {
            addCriterion("POSTCODE <", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThanOrEqualTo(String value) {
            addCriterion("POSTCODE <=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLike(String value) {
            addCriterion("POSTCODE like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotLike(String value) {
            addCriterion("POSTCODE not like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeIn(List<String> values) {
            addCriterion("POSTCODE in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotIn(List<String> values) {
            addCriterion("POSTCODE not in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeBetween(String value1, String value2) {
            addCriterion("POSTCODE between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotBetween(String value1, String value2) {
            addCriterion("POSTCODE not between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIsNull() {
            addCriterion("PHONENUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIsNotNull() {
            addCriterion("PHONENUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPhonenumberEqualTo(String value) {
            addCriterion("PHONENUMBER =", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotEqualTo(String value) {
            addCriterion("PHONENUMBER <>", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberGreaterThan(String value) {
            addCriterion("PHONENUMBER >", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberGreaterThanOrEqualTo(String value) {
            addCriterion("PHONENUMBER >=", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLessThan(String value) {
            addCriterion("PHONENUMBER <", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLessThanOrEqualTo(String value) {
            addCriterion("PHONENUMBER <=", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLike(String value) {
            addCriterion("PHONENUMBER like", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotLike(String value) {
            addCriterion("PHONENUMBER not like", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIn(List<String> values) {
            addCriterion("PHONENUMBER in", values, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotIn(List<String> values) {
            addCriterion("PHONENUMBER not in", values, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberBetween(String value1, String value2) {
            addCriterion("PHONENUMBER between", value1, value2, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotBetween(String value1, String value2) {
            addCriterion("PHONENUMBER not between", value1, value2, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberIsNull() {
            addCriterion("TAXNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andTaxnumberIsNotNull() {
            addCriterion("TAXNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andTaxnumberEqualTo(String value) {
            addCriterion("TAXNUMBER =", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberNotEqualTo(String value) {
            addCriterion("TAXNUMBER <>", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberGreaterThan(String value) {
            addCriterion("TAXNUMBER >", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberGreaterThanOrEqualTo(String value) {
            addCriterion("TAXNUMBER >=", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberLessThan(String value) {
            addCriterion("TAXNUMBER <", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberLessThanOrEqualTo(String value) {
            addCriterion("TAXNUMBER <=", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberLike(String value) {
            addCriterion("TAXNUMBER like", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberNotLike(String value) {
            addCriterion("TAXNUMBER not like", value, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberIn(List<String> values) {
            addCriterion("TAXNUMBER in", values, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberNotIn(List<String> values) {
            addCriterion("TAXNUMBER not in", values, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberBetween(String value1, String value2) {
            addCriterion("TAXNUMBER between", value1, value2, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andTaxnumberNotBetween(String value1, String value2) {
            addCriterion("TAXNUMBER not between", value1, value2, "taxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberIsNull() {
            addCriterion("FAXNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andFaxnumberIsNotNull() {
            addCriterion("FAXNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andFaxnumberEqualTo(String value) {
            addCriterion("FAXNUMBER =", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberNotEqualTo(String value) {
            addCriterion("FAXNUMBER <>", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberGreaterThan(String value) {
            addCriterion("FAXNUMBER >", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberGreaterThanOrEqualTo(String value) {
            addCriterion("FAXNUMBER >=", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberLessThan(String value) {
            addCriterion("FAXNUMBER <", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberLessThanOrEqualTo(String value) {
            addCriterion("FAXNUMBER <=", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberLike(String value) {
            addCriterion("FAXNUMBER like", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberNotLike(String value) {
            addCriterion("FAXNUMBER not like", value, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberIn(List<String> values) {
            addCriterion("FAXNUMBER in", values, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberNotIn(List<String> values) {
            addCriterion("FAXNUMBER not in", values, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberBetween(String value1, String value2) {
            addCriterion("FAXNUMBER between", value1, value2, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andFaxnumberNotBetween(String value1, String value2) {
            addCriterion("FAXNUMBER not between", value1, value2, "faxnumber");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeIsNull() {
            addCriterion("UPPERCOMCODE is null");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeIsNotNull() {
            addCriterion("UPPERCOMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeEqualTo(String value) {
            addCriterion("UPPERCOMCODE =", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeNotEqualTo(String value) {
            addCriterion("UPPERCOMCODE <>", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeGreaterThan(String value) {
            addCriterion("UPPERCOMCODE >", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPPERCOMCODE >=", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeLessThan(String value) {
            addCriterion("UPPERCOMCODE <", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeLessThanOrEqualTo(String value) {
            addCriterion("UPPERCOMCODE <=", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeLike(String value) {
            addCriterion("UPPERCOMCODE like", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeNotLike(String value) {
            addCriterion("UPPERCOMCODE not like", value, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeIn(List<String> values) {
            addCriterion("UPPERCOMCODE in", values, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeNotIn(List<String> values) {
            addCriterion("UPPERCOMCODE not in", values, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeBetween(String value1, String value2) {
            addCriterion("UPPERCOMCODE between", value1, value2, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andUppercomcodeNotBetween(String value1, String value2) {
            addCriterion("UPPERCOMCODE not between", value1, value2, "uppercomcode");
            return (Criteria) this;
        }

        public Criteria andInsurernameIsNull() {
            addCriterion("INSURERNAME is null");
            return (Criteria) this;
        }

        public Criteria andInsurernameIsNotNull() {
            addCriterion("INSURERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andInsurernameEqualTo(String value) {
            addCriterion("INSURERNAME =", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameNotEqualTo(String value) {
            addCriterion("INSURERNAME <>", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameGreaterThan(String value) {
            addCriterion("INSURERNAME >", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameGreaterThanOrEqualTo(String value) {
            addCriterion("INSURERNAME >=", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameLessThan(String value) {
            addCriterion("INSURERNAME <", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameLessThanOrEqualTo(String value) {
            addCriterion("INSURERNAME <=", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameLike(String value) {
            addCriterion("INSURERNAME like", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameNotLike(String value) {
            addCriterion("INSURERNAME not like", value, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameIn(List<String> values) {
            addCriterion("INSURERNAME in", values, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameNotIn(List<String> values) {
            addCriterion("INSURERNAME not in", values, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameBetween(String value1, String value2) {
            addCriterion("INSURERNAME between", value1, value2, "insurername");
            return (Criteria) this;
        }

        public Criteria andInsurernameNotBetween(String value1, String value2) {
            addCriterion("INSURERNAME not between", value1, value2, "insurername");
            return (Criteria) this;
        }

        public Criteria andComattributeIsNull() {
            addCriterion("COMATTRIBUTE is null");
            return (Criteria) this;
        }

        public Criteria andComattributeIsNotNull() {
            addCriterion("COMATTRIBUTE is not null");
            return (Criteria) this;
        }

        public Criteria andComattributeEqualTo(String value) {
            addCriterion("COMATTRIBUTE =", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeNotEqualTo(String value) {
            addCriterion("COMATTRIBUTE <>", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeGreaterThan(String value) {
            addCriterion("COMATTRIBUTE >", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeGreaterThanOrEqualTo(String value) {
            addCriterion("COMATTRIBUTE >=", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeLessThan(String value) {
            addCriterion("COMATTRIBUTE <", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeLessThanOrEqualTo(String value) {
            addCriterion("COMATTRIBUTE <=", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeLike(String value) {
            addCriterion("COMATTRIBUTE like", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeNotLike(String value) {
            addCriterion("COMATTRIBUTE not like", value, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeIn(List<String> values) {
            addCriterion("COMATTRIBUTE in", values, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeNotIn(List<String> values) {
            addCriterion("COMATTRIBUTE not in", values, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeBetween(String value1, String value2) {
            addCriterion("COMATTRIBUTE between", value1, value2, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComattributeNotBetween(String value1, String value2) {
            addCriterion("COMATTRIBUTE not between", value1, value2, "comattribute");
            return (Criteria) this;
        }

        public Criteria andComtypeIsNull() {
            addCriterion("COMTYPE is null");
            return (Criteria) this;
        }

        public Criteria andComtypeIsNotNull() {
            addCriterion("COMTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andComtypeEqualTo(String value) {
            addCriterion("COMTYPE =", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeNotEqualTo(String value) {
            addCriterion("COMTYPE <>", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeGreaterThan(String value) {
            addCriterion("COMTYPE >", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeGreaterThanOrEqualTo(String value) {
            addCriterion("COMTYPE >=", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeLessThan(String value) {
            addCriterion("COMTYPE <", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeLessThanOrEqualTo(String value) {
            addCriterion("COMTYPE <=", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeLike(String value) {
            addCriterion("COMTYPE like", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeNotLike(String value) {
            addCriterion("COMTYPE not like", value, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeIn(List<String> values) {
            addCriterion("COMTYPE in", values, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeNotIn(List<String> values) {
            addCriterion("COMTYPE not in", values, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeBetween(String value1, String value2) {
            addCriterion("COMTYPE between", value1, value2, "comtype");
            return (Criteria) this;
        }

        public Criteria andComtypeNotBetween(String value1, String value2) {
            addCriterion("COMTYPE not between", value1, value2, "comtype");
            return (Criteria) this;
        }

        public Criteria andComlevelIsNull() {
            addCriterion("COMLEVEL is null");
            return (Criteria) this;
        }

        public Criteria andComlevelIsNotNull() {
            addCriterion("COMLEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andComlevelEqualTo(String value) {
            addCriterion("COMLEVEL =", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelNotEqualTo(String value) {
            addCriterion("COMLEVEL <>", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelGreaterThan(String value) {
            addCriterion("COMLEVEL >", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelGreaterThanOrEqualTo(String value) {
            addCriterion("COMLEVEL >=", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelLessThan(String value) {
            addCriterion("COMLEVEL <", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelLessThanOrEqualTo(String value) {
            addCriterion("COMLEVEL <=", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelLike(String value) {
            addCriterion("COMLEVEL like", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelNotLike(String value) {
            addCriterion("COMLEVEL not like", value, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelIn(List<String> values) {
            addCriterion("COMLEVEL in", values, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelNotIn(List<String> values) {
            addCriterion("COMLEVEL not in", values, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelBetween(String value1, String value2) {
            addCriterion("COMLEVEL between", value1, value2, "comlevel");
            return (Criteria) this;
        }

        public Criteria andComlevelNotBetween(String value1, String value2) {
            addCriterion("COMLEVEL not between", value1, value2, "comlevel");
            return (Criteria) this;
        }

        public Criteria andManagerIsNull() {
            addCriterion("MANAGER is null");
            return (Criteria) this;
        }

        public Criteria andManagerIsNotNull() {
            addCriterion("MANAGER is not null");
            return (Criteria) this;
        }

        public Criteria andManagerEqualTo(String value) {
            addCriterion("MANAGER =", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotEqualTo(String value) {
            addCriterion("MANAGER <>", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThan(String value) {
            addCriterion("MANAGER >", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGER >=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThan(String value) {
            addCriterion("MANAGER <", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThanOrEqualTo(String value) {
            addCriterion("MANAGER <=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLike(String value) {
            addCriterion("MANAGER like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotLike(String value) {
            addCriterion("MANAGER not like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerIn(List<String> values) {
            addCriterion("MANAGER in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotIn(List<String> values) {
            addCriterion("MANAGER not in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerBetween(String value1, String value2) {
            addCriterion("MANAGER between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotBetween(String value1, String value2) {
            addCriterion("MANAGER not between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andAccountleaderIsNull() {
            addCriterion("ACCOUNTLEADER is null");
            return (Criteria) this;
        }

        public Criteria andAccountleaderIsNotNull() {
            addCriterion("ACCOUNTLEADER is not null");
            return (Criteria) this;
        }

        public Criteria andAccountleaderEqualTo(String value) {
            addCriterion("ACCOUNTLEADER =", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderNotEqualTo(String value) {
            addCriterion("ACCOUNTLEADER <>", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderGreaterThan(String value) {
            addCriterion("ACCOUNTLEADER >", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNTLEADER >=", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderLessThan(String value) {
            addCriterion("ACCOUNTLEADER <", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNTLEADER <=", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderLike(String value) {
            addCriterion("ACCOUNTLEADER like", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderNotLike(String value) {
            addCriterion("ACCOUNTLEADER not like", value, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderIn(List<String> values) {
            addCriterion("ACCOUNTLEADER in", values, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderNotIn(List<String> values) {
            addCriterion("ACCOUNTLEADER not in", values, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderBetween(String value1, String value2) {
            addCriterion("ACCOUNTLEADER between", value1, value2, "accountleader");
            return (Criteria) this;
        }

        public Criteria andAccountleaderNotBetween(String value1, String value2) {
            addCriterion("ACCOUNTLEADER not between", value1, value2, "accountleader");
            return (Criteria) this;
        }

        public Criteria andCashierIsNull() {
            addCriterion("CASHIER is null");
            return (Criteria) this;
        }

        public Criteria andCashierIsNotNull() {
            addCriterion("CASHIER is not null");
            return (Criteria) this;
        }

        public Criteria andCashierEqualTo(String value) {
            addCriterion("CASHIER =", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierNotEqualTo(String value) {
            addCriterion("CASHIER <>", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierGreaterThan(String value) {
            addCriterion("CASHIER >", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierGreaterThanOrEqualTo(String value) {
            addCriterion("CASHIER >=", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierLessThan(String value) {
            addCriterion("CASHIER <", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierLessThanOrEqualTo(String value) {
            addCriterion("CASHIER <=", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierLike(String value) {
            addCriterion("CASHIER like", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierNotLike(String value) {
            addCriterion("CASHIER not like", value, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierIn(List<String> values) {
            addCriterion("CASHIER in", values, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierNotIn(List<String> values) {
            addCriterion("CASHIER not in", values, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierBetween(String value1, String value2) {
            addCriterion("CASHIER between", value1, value2, "cashier");
            return (Criteria) this;
        }

        public Criteria andCashierNotBetween(String value1, String value2) {
            addCriterion("CASHIER not between", value1, value2, "cashier");
            return (Criteria) this;
        }

        public Criteria andAccountantIsNull() {
            addCriterion("ACCOUNTANT is null");
            return (Criteria) this;
        }

        public Criteria andAccountantIsNotNull() {
            addCriterion("ACCOUNTANT is not null");
            return (Criteria) this;
        }

        public Criteria andAccountantEqualTo(String value) {
            addCriterion("ACCOUNTANT =", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantNotEqualTo(String value) {
            addCriterion("ACCOUNTANT <>", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantGreaterThan(String value) {
            addCriterion("ACCOUNTANT >", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNTANT >=", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantLessThan(String value) {
            addCriterion("ACCOUNTANT <", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNTANT <=", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantLike(String value) {
            addCriterion("ACCOUNTANT like", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantNotLike(String value) {
            addCriterion("ACCOUNTANT not like", value, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantIn(List<String> values) {
            addCriterion("ACCOUNTANT in", values, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantNotIn(List<String> values) {
            addCriterion("ACCOUNTANT not in", values, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantBetween(String value1, String value2) {
            addCriterion("ACCOUNTANT between", value1, value2, "accountant");
            return (Criteria) this;
        }

        public Criteria andAccountantNotBetween(String value1, String value2) {
            addCriterion("ACCOUNTANT not between", value1, value2, "accountant");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeIsNull() {
            addCriterion("NEWCOMCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeIsNotNull() {
            addCriterion("NEWCOMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeEqualTo(String value) {
            addCriterion("NEWCOMCODE =", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeNotEqualTo(String value) {
            addCriterion("NEWCOMCODE <>", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeGreaterThan(String value) {
            addCriterion("NEWCOMCODE >", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWCOMCODE >=", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeLessThan(String value) {
            addCriterion("NEWCOMCODE <", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeLessThanOrEqualTo(String value) {
            addCriterion("NEWCOMCODE <=", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeLike(String value) {
            addCriterion("NEWCOMCODE like", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeNotLike(String value) {
            addCriterion("NEWCOMCODE not like", value, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeIn(List<String> values) {
            addCriterion("NEWCOMCODE in", values, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeNotIn(List<String> values) {
            addCriterion("NEWCOMCODE not in", values, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeBetween(String value1, String value2) {
            addCriterion("NEWCOMCODE between", value1, value2, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andNewcomcodeNotBetween(String value1, String value2) {
            addCriterion("NEWCOMCODE not between", value1, value2, "newcomcode");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andAcntunitIsNull() {
            addCriterion("ACNTUNIT is null");
            return (Criteria) this;
        }

        public Criteria andAcntunitIsNotNull() {
            addCriterion("ACNTUNIT is not null");
            return (Criteria) this;
        }

        public Criteria andAcntunitEqualTo(String value) {
            addCriterion("ACNTUNIT =", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitNotEqualTo(String value) {
            addCriterion("ACNTUNIT <>", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitGreaterThan(String value) {
            addCriterion("ACNTUNIT >", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitGreaterThanOrEqualTo(String value) {
            addCriterion("ACNTUNIT >=", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitLessThan(String value) {
            addCriterion("ACNTUNIT <", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitLessThanOrEqualTo(String value) {
            addCriterion("ACNTUNIT <=", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitLike(String value) {
            addCriterion("ACNTUNIT like", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitNotLike(String value) {
            addCriterion("ACNTUNIT not like", value, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitIn(List<String> values) {
            addCriterion("ACNTUNIT in", values, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitNotIn(List<String> values) {
            addCriterion("ACNTUNIT not in", values, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitBetween(String value1, String value2) {
            addCriterion("ACNTUNIT between", value1, value2, "acntunit");
            return (Criteria) this;
        }

        public Criteria andAcntunitNotBetween(String value1, String value2) {
            addCriterion("ACNTUNIT not between", value1, value2, "acntunit");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIsNull() {
            addCriterion("ARTICLECODE is null");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIsNotNull() {
            addCriterion("ARTICLECODE is not null");
            return (Criteria) this;
        }

        public Criteria andArticlecodeEqualTo(String value) {
            addCriterion("ARTICLECODE =", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotEqualTo(String value) {
            addCriterion("ARTICLECODE <>", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeGreaterThan(String value) {
            addCriterion("ARTICLECODE >", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeGreaterThanOrEqualTo(String value) {
            addCriterion("ARTICLECODE >=", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLessThan(String value) {
            addCriterion("ARTICLECODE <", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLessThanOrEqualTo(String value) {
            addCriterion("ARTICLECODE <=", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLike(String value) {
            addCriterion("ARTICLECODE like", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotLike(String value) {
            addCriterion("ARTICLECODE not like", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIn(List<String> values) {
            addCriterion("ARTICLECODE in", values, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotIn(List<String> values) {
            addCriterion("ARTICLECODE not in", values, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeBetween(String value1, String value2) {
            addCriterion("ARTICLECODE between", value1, value2, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotBetween(String value1, String value2) {
            addCriterion("ARTICLECODE not between", value1, value2, "articlecode");
            return (Criteria) this;
        }

        public Criteria andAcccodeIsNull() {
            addCriterion("ACCCODE is null");
            return (Criteria) this;
        }

        public Criteria andAcccodeIsNotNull() {
            addCriterion("ACCCODE is not null");
            return (Criteria) this;
        }

        public Criteria andAcccodeEqualTo(String value) {
            addCriterion("ACCCODE =", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotEqualTo(String value) {
            addCriterion("ACCCODE <>", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeGreaterThan(String value) {
            addCriterion("ACCCODE >", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACCCODE >=", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLessThan(String value) {
            addCriterion("ACCCODE <", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLessThanOrEqualTo(String value) {
            addCriterion("ACCCODE <=", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeLike(String value) {
            addCriterion("ACCCODE like", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotLike(String value) {
            addCriterion("ACCCODE not like", value, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeIn(List<String> values) {
            addCriterion("ACCCODE in", values, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotIn(List<String> values) {
            addCriterion("ACCCODE not in", values, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeBetween(String value1, String value2) {
            addCriterion("ACCCODE between", value1, value2, "acccode");
            return (Criteria) this;
        }

        public Criteria andAcccodeNotBetween(String value1, String value2) {
            addCriterion("ACCCODE not between", value1, value2, "acccode");
            return (Criteria) this;
        }

        public Criteria andCenterflagIsNull() {
            addCriterion("CENTERFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCenterflagIsNotNull() {
            addCriterion("CENTERFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCenterflagEqualTo(String value) {
            addCriterion("CENTERFLAG =", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagNotEqualTo(String value) {
            addCriterion("CENTERFLAG <>", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagGreaterThan(String value) {
            addCriterion("CENTERFLAG >", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagGreaterThanOrEqualTo(String value) {
            addCriterion("CENTERFLAG >=", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagLessThan(String value) {
            addCriterion("CENTERFLAG <", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagLessThanOrEqualTo(String value) {
            addCriterion("CENTERFLAG <=", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagLike(String value) {
            addCriterion("CENTERFLAG like", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagNotLike(String value) {
            addCriterion("CENTERFLAG not like", value, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagIn(List<String> values) {
            addCriterion("CENTERFLAG in", values, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagNotIn(List<String> values) {
            addCriterion("CENTERFLAG not in", values, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagBetween(String value1, String value2) {
            addCriterion("CENTERFLAG between", value1, value2, "centerflag");
            return (Criteria) this;
        }

        public Criteria andCenterflagNotBetween(String value1, String value2) {
            addCriterion("CENTERFLAG not between", value1, value2, "centerflag");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeIsNull() {
            addCriterion("OUTERPAYCODE is null");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeIsNotNull() {
            addCriterion("OUTERPAYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeEqualTo(String value) {
            addCriterion("OUTERPAYCODE =", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeNotEqualTo(String value) {
            addCriterion("OUTERPAYCODE <>", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeGreaterThan(String value) {
            addCriterion("OUTERPAYCODE >", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeGreaterThanOrEqualTo(String value) {
            addCriterion("OUTERPAYCODE >=", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeLessThan(String value) {
            addCriterion("OUTERPAYCODE <", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeLessThanOrEqualTo(String value) {
            addCriterion("OUTERPAYCODE <=", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeLike(String value) {
            addCriterion("OUTERPAYCODE like", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeNotLike(String value) {
            addCriterion("OUTERPAYCODE not like", value, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeIn(List<String> values) {
            addCriterion("OUTERPAYCODE in", values, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeNotIn(List<String> values) {
            addCriterion("OUTERPAYCODE not in", values, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeBetween(String value1, String value2) {
            addCriterion("OUTERPAYCODE between", value1, value2, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andOuterpaycodeNotBetween(String value1, String value2) {
            addCriterion("OUTERPAYCODE not between", value1, value2, "outerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeIsNull() {
            addCriterion("INNERPAYCODE is null");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeIsNotNull() {
            addCriterion("INNERPAYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeEqualTo(String value) {
            addCriterion("INNERPAYCODE =", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeNotEqualTo(String value) {
            addCriterion("INNERPAYCODE <>", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeGreaterThan(String value) {
            addCriterion("INNERPAYCODE >", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeGreaterThanOrEqualTo(String value) {
            addCriterion("INNERPAYCODE >=", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeLessThan(String value) {
            addCriterion("INNERPAYCODE <", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeLessThanOrEqualTo(String value) {
            addCriterion("INNERPAYCODE <=", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeLike(String value) {
            addCriterion("INNERPAYCODE like", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeNotLike(String value) {
            addCriterion("INNERPAYCODE not like", value, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeIn(List<String> values) {
            addCriterion("INNERPAYCODE in", values, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeNotIn(List<String> values) {
            addCriterion("INNERPAYCODE not in", values, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeBetween(String value1, String value2) {
            addCriterion("INNERPAYCODE between", value1, value2, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andInnerpaycodeNotBetween(String value1, String value2) {
            addCriterion("INNERPAYCODE not between", value1, value2, "innerpaycode");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andWebaddressIsNull() {
            addCriterion("WEBADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andWebaddressIsNotNull() {
            addCriterion("WEBADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andWebaddressEqualTo(String value) {
            addCriterion("WEBADDRESS =", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressNotEqualTo(String value) {
            addCriterion("WEBADDRESS <>", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressGreaterThan(String value) {
            addCriterion("WEBADDRESS >", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressGreaterThanOrEqualTo(String value) {
            addCriterion("WEBADDRESS >=", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressLessThan(String value) {
            addCriterion("WEBADDRESS <", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressLessThanOrEqualTo(String value) {
            addCriterion("WEBADDRESS <=", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressLike(String value) {
            addCriterion("WEBADDRESS like", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressNotLike(String value) {
            addCriterion("WEBADDRESS not like", value, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressIn(List<String> values) {
            addCriterion("WEBADDRESS in", values, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressNotIn(List<String> values) {
            addCriterion("WEBADDRESS not in", values, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressBetween(String value1, String value2) {
            addCriterion("WEBADDRESS between", value1, value2, "webaddress");
            return (Criteria) this;
        }

        public Criteria andWebaddressNotBetween(String value1, String value2) {
            addCriterion("WEBADDRESS not between", value1, value2, "webaddress");
            return (Criteria) this;
        }

        public Criteria andServicephoneIsNull() {
            addCriterion("SERVICEPHONE is null");
            return (Criteria) this;
        }

        public Criteria andServicephoneIsNotNull() {
            addCriterion("SERVICEPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andServicephoneEqualTo(String value) {
            addCriterion("SERVICEPHONE =", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneNotEqualTo(String value) {
            addCriterion("SERVICEPHONE <>", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneGreaterThan(String value) {
            addCriterion("SERVICEPHONE >", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneGreaterThanOrEqualTo(String value) {
            addCriterion("SERVICEPHONE >=", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneLessThan(String value) {
            addCriterion("SERVICEPHONE <", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneLessThanOrEqualTo(String value) {
            addCriterion("SERVICEPHONE <=", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneLike(String value) {
            addCriterion("SERVICEPHONE like", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneNotLike(String value) {
            addCriterion("SERVICEPHONE not like", value, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneIn(List<String> values) {
            addCriterion("SERVICEPHONE in", values, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneNotIn(List<String> values) {
            addCriterion("SERVICEPHONE not in", values, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneBetween(String value1, String value2) {
            addCriterion("SERVICEPHONE between", value1, value2, "servicephone");
            return (Criteria) this;
        }

        public Criteria andServicephoneNotBetween(String value1, String value2) {
            addCriterion("SERVICEPHONE not between", value1, value2, "servicephone");
            return (Criteria) this;
        }

        public Criteria andReportphoneIsNull() {
            addCriterion("REPORTPHONE is null");
            return (Criteria) this;
        }

        public Criteria andReportphoneIsNotNull() {
            addCriterion("REPORTPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andReportphoneEqualTo(String value) {
            addCriterion("REPORTPHONE =", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneNotEqualTo(String value) {
            addCriterion("REPORTPHONE <>", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneGreaterThan(String value) {
            addCriterion("REPORTPHONE >", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneGreaterThanOrEqualTo(String value) {
            addCriterion("REPORTPHONE >=", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneLessThan(String value) {
            addCriterion("REPORTPHONE <", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneLessThanOrEqualTo(String value) {
            addCriterion("REPORTPHONE <=", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneLike(String value) {
            addCriterion("REPORTPHONE like", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneNotLike(String value) {
            addCriterion("REPORTPHONE not like", value, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneIn(List<String> values) {
            addCriterion("REPORTPHONE in", values, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneNotIn(List<String> values) {
            addCriterion("REPORTPHONE not in", values, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneBetween(String value1, String value2) {
            addCriterion("REPORTPHONE between", value1, value2, "reportphone");
            return (Criteria) this;
        }

        public Criteria andReportphoneNotBetween(String value1, String value2) {
            addCriterion("REPORTPHONE not between", value1, value2, "reportphone");
            return (Criteria) this;
        }

        public Criteria andAgentcodeIsNull() {
            addCriterion("AGENTCODE is null");
            return (Criteria) this;
        }

        public Criteria andAgentcodeIsNotNull() {
            addCriterion("AGENTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andAgentcodeEqualTo(String value) {
            addCriterion("AGENTCODE =", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotEqualTo(String value) {
            addCriterion("AGENTCODE <>", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeGreaterThan(String value) {
            addCriterion("AGENTCODE >", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTCODE >=", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLessThan(String value) {
            addCriterion("AGENTCODE <", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLessThanOrEqualTo(String value) {
            addCriterion("AGENTCODE <=", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLike(String value) {
            addCriterion("AGENTCODE like", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotLike(String value) {
            addCriterion("AGENTCODE not like", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeIn(List<String> values) {
            addCriterion("AGENTCODE in", values, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotIn(List<String> values) {
            addCriterion("AGENTCODE not in", values, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeBetween(String value1, String value2) {
            addCriterion("AGENTCODE between", value1, value2, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotBetween(String value1, String value2) {
            addCriterion("AGENTCODE not between", value1, value2, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIsNull() {
            addCriterion("AGREEMENTNO is null");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIsNotNull() {
            addCriterion("AGREEMENTNO is not null");
            return (Criteria) this;
        }

        public Criteria andAgreementnoEqualTo(String value) {
            addCriterion("AGREEMENTNO =", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotEqualTo(String value) {
            addCriterion("AGREEMENTNO <>", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoGreaterThan(String value) {
            addCriterion("AGREEMENTNO >", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoGreaterThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO >=", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLessThan(String value) {
            addCriterion("AGREEMENTNO <", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLessThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO <=", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLike(String value) {
            addCriterion("AGREEMENTNO like", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotLike(String value) {
            addCriterion("AGREEMENTNO not like", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIn(List<String> values) {
            addCriterion("AGREEMENTNO in", values, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotIn(List<String> values) {
            addCriterion("AGREEMENTNO not in", values, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoBetween(String value1, String value2) {
            addCriterion("AGREEMENTNO between", value1, value2, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotBetween(String value1, String value2) {
            addCriterion("AGREEMENTNO not between", value1, value2, "agreementno");
            return (Criteria) this;
        }

        public Criteria andSysareacodeIsNull() {
            addCriterion("SYSAREACODE is null");
            return (Criteria) this;
        }

        public Criteria andSysareacodeIsNotNull() {
            addCriterion("SYSAREACODE is not null");
            return (Criteria) this;
        }

        public Criteria andSysareacodeEqualTo(String value) {
            addCriterion("SYSAREACODE =", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeNotEqualTo(String value) {
            addCriterion("SYSAREACODE <>", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeGreaterThan(String value) {
            addCriterion("SYSAREACODE >", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeGreaterThanOrEqualTo(String value) {
            addCriterion("SYSAREACODE >=", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeLessThan(String value) {
            addCriterion("SYSAREACODE <", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeLessThanOrEqualTo(String value) {
            addCriterion("SYSAREACODE <=", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeLike(String value) {
            addCriterion("SYSAREACODE like", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeNotLike(String value) {
            addCriterion("SYSAREACODE not like", value, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeIn(List<String> values) {
            addCriterion("SYSAREACODE in", values, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeNotIn(List<String> values) {
            addCriterion("SYSAREACODE not in", values, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeBetween(String value1, String value2) {
            addCriterion("SYSAREACODE between", value1, value2, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andSysareacodeNotBetween(String value1, String value2) {
            addCriterion("SYSAREACODE not between", value1, value2, "sysareacode");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateIsNull() {
            addCriterion("COMBVISITRATE is null");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateIsNotNull() {
            addCriterion("COMBVISITRATE is not null");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateEqualTo(BigDecimal value) {
            addCriterion("COMBVISITRATE =", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateNotEqualTo(BigDecimal value) {
            addCriterion("COMBVISITRATE <>", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateGreaterThan(BigDecimal value) {
            addCriterion("COMBVISITRATE >", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("COMBVISITRATE >=", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateLessThan(BigDecimal value) {
            addCriterion("COMBVISITRATE <", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("COMBVISITRATE <=", value, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateIn(List<BigDecimal> values) {
            addCriterion("COMBVISITRATE in", values, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateNotIn(List<BigDecimal> values) {
            addCriterion("COMBVISITRATE not in", values, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMBVISITRATE between", value1, value2, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andCombvisitrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMBVISITRATE not between", value1, value2, "combvisitrate");
            return (Criteria) this;
        }

        public Criteria andComsignIsNull() {
            addCriterion("COMSIGN is null");
            return (Criteria) this;
        }

        public Criteria andComsignIsNotNull() {
            addCriterion("COMSIGN is not null");
            return (Criteria) this;
        }

        public Criteria andComsignEqualTo(String value) {
            addCriterion("COMSIGN =", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignNotEqualTo(String value) {
            addCriterion("COMSIGN <>", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignGreaterThan(String value) {
            addCriterion("COMSIGN >", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignGreaterThanOrEqualTo(String value) {
            addCriterion("COMSIGN >=", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignLessThan(String value) {
            addCriterion("COMSIGN <", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignLessThanOrEqualTo(String value) {
            addCriterion("COMSIGN <=", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignLike(String value) {
            addCriterion("COMSIGN like", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignNotLike(String value) {
            addCriterion("COMSIGN not like", value, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignIn(List<String> values) {
            addCriterion("COMSIGN in", values, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignNotIn(List<String> values) {
            addCriterion("COMSIGN not in", values, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignBetween(String value1, String value2) {
            addCriterion("COMSIGN between", value1, value2, "comsign");
            return (Criteria) this;
        }

        public Criteria andComsignNotBetween(String value1, String value2) {
            addCriterion("COMSIGN not between", value1, value2, "comsign");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeIsNull() {
            addCriterion("MOTORAREACODE is null");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeIsNotNull() {
            addCriterion("MOTORAREACODE is not null");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeEqualTo(String value) {
            addCriterion("MOTORAREACODE =", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeNotEqualTo(String value) {
            addCriterion("MOTORAREACODE <>", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeGreaterThan(String value) {
            addCriterion("MOTORAREACODE >", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeGreaterThanOrEqualTo(String value) {
            addCriterion("MOTORAREACODE >=", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeLessThan(String value) {
            addCriterion("MOTORAREACODE <", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeLessThanOrEqualTo(String value) {
            addCriterion("MOTORAREACODE <=", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeLike(String value) {
            addCriterion("MOTORAREACODE like", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeNotLike(String value) {
            addCriterion("MOTORAREACODE not like", value, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeIn(List<String> values) {
            addCriterion("MOTORAREACODE in", values, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeNotIn(List<String> values) {
            addCriterion("MOTORAREACODE not in", values, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeBetween(String value1, String value2) {
            addCriterion("MOTORAREACODE between", value1, value2, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andMotorareacodeNotBetween(String value1, String value2) {
            addCriterion("MOTORAREACODE not between", value1, value2, "motorareacode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeIsNull() {
            addCriterion("ACTUALUPPERCOMCODE is null");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeIsNotNull() {
            addCriterion("ACTUALUPPERCOMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeEqualTo(String value) {
            addCriterion("ACTUALUPPERCOMCODE =", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeNotEqualTo(String value) {
            addCriterion("ACTUALUPPERCOMCODE <>", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeGreaterThan(String value) {
            addCriterion("ACTUALUPPERCOMCODE >", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACTUALUPPERCOMCODE >=", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeLessThan(String value) {
            addCriterion("ACTUALUPPERCOMCODE <", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeLessThanOrEqualTo(String value) {
            addCriterion("ACTUALUPPERCOMCODE <=", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeLike(String value) {
            addCriterion("ACTUALUPPERCOMCODE like", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeNotLike(String value) {
            addCriterion("ACTUALUPPERCOMCODE not like", value, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeIn(List<String> values) {
            addCriterion("ACTUALUPPERCOMCODE in", values, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeNotIn(List<String> values) {
            addCriterion("ACTUALUPPERCOMCODE not in", values, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeBetween(String value1, String value2) {
            addCriterion("ACTUALUPPERCOMCODE between", value1, value2, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andActualuppercomcodeNotBetween(String value1, String value2) {
            addCriterion("ACTUALUPPERCOMCODE not between", value1, value2, "actualuppercomcode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeIsNull() {
            addCriterion("MOTORCOUNTYCODE is null");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeIsNotNull() {
            addCriterion("MOTORCOUNTYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeEqualTo(String value) {
            addCriterion("MOTORCOUNTYCODE =", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeNotEqualTo(String value) {
            addCriterion("MOTORCOUNTYCODE <>", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeGreaterThan(String value) {
            addCriterion("MOTORCOUNTYCODE >", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeGreaterThanOrEqualTo(String value) {
            addCriterion("MOTORCOUNTYCODE >=", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeLessThan(String value) {
            addCriterion("MOTORCOUNTYCODE <", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeLessThanOrEqualTo(String value) {
            addCriterion("MOTORCOUNTYCODE <=", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeLike(String value) {
            addCriterion("MOTORCOUNTYCODE like", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeNotLike(String value) {
            addCriterion("MOTORCOUNTYCODE not like", value, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeIn(List<String> values) {
            addCriterion("MOTORCOUNTYCODE in", values, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeNotIn(List<String> values) {
            addCriterion("MOTORCOUNTYCODE not in", values, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeBetween(String value1, String value2) {
            addCriterion("MOTORCOUNTYCODE between", value1, value2, "motorcountycode");
            return (Criteria) this;
        }

        public Criteria andMotorcountycodeNotBetween(String value1, String value2) {
            addCriterion("MOTORCOUNTYCODE not between", value1, value2, "motorcountycode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}