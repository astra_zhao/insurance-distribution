package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpFlow;
import cn.com.libertymutual.sp.bean.TbSpFlowDetail;

public interface FlowService {

	ServiceResult addFlow(TbSpFlow flow);

	ServiceResult flowList(int pageNumber,int pageSize);

	ServiceResult addChildNode(TbSpFlowDetail flowDetail);

	ServiceResult flowChildNodeList(String flowNo);

	ServiceResult nodeDetail(String flowNo, String flowNode);

	ServiceResult removeNode(String flowNo, String flowNode);


}
