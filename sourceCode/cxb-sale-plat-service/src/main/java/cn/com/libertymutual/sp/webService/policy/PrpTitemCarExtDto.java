
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prpTitemCarExtDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTitemCarExtDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brandCN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="brandEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chgOwnerFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esCompensRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="haulage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ineffectualDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastCheckDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastyearClaimAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="lastyearClaimTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loanVehicleFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="madeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="madeFactory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noDamageYears" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="querySequenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rejectDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transferDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehicleCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehicleModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTitemCarExtDto", propOrder = {
    "brandCN",
    "brandEN",
    "chgOwnerFlag",
    "esCompensRate",
    "haulage",
    "ineffectualDate",
    "lastCheckDate",
    "lastyearClaimAmount",
    "lastyearClaimTime",
    "loanVehicleFlag",
    "madeDate",
    "madeFactory",
    "noDamageYears",
    "poWeight",
    "querySequenceNo",
    "rejectDate",
    "transferDate",
    "vehicleCategoryCode",
    "vehicleModel"
})
public class PrpTitemCarExtDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2281533221345085249L;
	protected String brandCN;
    protected String brandEN;
    protected String chgOwnerFlag;
    protected double esCompensRate;
    protected String haulage;
    protected String ineffectualDate;
    protected String lastCheckDate;
    protected double lastyearClaimAmount;
    protected String lastyearClaimTime;
    protected String loanVehicleFlag;
    protected String madeDate;
    protected String madeFactory;
    protected String noDamageYears;
    protected String poWeight;
    protected String querySequenceNo;
    protected String rejectDate;
    protected String transferDate;
    protected String vehicleCategoryCode;
    protected String vehicleModel;

    /**
     * Gets the value of the brandCN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCN() {
        return brandCN;
    }

    /**
     * Sets the value of the brandCN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCN(String value) {
        this.brandCN = value;
    }

    /**
     * Gets the value of the brandEN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandEN() {
        return brandEN;
    }

    /**
     * Sets the value of the brandEN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandEN(String value) {
        this.brandEN = value;
    }

    /**
     * Gets the value of the chgOwnerFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChgOwnerFlag() {
        return chgOwnerFlag;
    }

    /**
     * Sets the value of the chgOwnerFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChgOwnerFlag(String value) {
        this.chgOwnerFlag = value;
    }

    /**
     * Gets the value of the esCompensRate property.
     * 
     */
    public double getEsCompensRate() {
        return esCompensRate;
    }

    /**
     * Sets the value of the esCompensRate property.
     * 
     */
    public void setEsCompensRate(double value) {
        this.esCompensRate = value;
    }

    /**
     * Gets the value of the haulage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHaulage() {
        return haulage;
    }

    /**
     * Sets the value of the haulage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHaulage(String value) {
        this.haulage = value;
    }

    /**
     * Gets the value of the ineffectualDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIneffectualDate() {
        return ineffectualDate;
    }

    /**
     * Sets the value of the ineffectualDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIneffectualDate(String value) {
        this.ineffectualDate = value;
    }

    /**
     * Gets the value of the lastCheckDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastCheckDate() {
        return lastCheckDate;
    }

    /**
     * Sets the value of the lastCheckDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastCheckDate(String value) {
        this.lastCheckDate = value;
    }

    /**
     * Gets the value of the lastyearClaimAmount property.
     * 
     */
    public double getLastyearClaimAmount() {
        return lastyearClaimAmount;
    }

    /**
     * Sets the value of the lastyearClaimAmount property.
     * 
     */
    public void setLastyearClaimAmount(double value) {
        this.lastyearClaimAmount = value;
    }

    /**
     * Gets the value of the lastyearClaimTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastyearClaimTime() {
        return lastyearClaimTime;
    }

    /**
     * Sets the value of the lastyearClaimTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastyearClaimTime(String value) {
        this.lastyearClaimTime = value;
    }

    /**
     * Gets the value of the loanVehicleFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanVehicleFlag() {
        return loanVehicleFlag;
    }

    /**
     * Sets the value of the loanVehicleFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanVehicleFlag(String value) {
        this.loanVehicleFlag = value;
    }

    /**
     * Gets the value of the madeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMadeDate() {
        return madeDate;
    }

    /**
     * Sets the value of the madeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMadeDate(String value) {
        this.madeDate = value;
    }

    /**
     * Gets the value of the madeFactory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMadeFactory() {
        return madeFactory;
    }

    /**
     * Sets the value of the madeFactory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMadeFactory(String value) {
        this.madeFactory = value;
    }

    /**
     * Gets the value of the noDamageYears property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoDamageYears() {
        return noDamageYears;
    }

    /**
     * Sets the value of the noDamageYears property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoDamageYears(String value) {
        this.noDamageYears = value;
    }

    /**
     * Gets the value of the poWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoWeight() {
        return poWeight;
    }

    /**
     * Sets the value of the poWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoWeight(String value) {
        this.poWeight = value;
    }

    /**
     * Gets the value of the querySequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuerySequenceNo() {
        return querySequenceNo;
    }

    /**
     * Sets the value of the querySequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuerySequenceNo(String value) {
        this.querySequenceNo = value;
    }

    /**
     * Gets the value of the rejectDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRejectDate() {
        return rejectDate;
    }

    /**
     * Sets the value of the rejectDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRejectDate(String value) {
        this.rejectDate = value;
    }

    /**
     * Gets the value of the transferDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferDate() {
        return transferDate;
    }

    /**
     * Sets the value of the transferDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferDate(String value) {
        this.transferDate = value;
    }

    /**
     * Gets the value of the vehicleCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleCategoryCode() {
        return vehicleCategoryCode;
    }

    /**
     * Sets the value of the vehicleCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleCategoryCode(String value) {
        this.vehicleCategoryCode = value;
    }

    /**
     * Gets the value of the vehicleModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleModel() {
        return vehicleModel;
    }

    /**
     * Sets the value of the vehicleModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleModel(String value) {
        this.vehicleModel = value;
    }

}
