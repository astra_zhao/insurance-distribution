package cn.com.libertymutual.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.wx.service.WeChatService;
import cn.com.libertymutual.wx.util.WXUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/wechat")
public class WXcontrollerWeb {

	@Autowired
	private RedisUtils redisUtils;
	@Autowired
	private WeChatService weChatService;

	@ApiOperation(value = "清除AccessToken缓存", notes = "清除AccessToken缓存")
	@GetMapping(value = "/clearAccessToken")
	public ServiceResult clearAccessToken() {
		ServiceResult sr = new ServiceResult();
		redisUtils.deleteWithPrefix(WXUtil.ACCESSTOKEN);
		sr.setSuccess();
		return sr;

	}

	@ApiOperation(value = "补发", notes = "补发")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "userCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "activityName", value = "活动名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "beans", value = "宝豆数", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "times", value = "次数", required = false, paramType = "query", dataType = "Long") })
	@PostMapping(value = "/reissue")
	public ServiceResult reissue(String userCode, String type, String policyNo, String activityName, Integer beans, Integer times) {
		ServiceResult sr = new ServiceResult();
		sr = weChatService.reissue(userCode, type, policyNo, activityName, beans, times);
		return sr;

	}

}
