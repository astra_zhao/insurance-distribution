package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpSaveData;

public interface SaveDataService {
	ServiceResult createSaveData(TbSpSaveData saveData);

	ServiceResult findUserSaveData(Integer pageNumber, Integer pageSize, String userCode, String riskCode);

	ServiceResult findSaveDataId(Integer id);

}
