package cn.com.libertymutual.production.service.impl.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclass;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdcompany;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogoperation;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogsetting;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranch;
import cn.com.libertymutual.production.pojo.request.LogRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.CommonBusinessService;

import com.github.pagehelper.PageInfo;

/**
 * 事务管理注意事项：
 * 1. 新增/修改操作必须启用事务
 * 2. 方法体内部抛出异常即可启用事务
 * @author Steven.Li
 * @date 2017年7月28日
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class CommonBusinessServiceImpl extends CommonBusinessService {

	@Override
	public Response findPrpdCompany(Request request) {
		Response result = new Response();
		try {
			PageInfo<PrpdcompanyWithBLOBs> pageInfo = prpdCompanyService
					.findPrpdCompanys(request);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	@Override
	public Response findBranch(Request request) {
		Response result = new Response();
		try {
			PageInfo<TbSysBranch> pageInfo = branchService
					.findBranchs(request);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findAllPrpdClass() {
		Response result = new Response();
		try {
			List<Prpdclass> list = prpdClassService.findAllPrpdClass();
			result.setResult(list);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findClassNotComposite() {
		Response result = new Response();
		try {
			List<Prpdclass> list = prpdClassService.findClassNotComposite();
			result.setResult(list);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findAllPrpdRisk() {
		Response result = new Response();
		try {
			List<Prpdrisk> list = prpdRiskService.findAllPrpdRisk();
			result.setResult(list);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findRisksByPage(PrpdRiskRequest request) {
		Response result = new Response();
		try {
			PageInfo<Prpdrisk> risks = prpdRiskService.findAllByPage(request);
			result.setTotal(risks.getTotal());
			result.setResult(risks.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findLogs(LogRequest logRequest) {
		Response result = new Response();
		try {
			PageInfo<Prpdlogsetting> pageInfo = prpdLogSettingService
					.findLogByKeyValue(logRequest);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findLogDetail(LogRequest logRequest) {
		Response result = new Response();
		try {
			Prpdlogoperation logDetail = prpdLogOperationService
					.findLogDetail(logRequest.getBusinessKeyValue());
			result.setResult(logDetail);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findPrpsAgentByComCode(String comCodes, Request request) {
		Response result = new Response();
		if (StringUtils.isEmpty(comCodes)) {
			return result;
		}
		String[] codes = comCodes.split(",");
		List<String> comCodeArray = new ArrayList<String>();
		for (int i = 0; i < codes.length; i++) {
			comCodeArray.add(codes[i]);
		}
		try {
			result.setResult(prpsAgentService.findPrpsAgentByComCode(
					comCodeArray, request));
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	
	@Override
	public Response findUsersAgentByComCode(String comCodes, Request request) {
		Response result = new Response();
		if (StringUtils.isEmpty(comCodes)) {
			return result;
		}
		String[] codes = comCodes.split(",");
		List<String> comCodeArray = new ArrayList<String>();
		for (int i = 0; i < codes.length; i++) {
			comCodeArray.add(codes[i]);
		}
		try {
			result.setResult(userAgentService.findUsersAgentByComCode(
					comCodeArray, request));
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findPlansByRisks(String riskCodes) {
		Response result = new Response();
		List<String> riskList = new ArrayList<String>();
		if (StringUtils.isEmpty(riskCodes)) {
			return result;
		}
		try {
			String[] riskArry = riskCodes.split(",");
			for (String riskCode : riskArry) {
				riskList.add(riskCode);
			}
			result.setResult(prpdRiskPlanService.findPlansByRisks(riskList));
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findCompanys() {
		Response result = new Response();
		List<PrpdcompanyWithBLOBs> companys = new ArrayList<PrpdcompanyWithBLOBs>();
		try {
			companys = prpdCompanyService.findCompanys();
			result.setResult(companys);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	
	@Override
	public Response findNextBranchs() {
		Response result = new Response();
		List<TbSysBranch> companys = new ArrayList<TbSysBranch>();
		try {
			companys = branchService.findNextBranchs();
			result.setResult(companys);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findByClassAndComposite(PrpdRiskRequest request) {
		Response result = new Response();
		try {
			//获取所有组合险类
			List<Prpdclass> classes = prpdClassService.findClassIsComposite();
			List<String> classCodes = new ArrayList<String>();
			if(!classes.isEmpty()) {
				for(Prpdclass clazz : classes) {
					classCodes.add(clazz.getClasscode());
				}
			}
			PageInfo<Prpdrisk> risks = prpdRiskService.findByClassAndComposite(request, classCodes);
			result.setTotal(risks.getTotal());
			result.setResult(risks.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findRiskByClass(String classcode) {
		Response result = new Response();
		try {
			List<Prpdrisk> risks = prpdRiskService.findByClass(classcode);
			result.setResult(risks);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	@Override
	public Response queryCompanysBycodes(List<String> comcodes) {
		Response result = new Response();
		List<PrpdcompanyWithBLOBs> companys;
		try {
			companys = prpdCompanyService.findCompanysBycodes(comcodes);
			result.setResult(companys);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	
	@Override
	public Response queryBranchsBycodes(List<String> comcodes) {
		Response result = new Response();
		List<TbSysBranch> companys;
		try {
			companys = branchService.findNextBranchsByCodes(comcodes);
			result.setResult(companys);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
}
