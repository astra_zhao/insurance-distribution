package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdclauseKey {
    private String clausecode;

    private String lineno;

    public String getClausecode() {
        return clausecode;
    }

    public void setClausecode(String clausecode) {
        this.clausecode = clausecode == null ? null : clausecode.trim();
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno == null ? null : lineno.trim();
    }
}