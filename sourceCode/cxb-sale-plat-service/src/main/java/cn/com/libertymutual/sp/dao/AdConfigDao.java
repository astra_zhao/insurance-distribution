package cn.com.libertymutual.sp.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpAdconfig;

@Repository
public interface AdConfigDao extends PagingAndSortingRepository<TbSpAdconfig, Integer>, JpaSpecificationExecutor<TbSpAdconfig> {

	Optional<TbSpAdconfig> findById(Integer id);

}
