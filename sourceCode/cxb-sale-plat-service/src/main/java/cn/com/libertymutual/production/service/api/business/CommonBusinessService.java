package cn.com.libertymutual.production.service.api.business;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.pojo.request.LogRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.BranchService;
import cn.com.libertymutual.production.service.api.PrpdClassService;
import cn.com.libertymutual.production.service.api.PrpdCompanyService;
import cn.com.libertymutual.production.service.api.PrpdLogOperationService;
import cn.com.libertymutual.production.service.api.PrpdLogSettingService;
import cn.com.libertymutual.production.service.api.PrpdRiskPlanService;
import cn.com.libertymutual.production.service.api.PrpdRiskService;
import cn.com.libertymutual.production.service.api.PrpsAgentService;
import cn.com.libertymutual.production.service.api.UserAgentService;
import cn.com.libertymutual.production.utils.Constant;

import java.util.List;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class CommonBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected PrpdLogSettingService prpdLogSettingService;
	@Autowired
	protected PrpdLogOperationService prpdLogOperationService;
	@Autowired
	protected PrpdClassService prpdClassService;
	@Autowired
	protected PrpdRiskService prpdRiskService;
	@Autowired
	protected PrpdRiskPlanService prpdRiskPlanService;
	@Autowired
	protected PrpdCompanyService prpdCompanyService;
	@Autowired
	protected PrpsAgentService prpsAgentService;
	@Autowired
	protected BranchService branchService;
	@Autowired
	protected UserAgentService userAgentService;
	
	/**
	 * 获取机构信息
	 * @param request
	 * @return
	 */
	public abstract Response findPrpdCompany(Request request);
	public abstract Response findBranch(Request request) ;
	
	/**
	 * 获取所有险别
	 * @return
	 */
	public abstract Response findAllPrpdClass();
	
	/**
	 * 获取非组合险的险别
	 * @return
	 */
	public abstract Response findClassNotComposite();
	
	/**
	 * 获取所属险类及组合险的所有险种
	 * @param request
	 * @param compositeClasses
	 * @return
	 */
	public abstract Response findByClassAndComposite(PrpdRiskRequest request);
	
	/**
	 * 获取所有险种
	 * @return
	 */
	public abstract Response findAllPrpdRisk();
	
	/**
	 * 获取所有险种(分页)
	 * @param request
	 * @return
	 */
	public abstract Response findRisksByPage(PrpdRiskRequest request);
	
	/**
	 * 获取操作日志
	 * @param logRequest
	 * @return
	 */
	public abstract Response findLogs(LogRequest logRequest);
	
	/**
	 * 获取日志详情
	 * @param logRequest
	 * @return
	 */
	public abstract Response findLogDetail(LogRequest logRequest);
	
	/**
	 * 获取机构对应的渠道信息
	 * @param request
	 * @return
	 */
	public abstract Response findPrpsAgentByComCode(String comCodes, Request request);
	public abstract Response findUsersAgentByComCode(String comCodes, Request request);
	
	/**
	 * 查询险种关联的方案
	 * @param riskCodes
	 * @return
	 */
	public abstract Response findPlansByRisks(String riskCodes);
	
	/**
	 * 查询分支机构
	 * @return
	 */
	public abstract Response findCompanys();
	public abstract Response findNextBranchs();
	
	/**
	 * 根据险别查询对应的险种
	 * @param classcode
	 * @return
	 */
	public abstract Response findRiskByClass(String classcode);

	/**
	 * 查询分支机构
	 * @param comcode
	 * @return
     */
	public abstract Response queryCompanysBycodes(List<String> comcodes);
	
	public abstract Response queryBranchsBycodes(List<String> comcodes);
}