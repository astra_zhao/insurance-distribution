package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Prpdrisk extends PrpdriskKey {
    private String statriskcode;

    private String riskcname;

    private String riskename;

    private String classcode;

    private String producttypecode;

    private String groupcode;

    private Long calculator;

    private Date salestartdate;

    private Date saleenddate;

    private String enddateflag;

    private Date effectivestartdate;

    private Date invalidenddate;

    private String insuranceperiodtype;

    private Long insuranceperiod;

    private Long starthour;

    private Long startminute;

    private Long endhour;

    private Long endminute;

    private String templetriskcode;

    private String riskflag;

    private String newriskcode;

    private String articlecode;

    private String manageflag;

    private String settletype;

    private String validstatus;

    private String flag;

    private String usecommonuw;

    private String calculatepremiumtype;

    private String compositeflag;

    public String getStatriskcode() {
        return statriskcode;
    }

    public void setStatriskcode(String statriskcode) {
        this.statriskcode = statriskcode == null ? null : statriskcode.trim();
    }

    public String getRiskcname() {
        return riskcname;
    }

    public void setRiskcname(String riskcname) {
        this.riskcname = riskcname == null ? null : riskcname.trim();
    }

    public String getRiskename() {
        return riskename;
    }

    public void setRiskename(String riskename) {
        this.riskename = riskename == null ? null : riskename.trim();
    }

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode == null ? null : classcode.trim();
    }

    public String getProducttypecode() {
        return producttypecode;
    }

    public void setProducttypecode(String producttypecode) {
        this.producttypecode = producttypecode == null ? null : producttypecode.trim();
    }

    public String getGroupcode() {
        return groupcode;
    }

    public void setGroupcode(String groupcode) {
        this.groupcode = groupcode == null ? null : groupcode.trim();
    }

    public Long getCalculator() {
        return calculator;
    }

    public void setCalculator(Long calculator) {
        this.calculator = calculator;
    }

    public Date getSalestartdate() {
        return salestartdate;
    }

    public void setSalestartdate(Date salestartdate) {
        this.salestartdate = salestartdate;
    }

    public Date getSaleenddate() {
        return saleenddate;
    }

    public void setSaleenddate(Date saleenddate) {
        this.saleenddate = saleenddate;
    }

    public String getEnddateflag() {
        return enddateflag;
    }

    public void setEnddateflag(String enddateflag) {
        this.enddateflag = enddateflag == null ? null : enddateflag.trim();
    }

    public Date getEffectivestartdate() {
        return effectivestartdate;
    }

    public void setEffectivestartdate(Date effectivestartdate) {
        this.effectivestartdate = effectivestartdate;
    }

    public Date getInvalidenddate() {
        return invalidenddate;
    }

    public void setInvalidenddate(Date invalidenddate) {
        this.invalidenddate = invalidenddate;
    }

    public String getInsuranceperiodtype() {
        return insuranceperiodtype;
    }

    public void setInsuranceperiodtype(String insuranceperiodtype) {
        this.insuranceperiodtype = insuranceperiodtype == null ? null : insuranceperiodtype.trim();
    }

    public Long getInsuranceperiod() {
        return insuranceperiod;
    }

    public void setInsuranceperiod(Long insuranceperiod) {
        this.insuranceperiod = insuranceperiod;
    }

    public Long getStarthour() {
        return starthour;
    }

    public void setStarthour(Long starthour) {
        this.starthour = starthour;
    }

    public Long getStartminute() {
        return startminute;
    }

    public void setStartminute(Long startminute) {
        this.startminute = startminute;
    }

    public Long getEndhour() {
        return endhour;
    }

    public void setEndhour(Long endhour) {
        this.endhour = endhour;
    }

    public Long getEndminute() {
        return endminute;
    }

    public void setEndminute(Long endminute) {
        this.endminute = endminute;
    }

    public String getTempletriskcode() {
        return templetriskcode;
    }

    public void setTempletriskcode(String templetriskcode) {
        this.templetriskcode = templetriskcode == null ? null : templetriskcode.trim();
    }

    public String getRiskflag() {
        return riskflag;
    }

    public void setRiskflag(String riskflag) {
        this.riskflag = riskflag == null ? null : riskflag.trim();
    }

    public String getNewriskcode() {
        return newriskcode;
    }

    public void setNewriskcode(String newriskcode) {
        this.newriskcode = newriskcode == null ? null : newriskcode.trim();
    }

    public String getArticlecode() {
        return articlecode;
    }

    public void setArticlecode(String articlecode) {
        this.articlecode = articlecode == null ? null : articlecode.trim();
    }

    public String getManageflag() {
        return manageflag;
    }

    public void setManageflag(String manageflag) {
        this.manageflag = manageflag == null ? null : manageflag.trim();
    }

    public String getSettletype() {
        return settletype;
    }

    public void setSettletype(String settletype) {
        this.settletype = settletype == null ? null : settletype.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getUsecommonuw() {
        return usecommonuw;
    }

    public void setUsecommonuw(String usecommonuw) {
        this.usecommonuw = usecommonuw == null ? null : usecommonuw.trim();
    }

    public String getCalculatepremiumtype() {
        return calculatepremiumtype;
    }

    public void setCalculatepremiumtype(String calculatepremiumtype) {
        this.calculatepremiumtype = calculatepremiumtype == null ? null : calculatepremiumtype.trim();
    }

    public String getCompositeflag() {
        return compositeflag;
    }

    public void setCompositeflag(String compositeflag) {
        this.compositeflag = compositeflag == null ? null : compositeflag.trim();
    }
}