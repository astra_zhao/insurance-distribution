package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.req.RoleReq;

public interface RoleService {

	ServiceResult roleList(String type,int pageNumber, int pageSize);

	ServiceResult roleMenuList(String roleId);

	ServiceResult addRoleMenu(String roleId, String[] list);

	ServiceResult menuList();

	ServiceResult updateStatus(String type,Integer roleId);

	ServiceResult setPermission(RoleReq roleReq);

	ServiceResult addRole(RoleReq roleReq);


}
