package cn.com.libertymutual.production.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public class Constant {

	public static final Logger log = LoggerFactory.getLogger(Constant.class);
	
	public static enum BUSINESSTYPE {
		kind,
		plan,
		item,
	}
	
	public static enum BUSINESSLOGTYPE {
		KIND("条款管理"),
		PLAN("方案管理"),
		CLAUSE("特约管理"),
		EFILE("备案号管理"),
		LINKITEM("关联标的责任"),
		LINKRISK("关联险种");
		
		private String name;
		
		private BUSINESSLOGTYPE(String name) {
			this.name = name;
		}
		
		public String toString() {
			return this.name;
		}
	}
	
	public static enum OPERTYPE {
		INSERT("新增"),
	    DELETE("删除"),
	    UPDATE("修改"),
	    SELECT("查询");
	 
	    private String name;
	 
	    private OPERTYPE(String name) {
	    	this.name = name;
	    }
	 
	    public String toString() {
	        return this.name;
	    }
	}
}
