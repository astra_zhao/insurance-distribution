package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdserialno;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdserialnoExample;
@Mapper
public interface PrpdserialnoMapper {
    int countByExample(PrpdserialnoExample example);

    int deleteByExample(PrpdserialnoExample example);

    int insert(Prpdserialno record);

    int insertSelective(Prpdserialno record);

    List<Prpdserialno> selectByExample(PrpdserialnoExample example);

    int updateByExampleSelective(@Param("record") Prpdserialno record, @Param("example") PrpdserialnoExample example);

    int updateByExample(@Param("record") Prpdserialno record, @Param("example") PrpdserialnoExample example);
}