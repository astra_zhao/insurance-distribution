package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;

/**
 * 投保单基本信息
 * 
 * @author SHAOLIN
 *
 */
public class TPrptMainDto implements Serializable {
	private static final long serialVersionUID = 6810496396567983883L;
	/*
	 * private String ProposalNo;// 投保单号
	 * 
	 * private String ClassCode;// 险类
	 * 
	 * private String RiskCode;// 险种代码
	 * 
	 * private String riskVersion;// 险种版本
	 * 
	 * private String AgreementNo;// 业务关系代码
	 * 
	 * private String BusinessNatureName="8";// 业务来源
	 * 
	 * private String ComCode;// 归属部门
	 * 
	 * private String Handler1Code;// 业务员
	 * 
	 * private String StartDate;// 保险期间-自
	 * 
	 * private String StartHour;// 保险期间-起时
	 * 
	 * private String EndDate;// 保险期间-到
	 * 
	 * private String EndHour;// 保险期间-止时
	 * 
	 * private String OperateDate;// 投保日期
	 * 
	 * private String SignDate;// 制单日期
	 * 
	 * private String SumAmount;// 总保险金额
	 * 
	 * private String SumPremium;// 总保险费
	 * 
	 * private String ArgueSolution;// 合同争议解决方式代码
	 * 
	 * private String ArgueSolutionName;// 合同争议解决方式(仲裁机构)
	 * 
	 * private String OperatorCode;// 操作员
	 * 
	 * private String InputDate;// 录单日期
	 * 
	 * private String UpdaterCode;// 最近修改人
	 * 
	 * private String UnderWriteCode;// 核保人
	 * 
	 * private String UnderWriteEndDate;// 核保通过日期
	 * 
	 * private String MakeCode;// 制单机构
	 * 
	 * private String papolicyno;// 续保单号
	 * 
	 * private String sumDiscount;// 折扣优惠总金额
	 * 
	 * private String sumSubprem;// 总附加险保费
	 * 
	 * private String carinsuranceType;// 营运类型
	 * 
	 * private String PersonalAgentLicenseNo;// 个人代理资格证号
	 * 
	 * private String IntermediaryLicenseNo;// 中介机构许可证号
	 * 
	 * private String CountyCode;// 指定查询区域
	 * 
	 * private String pureriskpremiumflag;// 纯风险保费标志
	 * 
	 * private String PureRiskPremium;// 基准纯风险保费
	 * 
	 * private String referencepurerisk;// 全损调整车损险基准纯风险保费
	 * 
	 * private String MainRemark;// 出单员意见
	 * 
	 * private String kindmaintype;// 条款体系 private String MtplStartDate;// 交强险期间-自
	 * add shiqilin private String MtplStartHour;// 交强险期间-起时add shiqilin private
	 * String MtplEndDate ;//交强险期间-到add shiqilin private String MtplEndHour;//
	 * 交强险期间-止时add shiqilin private String ContractNo;//协议号
	 * 
	 * 
	 * //Production Management（查询）所增加 private String EditType; //编辑类型 private String
	 * PolicySort;//保单类型 private String businessLanguage;//语种 private String
	 * CoinsFlag;//联共保标识 private String BusinessFlag;//分入标志 private String
	 * AffinityCode;//Affinity private String PolicyNatureFlag;//市外/统括业务 private
	 * String PolicyType;//保单类型
	 * 
	 * //ewai private String AnonymityFlagE;//是否记名承保
	 * 
	 * private String JudicalScope;//司法管辖 private String insuranceCompany;//保险人名称;
	 * private String appliInsuredName;//投保人名称 private String riskName;//险种名称
	 * private String businessNature;//保单业务来源方式 private String agentName;//代理人名称
	 * private String commissionPayee;//佣金支付对象 private String policyStatus;//保单状态
	 * private String poaPlanId;//保单标识码 private String ownerComCode;//归属机构代码 private
	 * String handler1Name;//业务员名称 private String paidFee;//已缴保费 private String
	 * paidDate;//缴费时间 private String engages;//特别约定\明示告知 private String
	 * endorseNos;//批单号 private String Notice;//明示告知 private String
	 * salerNumber;//销售人员 private String PoliSource;//业务来源
	 * 
	 * private String MtplPolicyNo;//交强险保单号 private String PolicyNo;//保单号
	 * 
	 * private String mtplproposalNo;//投保单号
	 */
	protected String affinityCode;
	private String combineFlag;
	private String autonomyAdjustValue;
	private String channelAdjustValue;
	private String trdSalesCode;

	protected String agentName;
	protected String agreementNo;
	protected String anonymityFlagE;
	protected String appliInsuredName;
	protected String argueSolution;
	protected String argueSolutionName;
	protected String businessFlag;
	protected String businessLanguage;
	protected String businessNature;
	protected String businessNatureName;
	protected String carinsuranceType;
	protected String classCode;
	protected String coinsFlag;
	protected String comCode;
	protected String commissionPayee;
	protected String contractNo;
	protected String countyCode;
	protected String editType;
	protected String endDate;
	protected String endHour;
	protected String endorseNos;
	protected String engages;
	protected String handler1Code;
	protected String handler1Name;
	protected String inputDate;
	protected String insuranceCompany;
	protected String intermediaryLicenseNo;
	protected String judicalScope;
	protected String kindmaintype;
	protected String mainRemark;
	protected String makeCode;
	protected String mtplEndDate;
	protected String mtplEndHour;
	protected String mtplPolicyNo;
	protected String mtplStartDate;
	protected String mtplStartHour;
	protected String mtplproposalNo;
	protected String notice;
	protected String operateDate;
	protected String operatorCode;
	protected String ownerComCode;
	protected String paidDate;
	protected String paidFee;
	protected String papolicyno;
	protected String personalAgentLicenseNo;
	protected String poaPlanId;
	protected String poliSource;
	protected String policyNatureFlag;
	protected String policyNo;
	protected String policySort;
	protected String policyStatus;
	protected String policyType;
	protected String proposalNo;
	protected String pureRiskPremium;
	protected String pureriskpremiumflag;
	protected String referencepurerisk;
	protected String riskCode;
	protected String riskName;
	protected String riskVersion;
	protected String salerName;
	protected String salerNumber;
	protected String signDate;
	protected String startDate;
	protected String startHour;
	protected String sumAmount;
	protected String sumDiscount;
	protected String sumPremium;
	protected String sumSubprem;
	protected String underWriteCode;
	protected String underWriteEndDate;
	protected String updaterCode;
	private String agentCode;// 代理人编码
	private String comName;// 业务部门
	private String insuredName;// 被保人名称
	private String ip;// 当userflag 为0的时候必传
	private String usbKey;// 当userflag 为1的时候必传
	private String userFlag;// 1外部用户、0内部用户
	private String veriCode;
	private String callBackUrl;

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCombineFlag() {
		return combineFlag;
	}

	public void setCombineFlag(String combineFlag) {
		this.combineFlag = combineFlag;
	}

	public String getAutonomyAdjustValue() {
		return autonomyAdjustValue;
	}

	public void setAutonomyAdjustValue(String autonomyAdjustValue) {
		this.autonomyAdjustValue = autonomyAdjustValue;
	}

	public String getChannelAdjustValue() {
		return channelAdjustValue;
	}

	public void setChannelAdjustValue(String channelAdjustValue) {
		this.channelAdjustValue = channelAdjustValue;
	}

	public String getTrdSalesCode() {
		return trdSalesCode;
	}

	public void setTrdSalesCode(String trdSalesCode) {
		this.trdSalesCode = trdSalesCode;
	}

	/**
	 * Gets the value of the affinityCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getAffinityCode() {
		return affinityCode;
	}

	/**
	 * Sets the value of the affinityCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setAffinityCode(String value) {
		this.affinityCode = value;
	}

	/**
	 * Gets the value of the agentName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the value of the agentName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setAgentName(String value) {
		this.agentName = value;
	}

	/**
	 * Gets the value of the agreementNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getAgreementNo() {
		return agreementNo;
	}

	/**
	 * Sets the value of the agreementNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setAgreementNo(String value) {
		this.agreementNo = value;
	}

	/**
	 * Gets the value of the anonymityFlagE property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getAnonymityFlagE() {
		return anonymityFlagE;
	}

	/**
	 * Sets the value of the anonymityFlagE property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setAnonymityFlagE(String value) {
		this.anonymityFlagE = value;
	}

	/**
	 * Gets the value of the appliInsuredName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getAppliInsuredName() {
		return appliInsuredName;
	}

	/**
	 * Sets the value of the appliInsuredName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setAppliInsuredName(String value) {
		this.appliInsuredName = value;
	}

	/**
	 * Gets the value of the argueSolution property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getArgueSolution() {
		return argueSolution;
	}

	/**
	 * Sets the value of the argueSolution property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setArgueSolution(String value) {
		this.argueSolution = value;
	}

	/**
	 * Gets the value of the argueSolutionName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getArgueSolutionName() {
		return argueSolutionName;
	}

	/**
	 * Sets the value of the argueSolutionName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setArgueSolutionName(String value) {
		this.argueSolutionName = value;
	}

	/**
	 * Gets the value of the businessFlag property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getBusinessFlag() {
		return businessFlag;
	}

	/**
	 * Sets the value of the businessFlag property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setBusinessFlag(String value) {
		this.businessFlag = value;
	}

	/**
	 * Gets the value of the businessLanguage property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getBusinessLanguage() {
		return businessLanguage;
	}

	/**
	 * Sets the value of the businessLanguage property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setBusinessLanguage(String value) {
		this.businessLanguage = value;
	}

	/**
	 * Gets the value of the businessNature property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getBusinessNature() {
		return businessNature;
	}

	/**
	 * Sets the value of the businessNature property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setBusinessNature(String value) {
		this.businessNature = value;
	}

	/**
	 * Gets the value of the businessNatureName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getBusinessNatureName() {
		return businessNatureName;
	}

	/**
	 * Sets the value of the businessNatureName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setBusinessNatureName(String value) {
		this.businessNatureName = value;
	}

	/**
	 * Gets the value of the carinsuranceType property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getCarinsuranceType() {
		return carinsuranceType;
	}

	/**
	 * Sets the value of the carinsuranceType property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setCarinsuranceType(String value) {
		this.carinsuranceType = value;
	}

	/**
	 * Gets the value of the classCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * Sets the value of the classCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setClassCode(String value) {
		this.classCode = value;
	}

	/**
	 * Gets the value of the coinsFlag property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getCoinsFlag() {
		return coinsFlag;
	}

	/**
	 * Sets the value of the coinsFlag property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setCoinsFlag(String value) {
		this.coinsFlag = value;
	}

	/**
	 * Gets the value of the comCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getComCode() {
		return comCode;
	}

	/**
	 * Sets the value of the comCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setComCode(String value) {
		this.comCode = value;
	}

	/**
	 * Gets the value of the commissionPayee property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getCommissionPayee() {
		return commissionPayee;
	}

	/**
	 * Sets the value of the commissionPayee property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setCommissionPayee(String value) {
		this.commissionPayee = value;
	}

	/**
	 * Gets the value of the contractNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getContractNo() {
		return contractNo;
	}

	/**
	 * Sets the value of the contractNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setContractNo(String value) {
		this.contractNo = value;
	}

	/**
	 * Gets the value of the countyCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getCountyCode() {
		return countyCode;
	}

	/**
	 * Sets the value of the countyCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setCountyCode(String value) {
		this.countyCode = value;
	}

	/**
	 * Gets the value of the editType property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getEditType() {
		return editType;
	}

	/**
	 * Sets the value of the editType property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setEditType(String value) {
		this.editType = value;
	}

	/**
	 * Gets the value of the endHour property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getEndHour() {
		return endHour;
	}

	/**
	 * Sets the value of the endHour property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setEndHour(String value) {
		this.endHour = value;
	}

	/**
	 * Gets the value of the endorseNos property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getEndorseNos() {
		return endorseNos;
	}

	/**
	 * Sets the value of the endorseNos property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setEndorseNos(String value) {
		this.endorseNos = value;
	}

	/**
	 * Gets the value of the engages property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getEngages() {
		return engages;
	}

	/**
	 * Sets the value of the engages property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setEngages(String value) {
		this.engages = value;
	}

	/**
	 * Gets the value of the handler1Code property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getHandler1Code() {
		return handler1Code;
	}

	/**
	 * Sets the value of the handler1Code property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setHandler1Code(String value) {
		this.handler1Code = value;
	}

	/**
	 * Gets the value of the handler1Name property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getHandler1Name() {
		return handler1Name;
	}

	/**
	 * Sets the value of the handler1Name property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setHandler1Name(String value) {
		this.handler1Name = value;
	}

	/**
	 * Gets the value of the inputDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getInputDate() {
		return inputDate;
	}

	/**
	 * Sets the value of the inputDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setInputDate(String value) {
		this.inputDate = value;
	}

	/**
	 * Gets the value of the insuranceCompany property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	/**
	 * Sets the value of the insuranceCompany property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setInsuranceCompany(String value) {
		this.insuranceCompany = value;
	}

	/**
	 * Gets the value of the intermediaryLicenseNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getIntermediaryLicenseNo() {
		return intermediaryLicenseNo;
	}

	/**
	 * Sets the value of the intermediaryLicenseNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setIntermediaryLicenseNo(String value) {
		this.intermediaryLicenseNo = value;
	}

	/**
	 * Gets the value of the judicalScope property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getJudicalScope() {
		return judicalScope;
	}

	/**
	 * Sets the value of the judicalScope property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setJudicalScope(String value) {
		this.judicalScope = value;
	}

	/**
	 * Gets the value of the kindmaintype property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getKindmaintype() {
		return kindmaintype;
	}

	/**
	 * Sets the value of the kindmaintype property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setKindmaintype(String value) {
		this.kindmaintype = value;
	}

	/**
	 * Gets the value of the mainRemark property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMainRemark() {
		return mainRemark;
	}

	/**
	 * Sets the value of the mainRemark property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMainRemark(String value) {
		this.mainRemark = value;
	}

	/**
	 * Gets the value of the makeCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMakeCode() {
		return makeCode;
	}

	/**
	 * Sets the value of the makeCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMakeCode(String value) {
		this.makeCode = value;
	}

	/**
	 * Gets the value of the mtplEndDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplEndDate() {
		return mtplEndDate;
	}

	/**
	 * Sets the value of the mtplEndDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplEndDate(String value) {
		this.mtplEndDate = value;
	}

	/**
	 * Gets the value of the mtplEndHour property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplEndHour() {
		return mtplEndHour;
	}

	/**
	 * Sets the value of the mtplEndHour property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplEndHour(String value) {
		this.mtplEndHour = value;
	}

	/**
	 * Gets the value of the mtplPolicyNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplPolicyNo() {
		return mtplPolicyNo;
	}

	/**
	 * Sets the value of the mtplPolicyNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplPolicyNo(String value) {
		this.mtplPolicyNo = value;
	}

	/**
	 * Gets the value of the mtplStartDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplStartDate() {
		return mtplStartDate;
	}

	/**
	 * Sets the value of the mtplStartDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplStartDate(String value) {
		this.mtplStartDate = value;
	}

	/**
	 * Gets the value of the mtplStartHour property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplStartHour() {
		return mtplStartHour;
	}

	/**
	 * Sets the value of the mtplStartHour property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplStartHour(String value) {
		this.mtplStartHour = value;
	}

	/**
	 * Gets the value of the mtplproposalNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getMtplproposalNo() {
		return mtplproposalNo;
	}

	/**
	 * Sets the value of the mtplproposalNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setMtplproposalNo(String value) {
		this.mtplproposalNo = value;
	}

	/**
	 * Gets the value of the notice property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * Sets the value of the notice property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setNotice(String value) {
		this.notice = value;
	}

	/**
	 * Gets the value of the operateDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOperateDate() {
		return operateDate;
	}

	/**
	 * Sets the value of the operateDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOperateDate(String value) {
		this.operateDate = value;
	}

	/**
	 * Gets the value of the operatorCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOperatorCode() {
		return operatorCode;
	}

	/**
	 * Sets the value of the operatorCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOperatorCode(String value) {
		this.operatorCode = value;
	}

	/**
	 * Gets the value of the ownerComCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getOwnerComCode() {
		return ownerComCode;
	}

	/**
	 * Sets the value of the ownerComCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setOwnerComCode(String value) {
		this.ownerComCode = value;
	}

	/**
	 * Gets the value of the paidDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPaidDate() {
		return paidDate;
	}

	/**
	 * Sets the value of the paidDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPaidDate(String value) {
		this.paidDate = value;
	}

	/**
	 * Gets the value of the paidFee property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPaidFee() {
		return paidFee;
	}

	/**
	 * Sets the value of the paidFee property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPaidFee(String value) {
		this.paidFee = value;
	}

	/**
	 * Gets the value of the papolicyno property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPapolicyno() {
		return papolicyno;
	}

	/**
	 * Sets the value of the papolicyno property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPapolicyno(String value) {
		this.papolicyno = value;
	}

	/**
	 * Gets the value of the personalAgentLicenseNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPersonalAgentLicenseNo() {
		return personalAgentLicenseNo;
	}

	/**
	 * Sets the value of the personalAgentLicenseNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPersonalAgentLicenseNo(String value) {
		this.personalAgentLicenseNo = value;
	}

	/**
	 * Gets the value of the poaPlanId property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPoaPlanId() {
		return poaPlanId;
	}

	/**
	 * Sets the value of the poaPlanId property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPoaPlanId(String value) {
		this.poaPlanId = value;
	}

	/**
	 * Gets the value of the poliSource property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPoliSource() {
		return poliSource;
	}

	/**
	 * Sets the value of the poliSource property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPoliSource(String value) {
		this.poliSource = value;
	}

	/**
	 * Gets the value of the policyNatureFlag property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPolicyNatureFlag() {
		return policyNatureFlag;
	}

	/**
	 * Sets the value of the policyNatureFlag property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPolicyNatureFlag(String value) {
		this.policyNatureFlag = value;
	}

	/**
	 * Gets the value of the policyNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPolicyNo() {
		return policyNo;
	}

	/**
	 * Sets the value of the policyNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPolicyNo(String value) {
		this.policyNo = value;
	}

	/**
	 * Gets the value of the policySort property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPolicySort() {
		return policySort;
	}

	/**
	 * Sets the value of the policySort property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPolicySort(String value) {
		this.policySort = value;
	}

	/**
	 * Gets the value of the policyStatus property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPolicyStatus() {
		return policyStatus;
	}

	/**
	 * Sets the value of the policyStatus property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPolicyStatus(String value) {
		this.policyStatus = value;
	}

	/**
	 * Gets the value of the policyType property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * Sets the value of the policyType property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPolicyType(String value) {
		this.policyType = value;
	}

	/**
	 * Gets the value of the proposalNo property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getProposalNo() {
		return proposalNo;
	}

	/**
	 * Sets the value of the proposalNo property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setProposalNo(String value) {
		this.proposalNo = value;
	}

	/**
	 * Gets the value of the pureRiskPremium property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPureRiskPremium() {
		return pureRiskPremium;
	}

	/**
	 * Sets the value of the pureRiskPremium property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPureRiskPremium(String value) {
		this.pureRiskPremium = value;
	}

	/**
	 * Gets the value of the pureriskpremiumflag property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getPureriskpremiumflag() {
		return pureriskpremiumflag;
	}

	/**
	 * Sets the value of the pureriskpremiumflag property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setPureriskpremiumflag(String value) {
		this.pureriskpremiumflag = value;
	}

	/**
	 * Gets the value of the referencepurerisk property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getReferencepurerisk() {
		return referencepurerisk;
	}

	/**
	 * Sets the value of the referencepurerisk property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setReferencepurerisk(String value) {
		this.referencepurerisk = value;
	}

	/**
	 * Gets the value of the riskCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getRiskCode() {
		return riskCode;
	}

	/**
	 * Sets the value of the riskCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setRiskCode(String value) {
		this.riskCode = value;
	}

	/**
	 * Gets the value of the riskName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getRiskName() {
		return riskName;
	}

	/**
	 * Sets the value of the riskName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setRiskName(String value) {
		this.riskName = value;
	}

	/**
	 * Gets the value of the riskVersion property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getRiskVersion() {
		return riskVersion;
	}

	/**
	 * Sets the value of the riskVersion property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setRiskVersion(String value) {
		this.riskVersion = value;
	}

	/**
	 * Gets the value of the salerName property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSalerName() {
		return salerName;
	}

	/**
	 * Sets the value of the salerName property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSalerName(String value) {
		this.salerName = value;
	}

	/**
	 * Gets the value of the salerNumber property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSalerNumber() {
		return salerNumber;
	}

	/**
	 * Sets the value of the salerNumber property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSalerNumber(String value) {
		this.salerNumber = value;
	}

	/**
	 * Gets the value of the signDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSignDate() {
		return signDate;
	}

	/**
	 * Sets the value of the signDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSignDate(String value) {
		this.signDate = value;
	}

	/**
	 * Gets the value of the startDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Sets the value of the startDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setStartDate(String value) {
		this.startDate = value;
	}

	/**
	 * Gets the value of the startHour property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getStartHour() {
		return startHour;
	}

	/**
	 * Sets the value of the startHour property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setStartHour(String value) {
		this.startHour = value;
	}

	/**
	 * Gets the value of the sumAmount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSumAmount() {
		return sumAmount;
	}

	/**
	 * Sets the value of the sumAmount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSumAmount(String value) {
		this.sumAmount = value;
	}

	/**
	 * Gets the value of the sumDiscount property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSumDiscount() {
		return sumDiscount;
	}

	/**
	 * Sets the value of the sumDiscount property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSumDiscount(String value) {
		this.sumDiscount = value;
	}

	/**
	 * Gets the value of the sumPremium property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSumPremium() {
		return sumPremium;
	}

	/**
	 * Sets the value of the sumPremium property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSumPremium(String value) {
		this.sumPremium = value;
	}

	/**
	 * Gets the value of the sumSubprem property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getSumSubprem() {
		return sumSubprem;
	}

	/**
	 * Sets the value of the sumSubprem property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setSumSubprem(String value) {
		this.sumSubprem = value;
	}

	/**
	 * Gets the value of the underWriteCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getUnderWriteCode() {
		return underWriteCode;
	}

	/**
	 * Sets the value of the underWriteCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setUnderWriteCode(String value) {
		this.underWriteCode = value;
	}

	/**
	 * Gets the value of the underWriteEndDate property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getUnderWriteEndDate() {
		return underWriteEndDate;
	}

	/**
	 * Sets the value of the underWriteEndDate property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setUnderWriteEndDate(String value) {
		this.underWriteEndDate = value;
	}

	/**
	 * Gets the value of the updaterCode property.
	 * 
	 * @return
	 *     possible object is
	 *     {@link String }
	 *     
	 */
	public String getUpdaterCode() {
		return updaterCode;
	}

	/**
	 * Sets the value of the updaterCode property.
	 * 
	 * @param value
	 *     allowed object is
	 *     {@link String }
	 *     
	 */
	public void setUpdaterCode(String value) {
		this.updaterCode = value;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsbKey() {
		return usbKey;
	}

	public void setUsbKey(String usbKey) {
		this.usbKey = usbKey;
	}

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public String getVeriCode() {
		return veriCode;
	}

	public void setVeriCode(String veriCode) {
		this.veriCode = veriCode;
	}
}