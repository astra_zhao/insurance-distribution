
package cn.com.libertymutual.sp.webService.allpolicy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyListQueryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyListQueryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyListQueryResponseDto" type="{http://prpall.liberty.com/all/cb/policyListQuery/bean}policyListQueryResponseDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyListQueryResponse", namespace = "http://prpall.liberty.com/all/cb/policyListQuery/intf", propOrder = {
    "policyListQueryResponseDto"
})
public class PolicyListQueryResponse {

    protected PolicyListQueryResponseDto policyListQueryResponseDto;

    /**
     * Gets the value of the policyListQueryResponseDto property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyListQueryResponseDto }
     *     
     */
    public PolicyListQueryResponseDto getPolicyListQueryResponseDto() {
        return policyListQueryResponseDto;
    }

    /**
     * Sets the value of the policyListQueryResponseDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyListQueryResponseDto }
     *     
     */
    public void setPolicyListQueryResponseDto(PolicyListQueryResponseDto value) {
        this.policyListQueryResponseDto = value;
    }

}
