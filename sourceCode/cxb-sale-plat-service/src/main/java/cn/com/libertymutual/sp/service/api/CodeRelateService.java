package cn.com.libertymutual.sp.service.api;

import java.util.List;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.SelectDto;
import cn.com.libertymutual.sys.bean.SysCodeRelate;

public interface CodeRelateService {
	ServiceResult getSaleInitData(String codeType);

	ServiceResult findSaleInitUrlData(String userName);

	ServiceResult getCliaimsType(String code, String language);

	ServiceResult getSeleCountry(String planId);

	ServiceResult addRisk(SysCodeRelate sysCodeRelate);

	ServiceResult riskListPage(int pageNumber, int pageSize);

	ServiceResult getCarlimitNo();

	ServiceResult removeRisk(Integer id);

	ServiceResult riskList();

	List<SelectDto> getCodeRelateRisk(String codeType, String risk);

	List<SelectDto> getCodeRelateBranch(String codeType, String branchCode);

	ServiceResult queryCode(String level, String codeCode, int levelMax, int levelMin, boolean type);

}
