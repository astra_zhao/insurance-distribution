package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_activity")
public class TbSpActivity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566150338099015883L;

	private Integer id;
	private String activityCode;
	private String activityType;
	private String touchOffType;
	private String activityName;
	private Integer drawTimes;
	private Integer drawBalance;
	private String branchCode;
	private String status;
	private Double sumBudget;
	private Double balanceBudget;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date planStartTime;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date planEndTime;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date actualStartTime;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date actualEndTime;
	private Double warnBalance;
	private String warnPhoneNo;
	private String warnEmail;
	private String applyer;
	private Double policyRate;

	private Double amountLimitation;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date applyTime;
	private String approver;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date approveTime;
	private String stopApplyer;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date stopApplyTime;
	private String stopApprover;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date stopApproveTime;
	private String remark;
	private String activityScript;
	private Double minValue;
	private Integer maxTimes;
	private Double specialValue;
	private Integer specialTimes;
	private String hasRemind;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "POLICY_RATE")
	public Double getPolicyRate() {
		return policyRate;
	}

	public void setPolicyRate(Double policyRate) {
		this.policyRate = policyRate;
	}

	@Column(name = "amount_limitation")
	public Double getAmountLimitation() {
		return amountLimitation;
	}

	public void setAmountLimitation(Double amountLimitation) {
		this.amountLimitation = amountLimitation;
	}

	@Column(name = "activity_code", length = 20)
	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	@Column(name = "draw_times", length = 10)
	public Integer getDrawTimes() {
		return drawTimes;
	}

	public void setDrawTimes(Integer drawTimes) {
		this.drawTimes = drawTimes;
	}

	@Column(name = "draw_balance", length = 10)
	public Integer getDrawBalance() {
		return drawBalance;
	}

	public void setDrawBalance(Integer drawBalance) {
		this.drawBalance = drawBalance;
	}

	@Column(name = "activity_type", length = 2)
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@Column(name = "touchOff_type", length = 2)
	public String getTouchOffType() {
		return touchOffType;
	}

	public void setTouchOffType(String touchOffType) {
		this.touchOffType = touchOffType;
	}

	@Column(name = "actvity_name", length = 100)
	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	@Column(name = "branch_code", length = 20)
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@Column(name = "status", length = 2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "sum_budget")
	public Double getSumBudget() {
		return sumBudget;
	}

	public void setSumBudget(Double sumBudget) {
		this.sumBudget = sumBudget;
	}

	@Column(name = "balance_budget")
	public Double getBalanceBudget() {
		return balanceBudget;
	}

	public void setBalanceBudget(Double balanceBudget) {
		this.balanceBudget = balanceBudget;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "plan_starttime")
	public Date getPlanStartTime() {
		return planStartTime;
	}

	public void setPlanStartTime(Date planStartTime) {
		this.planStartTime = planStartTime;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "plan_endtime")
	public Date getPlanEndTime() {
		return planEndTime;
	}

	public void setPlanEndTime(Date planEndTime) {
		this.planEndTime = planEndTime;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "actual_starttime")
	public Date getActualStartTime() {
		return actualStartTime;
	}

	public void setActualStartTime(Date actualStartTime) {
		this.actualStartTime = actualStartTime;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "actual_endtime")
	public Date getActualEndTime() {
		return actualEndTime;
	}

	public void setActualEndTime(Date actualEndTime) {
		this.actualEndTime = actualEndTime;
	}

	@Column(name = "warn_balance")
	public Double getWarnBalance() {
		return warnBalance;
	}

	public void setWarnBalance(Double warnBalance) {
		this.warnBalance = warnBalance;
	}

	@Column(name = "warn_phoneno")
	public String getWarnPhoneNo() {
		return warnPhoneNo;
	}

	public void setWarnPhoneNo(String warnPhoneNo) {
		this.warnPhoneNo = warnPhoneNo;
	}

	@Column(name = "warn_email")
	public String getWarnEmail() {
		return warnEmail;
	}

	public void setWarnEmail(String warnEmail) {
		this.warnEmail = warnEmail;
	}

	@Column(name = "applyer")
	public String getApplyer() {
		return applyer;
	}

	public void setApplyer(String applyer) {
		this.applyer = applyer;
	}

	@Column(name = "apply_time")
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	@Column(name = "approver")
	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	@Column(name = "approve_time")
	public Date getApproveTime() {
		return approveTime;
	}

	public void setApproveTime(Date approveTime) {
		this.approveTime = approveTime;
	}

	@Column(name = "stop_applyer")
	public String getStopApplyer() {
		return stopApplyer;
	}

	public void setStopApplyer(String stopApplyer) {
		this.stopApplyer = stopApplyer;
	}

	@Column(name = "stop_applytime")
	public Date getStopApplyTime() {
		return stopApplyTime;
	}

	public void setStopApplyTime(Date stopApplyTime) {
		this.stopApplyTime = stopApplyTime;
	}

	@Column(name = "stop_approver")
	public String getStopApprover() {
		return stopApprover;
	}

	public void setStopApprover(String stopApprover) {
		this.stopApprover = stopApprover;
	}

	@Column(name = "stop_approvetime")
	public Date getStopApproveTime() {
		return stopApproveTime;
	}

	public void setStopApproveTime(Date stopApproveTime) {
		this.stopApproveTime = stopApproveTime;
	}

	@Column(name = "remak")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "activity_script")
	public String getActivityScript() {
		return activityScript;
	}

	public void setActivityScript(String activityScript) {
		this.activityScript = activityScript;
	}

	private TbSpApprove approve;

	@Transient
	public TbSpApprove getApprove() {
		return approve;
	}

	public void setApprove(TbSpApprove approve) {
		this.approve = approve;
	}

	private String approveAuth;
	@Transient
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}

	@Column(name = "MIN_VALUE")
	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	@Column(name = "MAX_VALUE")
	public Integer getMaxTimes() {
		return maxTimes;
	}

	public void setMaxTimes(Integer maxTimes) {
		this.maxTimes = maxTimes;
	}

	@Column(name = "SPECIAL_VALUE")
	public Double getSpecialValue() {
		return specialValue;
	}

	public void setSpecialValue(Double specialValue) {
		this.specialValue = specialValue;
	}

	@Column(name = "SPECIAL_TIMES")
	public Integer getSpecialTimes() {
		return specialTimes;
	}

	public void setSpecialTimes(Integer specialTimes) {
		this.specialTimes = specialTimes;
	}

	@Column(name = "hasRemind")
	public String getHasRemind() {
		return hasRemind;
	}

	public void setHasRemind(String hasRemind) {
		this.hasRemind = hasRemind;
	}

}
