package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class PrpmaxnoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpmaxnoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGroupnoIsNull() {
            addCriterion("GROUPNO is null");
            return (Criteria) this;
        }

        public Criteria andGroupnoIsNotNull() {
            addCriterion("GROUPNO is not null");
            return (Criteria) this;
        }

        public Criteria andGroupnoEqualTo(String value) {
            addCriterion("GROUPNO =", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoNotEqualTo(String value) {
            addCriterion("GROUPNO <>", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoGreaterThan(String value) {
            addCriterion("GROUPNO >", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoGreaterThanOrEqualTo(String value) {
            addCriterion("GROUPNO >=", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoLessThan(String value) {
            addCriterion("GROUPNO <", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoLessThanOrEqualTo(String value) {
            addCriterion("GROUPNO <=", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoLike(String value) {
            addCriterion("GROUPNO like", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoNotLike(String value) {
            addCriterion("GROUPNO not like", value, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoIn(List<String> values) {
            addCriterion("GROUPNO in", values, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoNotIn(List<String> values) {
            addCriterion("GROUPNO not in", values, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoBetween(String value1, String value2) {
            addCriterion("GROUPNO between", value1, value2, "groupno");
            return (Criteria) this;
        }

        public Criteria andGroupnoNotBetween(String value1, String value2) {
            addCriterion("GROUPNO not between", value1, value2, "groupno");
            return (Criteria) this;
        }

        public Criteria andTablenameIsNull() {
            addCriterion("TABLENAME is null");
            return (Criteria) this;
        }

        public Criteria andTablenameIsNotNull() {
            addCriterion("TABLENAME is not null");
            return (Criteria) this;
        }

        public Criteria andTablenameEqualTo(String value) {
            addCriterion("TABLENAME =", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameNotEqualTo(String value) {
            addCriterion("TABLENAME <>", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameGreaterThan(String value) {
            addCriterion("TABLENAME >", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameGreaterThanOrEqualTo(String value) {
            addCriterion("TABLENAME >=", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameLessThan(String value) {
            addCriterion("TABLENAME <", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameLessThanOrEqualTo(String value) {
            addCriterion("TABLENAME <=", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameLike(String value) {
            addCriterion("TABLENAME like", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameNotLike(String value) {
            addCriterion("TABLENAME not like", value, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameIn(List<String> values) {
            addCriterion("TABLENAME in", values, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameNotIn(List<String> values) {
            addCriterion("TABLENAME not in", values, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameBetween(String value1, String value2) {
            addCriterion("TABLENAME between", value1, value2, "tablename");
            return (Criteria) this;
        }

        public Criteria andTablenameNotBetween(String value1, String value2) {
            addCriterion("TABLENAME not between", value1, value2, "tablename");
            return (Criteria) this;
        }

        public Criteria andMaxnoIsNull() {
            addCriterion("MAXNO is null");
            return (Criteria) this;
        }

        public Criteria andMaxnoIsNotNull() {
            addCriterion("MAXNO is not null");
            return (Criteria) this;
        }

        public Criteria andMaxnoEqualTo(String value) {
            addCriterion("MAXNO =", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoNotEqualTo(String value) {
            addCriterion("MAXNO <>", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoGreaterThan(String value) {
            addCriterion("MAXNO >", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoGreaterThanOrEqualTo(String value) {
            addCriterion("MAXNO >=", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoLessThan(String value) {
            addCriterion("MAXNO <", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoLessThanOrEqualTo(String value) {
            addCriterion("MAXNO <=", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoLike(String value) {
            addCriterion("MAXNO like", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoNotLike(String value) {
            addCriterion("MAXNO not like", value, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoIn(List<String> values) {
            addCriterion("MAXNO in", values, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoNotIn(List<String> values) {
            addCriterion("MAXNO not in", values, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoBetween(String value1, String value2) {
            addCriterion("MAXNO between", value1, value2, "maxno");
            return (Criteria) this;
        }

        public Criteria andMaxnoNotBetween(String value1, String value2) {
            addCriterion("MAXNO not between", value1, value2, "maxno");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}