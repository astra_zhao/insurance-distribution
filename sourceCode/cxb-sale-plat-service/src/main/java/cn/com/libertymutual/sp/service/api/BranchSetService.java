package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;

public interface BranchSetService {

	ServiceResult areaList(String branchCode, int pageNumber, int pageSize);

	ServiceResult addArea(String agreementNo,String saleCode,String saleName,String branchCode,String branchName,String[] list,String isShowInvoice,String isShowInsurancePolicy,String contactMobile,String contactEmail);

	ServiceResult removeArea(String branchCode, String areaCode, String saleName, String branchCode2, String[] list,String invoiceStatus,String insurancePolicyStatus,String contactMobile,String contactEmail);

	ServiceResult getAgreementNo(String branchCode);

	ServiceResult notConfiguredArea(String branchCode);

	ServiceResult getSaleName(String agreementCode);

	ServiceResult addNewArea(String branchCode, String branchName, String areaCode, String areaName, String areaEname);
	
	String cutBranchCode(String branchCode);
	
	String getUserCodeBranchCode(String userCode);
}
