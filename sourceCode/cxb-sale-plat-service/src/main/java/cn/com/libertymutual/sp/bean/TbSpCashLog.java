package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_cashlog")
public class TbSpCashLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7537074119891484505L;
	private Integer id;
	private String businessNo;// 提现编号
	private String phoneNo;
	private String userCode;
	private Double score;
	private Double money;
	private String status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	private String currency;
	private Double rate;
	private String payee;// 收款人姓名
	private String idcard;// 身份证
	private String payeeBank;
	private String payeeAccount;// 收款银行账号
	private String bankName;
	private String bankProvence;// 开户行省份
	private String bankCity;
	private String payeePhone;// 收款人手机号
	private String remarks;
	private String branchCode;// 机构代码3399 或两位

	public TbSpCashLog() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TbSpCashLog(String businessNo, String phoneNo, String userCode, Double score, Double money, String status, Date createTime,
			String currency, Double rate, String payee, String idcard, String payeeBank, String payeeAccount, String bankName, String bankProvence,
			String bankCity, String payeePhone, String branchCode, String remarks) {
		super();
		this.businessNo = businessNo;
		this.phoneNo = phoneNo;
		this.userCode = userCode;
		this.score = score;
		this.money = money;
		this.status = status;
		this.createTime = createTime;
		this.currency = currency;
		this.rate = rate;
		this.payee = payee;
		this.idcard = idcard;
		this.payeeBank = payeeBank;
		this.payeeAccount = payeeAccount;
		this.bankName = bankName;
		this.bankProvence = bankProvence;
		this.bankCity = bankCity;
		this.payeePhone = payeePhone;
		this.remarks = remarks;
		this.branchCode = branchCode;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "business_No", length = 32)
	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	@Column(name = "phoneno", length = 20)
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "user_code", length = 32)
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "score")
	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@Column(name = "money")
	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	@Column(name = "status", length = 2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "currency", length = 10)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "rate")
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "payee", length = 100)
	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	@Column(name = "idcard", length = 32)
	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	@Column(name = "payee_bank", length = 100)
	public String getPayeeBank() {
		return payeeBank;
	}

	public void setPayeeBank(String payeeBank) {
		this.payeeBank = payeeBank;
	}

	@Column(name = "payee_account", length = 100)
	public String getPayeeAccount() {
		return payeeAccount;
	}

	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}

	@Column(name = "bank_name", length = 100)
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name = "bank_provence", length = 100)
	public String getBankProvence() {
		return bankProvence;
	}

	public void setBankProvence(String bankProvence) {
		this.bankProvence = bankProvence;
	}

	@Column(name = "bank_city", length = 100)
	public String getBankCity() {
		return bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}

	@Column(name = "payee_phone", length = 32)
	public String getPayeePhone() {
		return payeePhone;
	}

	public void setPayeePhone(String payeePhone) {
		this.payeePhone = payeePhone;
	}

	@Column(name = "remarks", length = 100)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "branch_code")
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

}
