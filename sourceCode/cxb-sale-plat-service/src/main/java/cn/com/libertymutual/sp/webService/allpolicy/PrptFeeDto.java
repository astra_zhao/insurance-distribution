
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptFeeDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptFeeDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount1Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="amount2Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="amountFee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="currency1Fee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency2Fee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeRate1Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="exchangeRate2Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planExchangeRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="premium1Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="premium2Fee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="premiumFee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptFeeDto", propOrder = {
    "amount1Fee",
    "amount2Fee",
    "amountFee",
    "currency1Fee",
    "currency2Fee",
    "currencyFee",
    "exchangeRate1Fee",
    "exchangeRate2Fee",
    "flag",
    "planExchangeRate",
    "premium1Fee",
    "premium2Fee",
    "premiumFee",
    "proposalNo",
    "riskCode"
})
public class PrptFeeDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected double amount1Fee;
    protected double amount2Fee;
    protected double amountFee;
    protected String currency1Fee;
    protected String currency2Fee;
    protected String currencyFee;
    protected double exchangeRate1Fee;
    protected double exchangeRate2Fee;
    protected String flag;
    protected String planExchangeRate;
    protected double premium1Fee;
    protected double premium2Fee;
    protected double premiumFee;
    protected String proposalNo;
    protected String riskCode;

    /**
     * Gets the value of the amount1Fee property.
     * 
     */
    public double getAmount1Fee() {
        return amount1Fee;
    }

    /**
     * Sets the value of the amount1Fee property.
     * 
     */
    public void setAmount1Fee(double value) {
        this.amount1Fee = value;
    }

    /**
     * Gets the value of the amount2Fee property.
     * 
     */
    public double getAmount2Fee() {
        return amount2Fee;
    }

    /**
     * Sets the value of the amount2Fee property.
     * 
     */
    public void setAmount2Fee(double value) {
        this.amount2Fee = value;
    }

    /**
     * Gets the value of the amountFee property.
     * 
     */
    public double getAmountFee() {
        return amountFee;
    }

    /**
     * Sets the value of the amountFee property.
     * 
     */
    public void setAmountFee(double value) {
        this.amountFee = value;
    }

    /**
     * Gets the value of the currency1Fee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency1Fee() {
        return currency1Fee;
    }

    /**
     * Sets the value of the currency1Fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency1Fee(String value) {
        this.currency1Fee = value;
    }

    /**
     * Gets the value of the currency2Fee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency2Fee() {
        return currency2Fee;
    }

    /**
     * Sets the value of the currency2Fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency2Fee(String value) {
        this.currency2Fee = value;
    }

    /**
     * Gets the value of the currencyFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyFee() {
        return currencyFee;
    }

    /**
     * Sets the value of the currencyFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyFee(String value) {
        this.currencyFee = value;
    }

    /**
     * Gets the value of the exchangeRate1Fee property.
     * 
     */
    public double getExchangeRate1Fee() {
        return exchangeRate1Fee;
    }

    /**
     * Sets the value of the exchangeRate1Fee property.
     * 
     */
    public void setExchangeRate1Fee(double value) {
        this.exchangeRate1Fee = value;
    }

    /**
     * Gets the value of the exchangeRate2Fee property.
     * 
     */
    public double getExchangeRate2Fee() {
        return exchangeRate2Fee;
    }

    /**
     * Sets the value of the exchangeRate2Fee property.
     * 
     */
    public void setExchangeRate2Fee(double value) {
        this.exchangeRate2Fee = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the planExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanExchangeRate() {
        return planExchangeRate;
    }

    /**
     * Sets the value of the planExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanExchangeRate(String value) {
        this.planExchangeRate = value;
    }

    /**
     * Gets the value of the premium1Fee property.
     * 
     */
    public double getPremium1Fee() {
        return premium1Fee;
    }

    /**
     * Sets the value of the premium1Fee property.
     * 
     */
    public void setPremium1Fee(double value) {
        this.premium1Fee = value;
    }

    /**
     * Gets the value of the premium2Fee property.
     * 
     */
    public double getPremium2Fee() {
        return premium2Fee;
    }

    /**
     * Sets the value of the premium2Fee property.
     * 
     */
    public void setPremium2Fee(double value) {
        this.premium2Fee = value;
    }

    /**
     * Gets the value of the premiumFee property.
     * 
     */
    public double getPremiumFee() {
        return premiumFee;
    }

    /**
     * Sets the value of the premiumFee property.
     * 
     */
    public void setPremiumFee(double value) {
        this.premiumFee = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

}
