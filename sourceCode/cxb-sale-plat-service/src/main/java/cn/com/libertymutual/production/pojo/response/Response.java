package cn.com.libertymutual.production.pojo.response;


public class Response extends MessageProcessor {

	/** 总页数 */
	private long total;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
}
