package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanWithBLOBs;
@Mapper
public interface PrpdriskplanMapper {
    int countByExample(PrpdriskplanExample example);

    int deleteByExample(PrpdriskplanExample example);

    int deleteByPrimaryKey(PrpdriskplanKey key);

    int insert(PrpdriskplanWithBLOBs record);

    int insertSelective(PrpdriskplanWithBLOBs record);

    List<PrpdriskplanWithBLOBs> selectByExampleWithBLOBs(PrpdriskplanExample example);

    List<Prpdriskplan> selectByExample(PrpdriskplanExample example);

    PrpdriskplanWithBLOBs selectByPrimaryKey(PrpdriskplanKey key);

    int updateByExampleSelective(@Param("record") PrpdriskplanWithBLOBs record, @Param("example") PrpdriskplanExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdriskplanWithBLOBs record, @Param("example") PrpdriskplanExample example);

    int updateByExample(@Param("record") Prpdriskplan record, @Param("example") PrpdriskplanExample example);

    int updateByPrimaryKeySelective(PrpdriskplanWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpdriskplanWithBLOBs record);

    int updateByPrimaryKey(Prpdriskplan record);
    
    @Update(
            "UPDATE PRPDRISKPLAN A "
                    + "   SET A.VALIDSTATUS = '0' "
                    + " WHERE EXISTS (SELECT 1 "
                    + "          FROM PRPDRATION B "
                    + "         WHERE B.PLANCODE = A.PLANCODE "
                    + "           AND B.RISKCODE = A.RISKCODE "
                    + "           AND B.RISKVERSION = A.RISKVERSION "
                    + "           AND B.KINDCODE = #{kindCode} "
                    + "           AND B.KINDVERSION = #{kindVersion}) "
    )
    void inActiveRiskPlan(@Param("kindCode") String kindCode, @Param("kindVersion") String kindVersion);

    @Select(
            "SELECT * "
                    + "  FROM PRPDRISKPLAN A "
                    + " WHERE A.VALIDSTATUS = '1' "
                    + "   AND A.RISKCODE = #{riskcode} "
                    + "   AND A.RISKVERSION = #{riskversion} "
                    + "   AND EXISTS ( "
                    + "        SELECT 1 "
                    + "          FROM PRPDRATION B "
                    + "         WHERE B.PLANCODE = A.PLANCODE "
                    + "           AND B.RISKCODE = A.RISKCODE "
                    + "           AND B.RISKVERSION = A.RISKVERSION "
                    + "           AND B.VALIDSTATUS = '1' "
                    + "           AND B.KINDCODE = #{kindCode} "
                    + "   )"
    )
    List<Prpdriskplan> checkRiskLinkedDeletable(@Param("kindCode") String kindCode,
                                                @Param("riskcode") String riskcode,
                                                @Param("riskversion") String riskversion);

    @Select(
            "SELECT * "
                    + "  FROM PRPDRISKPLAN A "
                    + " WHERE A.VALIDSTATUS = '1' "
                    + "   AND EXISTS (SELECT 1 "
                    + "          FROM PRPDRATION B "
                    + "         WHERE B.PLANCODE = A.PLANCODE "
                    + "           AND B.RISKCODE = A.RISKCODE "
                    + "           AND B.RISKVERSION = A.RISKVERSION "
                    + "           AND B.VALIDSTATUS = '1' "
                    + "           AND B.KINDCODE = #{kindCode} "
                    + "           AND B.KINDVERSION = #{kindVersion} "
                    + "           AND B.ITEMCODE = #{itemcode}) "
    )
    List<Prpdriskplan> checkItemLinkedDeletable(@Param("kindCode") String kindCode,
                                                @Param("kindVersion") String kindVersion,
                                                @Param("itemcode") String itemcode);

    @Select(
            "SELECT DISTINCT A.PLANCODE "
                    + "                    FROM PRPDRISKPLAN A "
                    + "                   WHERE A.VALIDSTATUS = '1' "
                    + "                     AND EXISTS (SELECT 1 "
                    + "                            FROM PRPDRATION B "
                    + "                           WHERE B.KINDCODE = #{kindCode} "
                    + "                             AND B.KINDVERSION = #{kindVersion} "
                    + "                             AND B.PLANCODE = A.PLANCODE "
                    + "                             AND B.RISKCODE = A.RISKCODE "
                    + "                             AND B.RISKVERSION = A.RISKVERSION "
                    + "                             AND B.VALIDSTATUS = '1') "
    )
    List<String> fetchPlancodesKindUsed(@Param("kindCode") String kindCode, @Param("kindVersion") String kindVersion);
}