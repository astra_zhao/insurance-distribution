package cn.com.libertymutual.production.model.nomorcldatasource;

public class Fhexitemkind extends FhexitemkindKey {
    private String flag;

    private String itemkinddesc;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getItemkinddesc() {
        return itemkinddesc;
    }

    public void setItemkinddesc(String itemkinddesc) {
        this.itemkinddesc = itemkinddesc == null ? null : itemkinddesc.trim();
    }
}