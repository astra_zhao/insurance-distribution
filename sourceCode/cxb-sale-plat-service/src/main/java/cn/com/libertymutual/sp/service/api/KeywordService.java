package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;

public interface KeywordService {

	ServiceResult allKeywords(String type, Integer pageNumber, Integer pageSize);

	ServiceResult addOrUpdateKeyword(Integer id, String type, String content);

	ServiceResult saveShopName(String userCode, String shopName, String shopIntroduct);

}
