package cn.com.libertymutual.sp.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpCheckCode;

@Repository
public interface CheckCodeDao extends PagingAndSortingRepository<TbSpCheckCode, Integer>, JpaSpecificationExecutor<TbSpCheckCode> {

@Query("from TbSpCheckCode where status='1' and checkDate= ?1 order by createTime desc")	
public List<TbSpCheckCode> findByCheckDate(String checkDate);

/*@Query("from TbSpCheckCode where status='1' and DATE_ADD(CURDATE(), INTERVAL -1 MONTH) <= STR_TO_DATE(checkDate,'%Y%m%d')  order by createTime desc")	
public List<TbSpCheckCode> findOneMoth();*/
}
