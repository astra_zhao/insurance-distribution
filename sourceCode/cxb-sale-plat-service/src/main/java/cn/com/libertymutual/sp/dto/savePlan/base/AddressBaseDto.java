package cn.com.libertymutual.sp.dto.savePlan.base;

/**
 * 销售平台基础财险地质基础Dto
 */
public class AddressBaseDto {
	
    //地址序号
    private String addressNo ;
    //地址邮编
    private String postCode ;
    //地址编码
    private String addressCode ;
    //地址名称
    private String addressName ;
    //被保险人序号
    private String projectName ;
    //场所性质名称
    private String locationName ;
    //场所性质类型
    private String locationType ;
    //地址名称
    private String addressTitleName ;
    //防火等级
    private String fireLevel ;
    //建筑物结构
    private String buildStructure ;
    //建筑物年份
    private String buildYear ;
    //建筑物性质
    private String buildNature ;
    //建筑物类型
    private String buildType ;
    //占用性质
    private String occupancy ;
    //标志字段
    private String flag ;
    
    private String region;
    
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getAddressNo() {
		return addressNo;
	}
	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getAddressCode() {
		return addressCode;
	}
	public void setAddressCode(String addressCode) {
		this.addressCode = addressCode;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public String getAddressTitleName() {
		return addressTitleName;
	}
	public void setAddressTitleName(String addressTitleName) {
		this.addressTitleName = addressTitleName;
	}
	public String getFireLevel() {
		return fireLevel;
	}
	public void setFireLevel(String fireLevel) {
		this.fireLevel = fireLevel;
	}
	public String getBuildStructure() {
		return buildStructure;
	}
	public void setBuildStructure(String buildStructure) {
		this.buildStructure = buildStructure;
	}
	public String getBuildYear() {
		return buildYear;
	}
	public void setBuildYear(String buildYear) {
		this.buildYear = buildYear;
	}
	public String getBuildNature() {
		return buildNature;
	}
	public void setBuildNature(String buildNature) {
		this.buildNature = buildNature;
	}
	public String getBuildType() {
		return buildType;
	}
	public void setBuildType(String buildType) {
		this.buildType = buildType;
	}
	public String getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(String occupancy) {
		this.occupancy = occupancy;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
}
