package cn.com.libertymutual.sp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_store")
public class TbSpStore implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 6038250670700123879L;
	private Integer id;
	private String userId;
	private String storeName;
	private String introduce;
	private Integer quantity;
	private Integer fans;
	private Integer approve;
	private String isValidate;

	// Constructors

	/** default constructor */
	public TbSpStore() {
	}

	/** full constructor */
	public TbSpStore(String userId, String storeName, String introduce,
			Integer quantity, Integer fans, Integer approve, String isValidate) {
		this.userId = userId;
		this.storeName = storeName;
		this.introduce = introduce;
		this.quantity = quantity;
		this.fans = fans;
		this.approve = approve;
		this.isValidate = isValidate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USER_ID", length = 50)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "STORE_NAME", length = 100)
	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	@Column(name = "INTRODUCE", length = 100)
	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	@Column(name = "QUANTITY")
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "FANS")
	public Integer getFans() {
		return this.fans;
	}

	public void setFans(Integer fans) {
		this.fans = fans;
	}

	@Column(name = "APPROVE")
	public Integer getApprove() {
		return this.approve;
	}

	public void setApprove(Integer approve) {
		this.approve = approve;
	}

	@Column(name = "IS_VALIDATE", length = 1)
	public String getIsValidate() {
		return this.isValidate;
	}

	public void setIsValidate(String isValidate) {
		this.isValidate = isValidate;
	}

}