package cn.com.libertymutual.sp.action.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.pojo.request.PrpdPlanPropsRequest;
import cn.com.libertymutual.production.pojo.request.PrpdPropsRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.PropsBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("props")
public class PropsController {
	
	@Autowired
	protected PropsBusinessService propsBusinessService;
	
	/**
	 * 查询因子基础信息
	 * @param request
	 * @return
	 */
	@RequestMapping("findProps")
	public Response findProps(PrpdPropsRequest request) {
		Response result = new Response();
		result = propsBusinessService.findProps(request);
		return result;
		
	}
	
	/**
	 * 方案管理-获取已配置因子
	 * @param request
	 * @return
	 */
	@RequestMapping("findPlanProps")
	public Response findPlanProps(Prpdriskplan plan) {
		Response result = new Response();
		result = propsBusinessService.findPlanProps(plan);
		return result;
		
	}
	
	/**
	 * 方案管理-配置因子
	 * @param request
	 * @return
	 */
	@RequestMapping("addProps")
	public Response addProps(@RequestBody PrpdPlanPropsRequest request) {
		Response result = new Response();
		try {
			propsBusinessService.insertProps(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
		
	}
}
