package cn.com.libertymutual.sp.dto.savePlan.base;

import java.io.Serializable;
/**
 * 投保单保存公共相应头信息，各险种具体请求头对应应继承此类
 * @author zhangdongkun
 */
public class ProposalBaseRequestDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String sequenceId;// 请求唯一识别码
	private String consumerId;
	private String operateCode;
	private String password;
	private String usercode;
	private String requestType;
	private String comCode;//登陆机构  保留此字段


	
	public String getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}
	public String getOperateCode() {
		return operateCode;
	}
	public void setOperateCode(String operateCode) {
		this.operateCode = operateCode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getComCode() {
		return comCode;
	}
	public void setComCode(String comCode) {
		this.comCode = comCode;
	}
	public String getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
