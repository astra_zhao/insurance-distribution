package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdrationMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdration;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdrationExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdrationExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdrationService;

@Service
public class PrpdrationServiceImpl implements PrpdrationService {

	@Autowired
	private PrpdrationMapper prpdrationMapper;

	@Override
	public Prpdkindlibrary checkKindDeletable(String kindCode,
			String kindVersion) {
		List<Prpdkindlibrary> kinds = prpdrationMapper.checkKindDeletable(
				kindCode, kindVersion);
		if (kinds.isEmpty()) {
			return null;
		}
		return kinds.get(0);
	}

	@Override
	public void insert(Prpdration record) {
		prpdrationMapper.insertSelective(record);
	}

	@Override
	public void deleteByPlan(Prpdration criteria) {
		PrpdrationExample example = new PrpdrationExample();
		Criteria where = example.createCriteria();
		where.andPlancodeEqualTo(criteria.getPlancode());
		where.andRiskcodeEqualTo(criteria.getRiskcode());
		where.andRiskversionEqualTo(criteria.getRiskversion());
		prpdrationMapper.deleteByExample(example);
	}

	@Override
	public List<Prpdration> findByPlanCode(String planCode, Boolean isValid) {
		PrpdrationExample example = new PrpdrationExample();
		Criteria criteria = example.createCriteria();
		criteria.andPlancodeEqualTo(planCode);
		example.setOrderByClause("SERIALNO ASC");
		if (isValid != null) {
			if (isValid){
				criteria.andValidstatusEqualTo("1");
			} else {
				criteria.andValidstatusEqualTo("0");
			}
		}
		return prpdrationMapper.selectByExample(example);
	}

	@Override
	public void update(Prpdration record) {
		PrpdrationExample example = new PrpdrationExample();
		Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmpty(record.getPlancode())) {
			criteria.andPlancodeEqualTo(record.getPlancode());
		}
		if (!StringUtils.isEmpty(record.getRiskcode())) {
			criteria.andRiskcodeEqualTo(record.getRiskcode());
		}
		if (!StringUtils.isEmpty(record.getRiskversion())) {
			criteria.andRiskversionEqualTo(record.getRiskversion());
		}
		if (!StringUtils.isEmpty(record.getKindcode())) {
			criteria.andKindcodeEqualTo(record.getKindcode());
		}
		if (!StringUtils.isEmpty(record.getKindversion())) {
			criteria.andKindversionEqualTo(record.getKindversion());
		}
		if (record.getItemcode() == null) {
			criteria.andItemcodeIsNull();
		} else {
			criteria.andItemcodeEqualTo(record.getItemcode());
		}
		prpdrationMapper.updateByExampleSelective(record, example);
	}

	@Override
	public void updateByCriteria(Prpdration record, Prpdration criteria) {
		PrpdrationExample example = new PrpdrationExample();
		Criteria where = example.createCriteria();
		if (criteria.getPlancode() != null) {
			where.andPlancodeEqualTo(criteria.getPlancode());
		}
		if (criteria.getRiskcode() != null) {
			where.andRiskcodeEqualTo(criteria.getRiskcode());
		}
		if (criteria.getRiskversion() != null) {
			where.andRiskversionEqualTo(criteria.getRiskversion());
		}
		if (criteria.getKindcode() != null) {
			where.andKindcodeEqualTo(criteria.getKindcode());
		}
		if (criteria.getKindversion() != null) {
			where.andKindversionEqualTo(criteria.getKindversion());
		}
		if (criteria.getItemcode() != null) {
			where.andItemcodeEqualTo(criteria.getItemcode());
		}
		prpdrationMapper.updateByExampleSelective(record, example);
	}
	
	@Override
	public int getMaxSerialNum(Prpdration criteria) {
		List<Integer> maxSerialNum = prpdrationMapper.getMaxSerialNum(
				criteria.getPlancode(), criteria.getRiskcode(),
				criteria.getRiskversion());
		return maxSerialNum.get(0);
	}

	@Override
	public void inActiveRation(String kindCode, String kindVersion) {
		prpdrationMapper.inActiveRation(kindCode, kindVersion);
	}


}
