
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptaddressDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptaddressDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="addressNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="buildNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buildStructure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buildType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fireLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupancy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptaddressDto", propOrder = {
    "addressName",
    "addressNo",
    "buildNature",
    "buildStructure",
    "buildType",
    "fireLevel",
    "occupancy",
    "postCode"
})
public class PrptaddressDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String addressName;
    protected int addressNo;
    protected String buildNature;
    protected String buildStructure;
    protected String buildType;
    protected String fireLevel;
    protected String occupancy;
    protected String postCode;

    /**
     * Gets the value of the addressName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressName() {
        return addressName;
    }

    /**
     * Sets the value of the addressName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressName(String value) {
        this.addressName = value;
    }

    /**
     * Gets the value of the addressNo property.
     * 
     */
    public int getAddressNo() {
        return addressNo;
    }

    /**
     * Sets the value of the addressNo property.
     * 
     */
    public void setAddressNo(int value) {
        this.addressNo = value;
    }

    /**
     * Gets the value of the buildNature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildNature() {
        return buildNature;
    }

    /**
     * Sets the value of the buildNature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildNature(String value) {
        this.buildNature = value;
    }

    /**
     * Gets the value of the buildStructure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildStructure() {
        return buildStructure;
    }

    /**
     * Sets the value of the buildStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildStructure(String value) {
        this.buildStructure = value;
    }

    /**
     * Gets the value of the buildType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildType() {
        return buildType;
    }

    /**
     * Sets the value of the buildType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildType(String value) {
        this.buildType = value;
    }

    /**
     * Gets the value of the fireLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFireLevel() {
        return fireLevel;
    }

    /**
     * Sets the value of the fireLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFireLevel(String value) {
        this.fireLevel = value;
    }

    /**
     * Gets the value of the occupancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupancy() {
        return occupancy;
    }

    /**
     * Sets the value of the occupancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupancy(String value) {
        this.occupancy = value;
    }

    /**
     * Gets the value of the postCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the value of the postCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostCode(String value) {
        this.postCode = value;
    }

}
