package cn.com.libertymutual.production.pojo.request;

import java.util.Date;

public class PrpdRiskPlanRequest extends Request {

	private String plancname;

	private String planename;

	private Long serialno;

	private String validstatus;

	private String flag;

	private String plandesc;

	private String plantype;

	private String applycomcode;

	private String applychannel;

	private String autoundwrt;

	private String periodtype;

	private Integer period;

	private String occupationcode;

	private String traveldestination;

	private String riskcode;

	private String riskversion;

	private String plancode;

	private Date createdate;

	public String getPlancname() {
		return plancname;
	}

	public void setPlancname(String plancname) {
		this.plancname = plancname;
	}

	public String getPlanename() {
		return planename;
	}

	public void setPlanename(String planename) {
		this.planename = planename;
	}

	public Long getSerialno() {
		return serialno;
	}

	public void setSerialno(Long serialno) {
		this.serialno = serialno;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getPlandesc() {
		return plandesc;
	}

	public void setPlandesc(String plandesc) {
		this.plandesc = plandesc;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	public String getApplycomcode() {
		return applycomcode;
	}

	public void setApplycomcode(String applycomcode) {
		this.applycomcode = applycomcode;
	}

	public String getApplychannel() {
		return applychannel;
	}

	public void setApplychannel(String applychannel) {
		this.applychannel = applychannel;
	}

	public String getAutoundwrt() {
		return autoundwrt;
	}

	public void setAutoundwrt(String autoundwrt) {
		this.autoundwrt = autoundwrt;
	}

	public String getPeriodtype() {
		return periodtype;
	}

	public void setPeriodtype(String periodtype) {
		this.periodtype = periodtype;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getOccupationcode() {
		return occupationcode;
	}

	public void setOccupationcode(String occupationcode) {
		this.occupationcode = occupationcode;
	}

	public String getTraveldestination() {
		return traveldestination;
	}

	public void setTraveldestination(String traveldestination) {
		this.traveldestination = traveldestination;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

}
