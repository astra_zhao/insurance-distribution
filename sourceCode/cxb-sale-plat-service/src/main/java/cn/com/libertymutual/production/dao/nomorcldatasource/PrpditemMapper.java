package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpditem;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemWithBLOBs;
@Mapper
public interface PrpditemMapper {
    int countByExample(PrpditemExample example);

    int deleteByExample(PrpditemExample example);

    int deleteByPrimaryKey(PrpditemKey key);

    int insert(PrpditemWithBLOBs record);

    int insertSelective(PrpditemWithBLOBs record);

    List<PrpditemWithBLOBs> selectByExampleWithBLOBs(PrpditemExample example);

    List<Prpditem> selectByExample(PrpditemExample example);

    PrpditemWithBLOBs selectByPrimaryKey(PrpditemKey key);

    int updateByExampleSelective(@Param("record") PrpditemWithBLOBs record, @Param("example") PrpditemExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpditemWithBLOBs record, @Param("example") PrpditemExample example);

    int updateByExample(@Param("record") Prpditem record, @Param("example") PrpditemExample example);

    int updateByPrimaryKeySelective(PrpditemWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpditemWithBLOBs record);

    int updateByPrimaryKey(Prpditem record);
}