package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_adconfig")
public class TbSpAdconfig implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -4985784984676286241L;
	private Integer id;
	private String imgUrl;
	private String toUrl;
	private String toType;
	private Integer serialNo;
	private String isShow;
	private String description;
	private String remark;
	private String adType;
	private String detail;
	private String productId;
	// @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date startDate;
	// @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endDate;
	// Constructors

	/** default constructor */
	public TbSpAdconfig() {
	}

	/** full constructor */
	public TbSpAdconfig(String imgUrl, String toUrl, String toType, Integer serialNo, String isShow, String description, String remark,
			String adType) {
		this.imgUrl = imgUrl;
		this.toUrl = toUrl;
		this.toType = toType;
		this.serialNo = serialNo;
		this.isShow = isShow;
		this.description = description;
		this.remark = remark;
		this.adType = adType;
	}

	@Column(name = "START_TIME")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "END_TIME")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "PRODUCT_ID", length = 10)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "IMG_URL", length = 100)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "TO_URL", length = 100)
	public String getToUrl() {
		return this.toUrl;
	}

	public void setToUrl(String toUrl) {
		this.toUrl = toUrl;
	}

	@Column(name = "TO_TYPE", length = 1)
	public String getToType() {
		return this.toType;
	}

	public void setToType(String toType) {
		this.toType = toType;
	}

	@Column(name = "SERIAL_NO")
	public Integer getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "IS_SHOW", length = 1)
	public String getIsShow() {
		return this.isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	@Column(name = "DESCRIPTION", length = 100)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "REMARK", length = 100)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "AD_TYPE", length = 1)
	public String getAdType() {
		return this.adType;
	}

	public void setAdType(String adType) {
		this.adType = adType;
	}

	@Column(name = "Detail")
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}