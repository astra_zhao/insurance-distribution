package cn.com.libertymutual.production.service.api.business;

import cn.com.libertymutual.production.pojo.request.LinkKindRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.*;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class RiskBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdRiskService prpdRiskService;
	@Autowired
	protected PrpdKindService prpdKindService;
	@Autowired
	protected FdriskconfigService fdriskconfigService;
	@Autowired
	protected FhriskService fhriskService;
	@Autowired
	protected FhxriskService fhxriskService;
	@Autowired
	protected FhtreatyService fhtreatyService;
	@Autowired
	protected FhsectionService fhsectionService;
	@Autowired
	protected FhexitemkindService fhexitemkindService;
	@Autowired
	protected FhxtreatyService fhxtreatyService;
	@Autowired
	protected FhxlayerService fhxlayerService;
	@Autowired
	protected FhxsectionService fhxsectionService;

	/**
	 * 校验险种代码是否使用
	 * @param riskcode
	 * @return
	 */
	public abstract Response checkRiskCodeUsed(String riskcode);
	
	/**
	 * 新增险种
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response insert(PrpdRiskRequest request) throws Exception;
	
	/**
	 * 修改险种
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response update(PrpdRiskRequest request) throws Exception;
	
	/**
	 * 险种关联条款
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response linkKind(LinkKindRequest request) throws Exception;

	/**
	 * 查询合约
	 * @return
     */
	public abstract Response queryAllFhtreaty();

	/**
	 * 查询合约节
	 * @param treatyno
	 * @return
     */
	public abstract Response queryFhsections(String treatyno);

	/**
	 * 查询合约除外责任
	 * @param treatyno
	 * @param sectionno
     * @return
     */
	public abstract Response queryFhexitemkinds(String treatyno, String sectionno);

	/**
	 * 查询超赔合约
	 * @return
     */
	public abstract Response queryAllFhxtreaty();

	/**
	 * 查询超赔合约层
	 * @param treatyno
	 * @return
     */
	public abstract Response queryFhxlayers(String treatyno);

	/**
	 * 查询超赔合约层
	 * @param treatyno
	 * @param layerno
     * @return
     */
	public abstract Response queryFhxsections(String treatyno, String layerno);

	/**
	 * 查询再保方式
	 * @param riskcode 险种代码
	 * @return
     */
	public abstract Response queryFdriskconfigByRisk(String riskcode);

	/**
	 * 查询合约配置
	 * @param riskcode 险种代码
	 * @return
     */
	public abstract Response queryFhriskByRisk(String riskcode);

	/**
	 * 查询除外合约条件配置
	 * @param riskcode 险种代码
	 * @return
     */
	public abstract Response queryFhexitemkindByRisk(String riskcode);

	/**
	 * 查询超赔合约配置
	 * @param riskcode 险种代码
	 * @return
     */
	public abstract Response queryFhxriskByRisk(String riskcode);

	/**
	 * 查询险种
	 * @param riskcodes
	 * @return
     */
	public abstract Response queryRiskByCodes(List<String> riskcodes);
}