package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpShare;

public interface ShareDao extends PagingAndSortingRepository<TbSpShare, Integer>, JpaSpecificationExecutor<TbSpShare>{
	List<TbSpShare> findByShareId(String shareId);
	
	
	
	
	@Query("from TbSpShare o  where  o.shareId=?1 and  productId=?2")
	List<TbSpShare> findTbSpShare(String shareId,String productId);
	
	@Query("from TbSpShare o  where  o.shareId=?1 and  productId=?2 ")
	List<TbSpShare> findProductShare(String shareId,String productId);
}
