package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdcoderiskKey {
    private String codetype;

    private String codecode;

    private String riskcode;

    public String getCodetype() {
        return codetype;
    }

    public void setCodetype(String codetype) {
        this.codetype = codetype == null ? null : codetype.trim();
    }

    public String getCodecode() {
        return codecode;
    }

    public void setCodecode(String codecode) {
        this.codecode = codecode == null ? null : codecode.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }
}