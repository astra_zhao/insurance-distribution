import {
    Mutations,
    policyHolderType,
    Gender,
    PerIdentifyType,
    EntIdentifyType,
    benefType,
    insuredidentity
} from 'src/common/const';
import DateUtil from "src/common/util/dateUtil";
import {
    RequestUrl
} from 'src/common/url';
export default {
    state: {
        seleNsPlan: {},   //当前选择的 女神那边的计划
        seleSalePlan: "", //当前选择的这边的计划


        curPlansPrice: '',

        curSelePlan: {},   //当前选择的计划-我们这边
        curNsSelePlan: {}, //当前选择的计划  女神那边的

        productData: {},

        isGetCountry: false,
        seleCountry: [], //当前可以选择的国家

        isQuery: false,
        ageRelated: false,
        selePlanPrice: 0.0,
        planId: "",


        allData: [],
        schengenData: [],
        countryList: [],
        fristAll: [],

        isPlanQuery: false,
        planQueryStyle: "background:#C8161D;!important"
    },
    mutations: {
        //planParams包含this,和请求参数
        ["DETAILS_TEST"](state, planParams) {
            let type = planParams.type;
            let productId = planParams.productId;
            let planId = planParams.planId;
            let age = planParams.age;
            let deadline = planParams.deadline;
            let _this = planParams._this;
            let index = planParams.index;

            // 初始化 产品 
            if (type == "product") {
                //不存在 则创建最外层的结构 用于保存产品数据
                if (typeof (state.productData[productId]) == 'undefined') { //以id为单位
                    let productQuery = {
                        TYPE: 'FORM',
                        productId: productId,
                        userCode: planParams.userCode
                    }
                    state.productData[productId] = {
                        selePlanId: "",
                        startTime: "",
                        endTime: "",
                        birthday: "",
                        spPlans: {},
                        minAge: "",
                        maxDeadLine: ""

                    }
                    _this.$http.post(RequestUrl.QUERY_SALE_PLAN, productQuery)
                        .then(function (res) {
                            state.isQuery = true;
                            // $.ajax({
                            //     type: "POST",
                            //     url: RequestUrl.QUERY_SALE_PLAN,
                            //     data: productQuery,
                            //     async: false,
                            //     success: function (res, status, xhr) {

                            if (res.success == true) {
                                state.seleSalePlan = res.result[0];
                                _this.age = res.result[0].minAge;
                                state.productData[productId].salePlans = res.result;
                                state.productData[productId].spPlans = res.result[0];
                                state.curSelePlan = res.result[0];
                                _this.spPlans = res.result;
                                _this.selePlanId = res.result[0].planId;
                                state.productData[productId].selePlanId = res.result[0].planId;
                                // state.productData[productId].minAge = res.result[0].minAge;
                                // state.productData[productId].maxDeadLine = res.result[0].maxDeadLine;
                                // _this.endEndData = DateUtil.getDateAfterOfDay(_this.PeriodDto.startDate, _this.detailsStore.seleSalePlan.maxDeadLine - 1);
                                for (let i = 0; i < state.productData[productId].salePlans.length; i++) {
                                    let isYear = false;
                                    if (state.productData[productId].salePlans[i].maxDeadLine == "365" && state.productData[productId].salePlans[i].minDeadLine == "365") {
                                        isYear = true;
                                    } else {
                                        isYear = false;
                                    }

                                    if (state.productData[productId].salePlans[i].ageRelated == 1) {
                                        state.productData[productId][res.result[i].planId] = {
                                            ageRelated: true,
                                            isYear: isYear,
                                            salePlan: state.productData[productId].salePlans[i],
                                            isImmediate: false,
                                        };
                                    } else {
                                        state.productData[productId][res.result[i].planId] = {
                                            ageRelated: false,
                                            isYear: isYear,
                                            salePlan: state.productData[productId].salePlans[i],
                                            isImmediate: false,
                                        };
                                    }
                                    if (state.productData[productId].salePlans[i].isImmediate == "1" || state.productData[productId].salePlans[i].isImmediate == 1) {
                                        state.productData[productId][res.result[i].planId].isImmediate = true;
                                    }
                                }
                                state.ageRelated = state.productData[productId][_this.selePlanId].ageRelated;
                                _this.isYear = state.productData[productId][_this.selePlanId].isYear;
                                _this.isShowBirthTime = state.productData[productId][_this.selePlanId].ageRelated;
                                _this.isImm = state.productData[productId][_this.selePlanId].isImmediate;
                                let de = res.result[0].maxDeadLine - res.result[0].minDeadLine;
                                if (de < 2) {
                                    let data = DateUtil.getDateAfter();
                                    _this.PeriodDto.endDate = DateUtil.getDateInsure(data);

                                } else {
                                    _this.PeriodDto.endDate = DateUtil.getDateAfterOfDay(_this.PeriodDto.startDate, res.result[0].minDeadLine - 1);

                                }

                                _this.changeStartEndTime();

                            }
                            // else {
                            //     _this.$vux.alert.show({
                            //         title: "提示",
                            //         content: res.result
                            //     });
                            // }
                            // }
                        });
                } else {
                    //seleSalePlan//

                    _this.spPlans = state.productData[productId].salePlans;
                    _this.selePlanId = state.productData[productId].selePlanId;
                    _this.isShowBirthTime = state.productData[productId][_this.selePlanId].ageRelated;
                    _this.isImm = state.productData[productId][_this.selePlanId].isImmediate;
                    _this.isYear = state.productData[productId][_this.selePlanId].isYear;
                    state.ageRelated = state.productData[productId][_this.selePlanId].ageRelated;
                    // state.seleSalePlan = state.productData[productId][_this.selePlanId].seleSalePlan;
                    for (let i = 0; i < _this.spPlans.length; i++) {
                        if (_this.spPlans[i].planId == _this.selePlanId) {
                            state.productData[productId].spPlans = _this.spPlans[i];
                            state.curSelePlan = _this.spPlans[i];
                        }
                    }
                    // let isImm = state.productData[productId].isImm;
                    // let startHour = state.productData[productId].startHour;
                    let startTime = state.productData[productId].startTime;
                    let endTime = state.productData[productId].endTime;
                    let birthday = state.productData[productId].birthday;
                    setTimeout(function () {
                        // debugger
                        // if (isImm) {
                        //     _this.immData = startHour;
                        // }
                        _this.PeriodDto.startDate = startTime;
                        _this.PeriodDto.endDate = endTime;
                        _this.birthTime = birthday;
                        // _this.PeriodDto.startDate = state.productData[productId].startTime;
                        // _this.PeriodDto.endDate = state.productData[productId].endTime;
                        // _this.birthTime = state.productData[productId].birthday;
                    }, 100);
                }
            }

            // 初始化 计划 
            if ((type == "plan" || type == "price" || type == "add") && planId != "") {
                // debugger
                if (state.productData[productId][planId].isImmediate) {
                    deadline = deadline - 1;
                    if (deadline < 1) {
                        deadline = state.productData[productId][planId].salePlan.minDeadLine
                    }
                }
                // console.log('deadline------:' + deadline);
                //不存在 则创建最外层的结构 用于保存产品数据
                if (typeof (state.productData[productId][planId][age + deadline]) == 'undefined') {
                    if (type == "plan") {
                        state.productData[productId].selePlanId = _this.selePlanId;
                        if (deadline == "") {
                            for (let i = 0; i < state.productData[productId].salePlans.length; i++) {
                                if (state.productData[productId].selePlanId == state.productData[productId].salePlans[i].planId) {
                                    deadline = state.productData[productId].salePlans[i].maxDeadLine;
                                }
                            }
                        } else {
                            state.productData[productId].startTime = _this.PeriodDto.startDate;
                            state.productData[productId].endTime = _this.PeriodDto.endDate;
                            state.productData[productId].birthday = _this.birthTime;
                        }
                    }

                    let planQuery = {
                        TYPE: 'FORM',
                        planId: planId,
                        age: age,
                        deadLine: deadline
                    }
                    state.isQuery = false;
                    state.isPlanQuery = true;
                    state.planQueryStyle = "background:#636365;!important";
                    _this.$http.post(RequestUrl.QUERY_PLAN, planQuery)
                        .then(function (res) {
                            state.isQuery = true;
                            if (res.success == true) {
                                state.isPlanQuery = false;
                                state.planQueryStyle = "background:#C8161D;!important";

                                state.productData[productId][planId][age + deadline] = res.result[0];
                                if (type == "plan") {
                                    _this.isYear = state.productData[productId][_this.selePlanId].isYear;
                                    _this.selePlanPrice = state.productData[productId][planId][age + deadline].planPrice;
                                    _this.selePlanList = state.productData[productId][planId][age + deadline];
                                    _this.isShowBirthTime = state.productData[productId][planId].ageRelated;
                                    _this.isImm = state.productData[productId][planId].isImmediate;
                                    state.ageRelated = state.productData[productId][planId].ageRelated;
                                    state.curNsSelePlan = res.result[0];
                                    sessionStorage["CUR_SELE_MIN_AGE"] = state.curNsSelePlan.minAge;
                                    state.planId = res.result[0].planId;
                                } else {
                                    // 请求价格
                                    if (res.result.length > 0) {
                                        if (type == "add") {
                                            _this.insurePriceList.push(res.result[0].planPrice);
                                        } else {
                                            _this.insurePriceList[index] = res.result[0].planPrice;
                                        }
                                        _this.getInsurePriceListPrice();
                                    }
                                }
                            }


                        });
                } else {
                    if (type == "plan") {
                        state.planId = state.productData[productId][planId][age + deadline].planId;
                        state.curNsSelePlan = state.productData[productId][planId][age + deadline];
                        sessionStorage["CUR_SELE_MIN_AGE"] = state.curNsSelePlan.minAge;
                        _this.isYear = state.productData[productId][planId].isYear;
                        _this.selePlanPrice = state.productData[productId][planId][age + deadline].planPrice;
                        _this.selePlanList = state.productData[productId][planId][age + deadline];
                        state.ageRelated = state.productData[productId][planId].ageRelated;
                        _this.isShowBirthTime = state.productData[productId][planId].ageRelated;
                        _this.isImm = state.productData[productId][planId].isImmediate;
                    } else {
                        //请求价格
                        if (type == "add") {
                            _this.insurePriceList.push(state.productData[productId][planId][age + deadline].planPrice);
                        } else {
                            _this.insurePriceList[index] = state.productData[productId][planId][age + deadline].planPrice;
                        }
                        _this.getInsurePriceListPrice();
                    }
                }

                if (type == "plan" || type == "product") {
                    for (let i = 0; i < _this.spPlans.length; i++) {
                        if (_this.spPlans[i].planId == _this.selePlanId) {
                            state.productData[productId].spPlans = _this.spPlans[i];
                            state.curSelePlan = _this.spPlans[i];
                        }
                    }
                }
            }




        },
        ["DETAILSS_SAVE"](state, parm) {
            let _this = parm._this;
            // state.productData[parm.productId].isImm = parm.isImm;
            // state.productData[parm.productId].startHour = parm.startHour;

            state.productData[parm.productId].selePlanId = parm.seleId;
            state.productData[parm.productId].startTime = parm.startTime;
            state.productData[parm.productId].endTime = parm.endTime;
            state.productData[parm.productId].birthday = parm.birthday;
            // for (let i = 0; i < state.spPlans.length; i++) {
            //     if (state.spPlans[i].planId == parm.seleId) {
            //         state.curSelePlan = state.spPlans[i];
            //     }
            // }
            // for (let i = 0; i < state.planLists.length; i++) {
            //     if (state.planLists[i].planId == parm.seleId) {
            //         state.curNsSelePlan = state.planLists[i];
            //         state.curPlansPrice = state.curNsSelePlan.planPrice;
            //     }
            // }
        },
        ["DETAILSS_SAVE_TEST1"](state, parm) {
            let _this = parm._this;
            if (parm.isAbroadTravel && typeof (state.countryList[parm.planId]) == "undefined") {
                // state.isGetCountry = true;
                // let codeType = {
                //     TYPE: 'FORM',
                //     codeType: 'CountryCode'
                // }
                // _this.$http.post(RequestUrl.GET_CODE_TYPE, codeType)
                //     .then(function (res) {
                //         state.seleCountry = res.result;
                //     })
                let query = {
                    TYPE: "FORM",
                    planId: parm.planId
                }
                _this.$http.post(RequestUrl.GET_SELE_COUNTRY, query)
                    .then(function (res) {
                        let all = [];
                        //所有国家
                        let allData = [];
                        let schengenData = [];
                        let pinYinAll = "";
                        let pinYinSchengen = "";
                        for (let i = 0; i < res.result.length; i++) {

                            let add = res.result[i].codeCname1 + "," + res.result[i].codeEname1;
                            allData.push({
                                name: res.result[i].codeCname1,
                                value: add
                            });
                            if (pinYinAll != res.result[i].codeType2) {
                                pinYinAll = res.result[i].codeType2;
                                state.fristAll[pinYinAll] = i;
                            }
                            if (res.result[i].code1 == "Schengen-States") {
                                if (pinYinSchengen != res.result[i].codeType2) {
                                    pinYinSchengen = res.result[i].codeType2;
                                    _this.fristSchengen[pinYinSchengen] = add;
                                }
                                schengenData.push({
                                    name: res.result[i].codeCname1,
                                    value: add
                                });
                            }

                        }
                        _this.all.push(allData);
                        _this.schengen.push(schengenData);
                        _this.countryList = _this.all;
                        // _this.setClick();
                        _this.fristAll = state.fristAll;
                        state.allData = _this.all;
                        state.schengenData = _this.schengen;
                        state.countryList[parm.planId] = _this.countryList;
                        setTimeout(function () {

                        }, 0);
                    })
            } else if (parm.isAbroadTravel) {
                _this.all = state.allData;
                _this.fristAll = state.fristAll;
                _this.schengen = state.schengenData;
                _this.countryList = state.countryList[parm.planId];
                // _this.setClick();
            }
        },
        // [Mutations.DETAILSS](state, parm) {
        //     let _this = parm._this;

        //     state.productData[parm.productId].seleId = parm.seleId;

        //     if (!state.isGetCountry && parm.isAbroadTravel) {
        //         state.isGetCountry = true;
        //         let codeType = {
        //             TYPE: 'FORM',
        //             codeType: 'CountryCode'
        //         }
        //         _this.$http.post(RequestUrl.GET_CODE_TYPE, codeType)
        //             .then(function (res) {
        //                 state.seleCountry = res.result;
        //             })
        //     }
        // }
    },

    actions: {

    },
    getters: {

    }
}
