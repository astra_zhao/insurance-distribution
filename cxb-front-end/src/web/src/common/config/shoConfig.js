
export default {
    /**
     * 获得软文类型-全部
     */
    getSoftPaperTypeAll() {
        return SOFT_PAPER_TYPE_LIST;
    },
    /**
     * 获得软文类型
     * @param {*} index 
     */
    getSoftPaperType(index) {
        return SOFT_PAPER_TYPE_LIST[index];
    },
    /**
     * 获得海报类型-全部
     */
    getPosterTypeAll() {
        return POSTER_TYPE_LIST;
    },
    /**
     * 获得海报类型
     * @param {*} index 
     */
    getPosterType(index) {
        return POSTER_TYPE_LIST[index];
    },
}
//软文类型,1.旅行常识 2.突发事件 3.保险常识 4.常见风险
const SOFT_PAPER_TYPE_LIST = [
    { key: "all", value: "全部" },
    { key: "1", value: "旅行常识" },
    { key: "2", value: "突发事件" },
    { key: "3", value: "保险常识" },
    { key: "4", value: "常见风险" }
];
// 海报类别,1.旅行常识 2.突发事件 3.保险常识 4.常见风险
const POSTER_TYPE_LIST = [
    { key: "all", value: "全部" },
    { key: "1", value: "店铺海报" },
    { key: "2", value: "产品海报" },
    { key: "3", value: "邀请海报" },
    // { key: "3", value: "保险常识" },
    // { key: "4", value: "常见风险" }
];
