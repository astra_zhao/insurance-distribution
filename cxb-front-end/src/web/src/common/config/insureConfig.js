import {
  policyHolderType
} from 'src/common/const';
export default {
  /*
      个人-投保人与被保险人关系
  */
  getRel(type) {
    return RELATIONSHIP['' + type + ''];
  },
  getRelTypeName(type) {
    return REL['' + type + ''];
  },
  /*
      房屋类型
  */
  getHouseType() {
    return BUILD_STRUCTURE;
  },
  getAxtxCar() {
    return AXTX_CAR;
  },
  /*
      房屋类型value
  */
  getHouseTypeValue(key) {
    for (let i = 0; i < BUILD_STRUCTURE.length; i++) {
      if (key == BUILD_STRUCTURE[i].key) {
        return BUILD_STRUCTURE[i].value;
      }
    }
  },
  // /*
  //     录入元素
  // */
  // getInsureElement(type) {
  //   return INSURE_ELEMENT_SHOW['' + type + ''];
  // },
  /*
      地区加权
  */
  getValidationParam() {
    return ValidationParam;
  },
  /*
      选择地区
  */
  getReginSele() {
    return REGION_SELE;
  },


}
const AXTX_CAR = [
  { key: "浙", value: "浙" },
  { key: "闽", value: "闽" },
  { key: "粤", value: "粤" },
  { key: "京", value: "京" },
  { key: "津", value: "津" },
  { key: "冀", value: "冀" },
  { key: "晋", value: "晋" },
  { key: "蒙", value: "蒙" },
  { key: "辽", value: "辽" },
  { key: "吉", value: "吉" },
  { key: "黑", value: "黑" },
  { key: "沪", value: "沪" },
  { key: "苏", value: "苏" },
  { key: "皖", value: "皖" },
  { key: "赣", value: "赣" },
  { key: "鲁", value: "鲁" },
  { key: "豫", value: "豫" },
  { key: "鄂", value: "鄂" },
  { key: "湘", value: "湘" },
  { key: "桂", value: "桂" },
  { key: "琼", value: "琼" },
  { key: "渝", value: "渝" },
  { key: "川", value: "川" },
  { key: "贵", value: "贵" },
  { key: "云", value: "云" },
  { key: "藏", value: "藏" },
  { key: "陕", value: "陕" },
  { key: "甘", value: "甘" },
  { key: "青", value: "青" },
  { key: "宁", value: "宁" },
  { key: "新", value: "新" }
];


// 证件类型
const IDENTIFY_TYPE = {
  //个人
  [policyHolderType.PERSONAL]: [{
    key: '01',
    value: '身份证'
  },
  {
    key: '03',
    value: '护照'
  },
  {
    key: '04',
    value: '军官证'
  },
  {
    key: '99',
    value: '其他'
  },
  ],
  //企业
  [policyHolderType.ENTERPRISE]: [{
    key: '99',
    value: '其他'
  },]
};

const REL = {
  "01": "本人",
  "50": "父母",
  "10": "配偶",
  "40": "子女",
  "99": "其他",
}


// 投保人与被保险人关系
const RELATIONSHIP = {
  //个人
  [policyHolderType.PERSONAL]: [
    // {
    //   key: '01',
    //   value: '本人'
    // },
    {
      key: '50',
      value: '父母'
    },
    {
      key: '10',
      value: '配偶'
    },
    {
      key: '40',
      value: '子女'
    },
    {
      key: '99',
      value: '其他'
    },
  ],
  //企业
  [policyHolderType.ENTERPRISE]: [
    // {
    //   key: '11',
    //   value: '丈夫'
    // },
    // {
    //   key: '12',
    //   value: '妻子'
    // },
    // {
    //   key: '20',
    //   value: '儿子'
    // },
    // {
    //   key: '30',
    //   value: '女儿'
    // },
    {
      key: '99',
      value: '其他'
    },
    // {
    //   key: '51',
    //   value: '父亲'
    // },
    // {
    //   key: '52',
    //   value: '母亲'
    // },
    // {
    //   key: '60',
    //   value: '雇佣'
    // },
    // {
    //   key: '61',
    //   value: '代理'
    // },
  ]
};





// 房屋类型
const BUILD_STRUCTURE = [{
  key: '0001',
  value: '钢混'
},
// {
//   key: '0002',
//   value: '钢筋混凝土'
// },
{
  key: '0003',
  value: '砖混'
},
  // {
  //   key: '0004',
  //   value: '砖木'
  // },
  // {
  //   key: '0005',
  //   value: '其他'
  // },
];



//页面录单其他信息显示元素配置
// const INSURE_ELEMENT_SHOW = {
//   property: {
//     moreInsured: false, //是否多个被保险人
//     isShowOther: true,  //补充信息是否录入
//     occupation: false,  //职业类别
//     locationHouse: true,//房屋所在地
//     address: true,      //详细地址
//     houseType: true,    //房屋类型
//     destination: false, //目的地
//     moreInsured: false, //是否多个被保险人
//     abroadDestination: false       //境外旅行国家选择

//   },
//   travel: { //旅行险
//     isShowOther: true,
//     occupation: false,
//     locationHouse: false,
//     address: false,
//     houseType: false,
//     destination: true,
//     moreInsured: true, //是否多个被保险人
//     abroadDestination: false
//   },
//   travel_Abroad: {     //旅行险
//     isShowOther: true,
//     occupation: false,
//     locationHouse: false,
//     address: false,
//     houseType: false,
//     destination: false,
//     moreInsured: true, //是否多个被保险人
//     abroadDestination: true
//   },
//   accident: {
//     isShowOther: false,
//     occupation: false,
//     locationHouse: false,
//     address: false,
//     houseType: false,
//     destination: false,
//     moreInsured: true,
//     abroadDestination: false
//   }
// }
// const BENEF_TYPE = [{
//   key: '1',
//   value: '法定受益'
// },
// {
//   key: '1',
//   value: '指定顺序受益'
// },
// {
//   key: '2',
//   value: '指定份额受益'
// },

// ];


const REGION_SELE = [{
  key: '北京',
  value: '北京'
},
{
  key: '天津',
  value: '天津'
},
{
  key: '河北',
  value: '河北'
},
{
  key: '山西',
  value: '山西'
},
{
  key: '内蒙古',
  value: '内蒙古'
},
{
  key: '辽宁',
  value: '辽宁'
},
{
  key: '吉林',
  value: '吉林'
},
{
  key: '黑龙江',
  value: '黑龙江'
},
{
  key: '上海',
  value: '上海'
},
{
  key: '江苏',
  value: '江苏'
},
{
  key: '浙江',
  value: '浙江'
},
{
  key: '安徽',
  value: '安徽'
},
{
  key: '福建',
  value: '福建'
},
{
  key: '江西',
  value: '江西'
},
{
  key: '山东',
  value: '山东'
},
{
  key: '河南',
  value: '河南'
},
{
  key: '湖北',
  value: '湖北'
},
{
  key: '湖南',
  value: '湖南'
},
{
  key: '广东',
  value: '广东'
},
{
  key: '广西',
  value: '广西'
},
{
  key: '海南',
  value: '海南'
},
{
  key: '重庆',
  value: '重庆'
},
{
  key: '四川',
  value: '四川'
},
{
  key: '贵州',
  value: '贵州'
},
{
  key: '云南',
  value: '云南'
},
{
  key: '西藏',
  value: '西藏'
},
{
  key: '陕西',
  value: '陕西'
},
{
  key: '甘肃',
  value: '甘肃'
},
{
  key: '青海',
  value: '青海'
},
{
  key: '宁夏',
  value: '宁夏'
},
{
  key: '新疆',
  value: '新疆'
},
{
  key: '台湾',
  value: '台湾'
},
{
  key: '香港',
  value: '香港'
},
{
  key: '澳门',
  value: '澳门'
},
{
  key: '国外',
  value: '国外'
}
];

export const ValidationParam = {
  CHINA_ID_MAX_LENGTH: 18,
  /** 每位加权因子 */
  POWER: [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2],
  CITYCODES: {
    11: "北京",
    12: "天津",
    13: "河北",
    14: "山西",
    15: "内蒙古",
    21: "辽宁",
    22: "吉林",
    23: "黑龙江",
    31: "上海",
    32: "江苏",
    33: "浙江",
    34: "安徽",
    35: "福建",
    36: "江西",
    37: "山东",
    41: "河南",
    42: "湖北",
    43: "湖南",
    44: "广东",
    45: "广西",
    46: "海南",
    50: "重庆",
    51: "四川",
    52: "贵州",
    53: "云南",
    54: "西藏",
    61: "陕西",
    62: "甘肃",
    63: "青海",
    64: "宁夏",
    65: "新疆",
    71: "台湾",
    81: "香港",
    82: "澳门",
    91: "国外"
  }
}
