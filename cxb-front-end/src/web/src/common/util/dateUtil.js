export default {
  //----------------------------------------------------------------------------当前时间
  //当前时间转 yyyyMMdd
  getNowDateStrYmd() {
    let dateObj = this.currentDate(new Date());//获取时间的年月日对象
    let yyyyMMdd = dateObj.year + "" + dateObj.month + "" + dateObj.day;
    return yyyyMMdd;
  },
  //当前时间转 yyyyMMddhhmmss
  getNowDateStrYmdhms() {
    let dateObj = this.currentDate(new Date());//获取时间的年月日对象
    let yyyyMMdd = dateObj.year + "" + dateObj.month + "" + dateObj.day + "" + dateObj.hour + "" + dateObj.minute + "" + dateObj.second;
    return yyyyMMdd;
  },
  //当前日期转 yyyy_MM_dd
  getNowDateStr_Ymd() {
    let dateObj = this.currentDate(new Date());//获取时间的年月日对象
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
  //当前日期转 yyyy_MM_dd HH:mm
  getNowDateStr_Ymdhm() {
    let dateObj = this.currentDate(new Date());//获取时间的年月日对象
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day + " " + dateObj.hour + ":" + dateObj.minute;
    return yyyy_MM_dd;
  },
  //当前时间转 时分秒hh:mm:ss
  getNowDateStr_Hms() {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(new Date());
    let hh_mm_ss = dateObj.hour + ":" + dateObj.minute + ":" + dateObj.second;
    return hh_mm_ss;
  },
  //当前时间 的年月日数字
  getNumNowYMD(type) {
    let dateObj = this.currentDate(new Date());
    if (type == "Y") {
      return dateObj.year;
    } else if (type == "M") {
      return dateObj.month;
    } else {
      return dateObj.day;
    }
  },

  //----------------------------------------------------------------------------时间戳

  //时间戳 转yyyyMMdd
  getDateStrYmdByTs(val) {
    //获取时间的年月日对象
    let dateObj = this.currentDate(new Date(val));
    let yyyyMMdd = dateObj.year + "" + dateObj.month + "" + dateObj.day;
    return yyyyMMdd;
  },
  //时间戳 转yyyy-MM-dd
  getDateStr_YmdByTs(val) {
    //获取时间的年月日对象
    let dateObj = this.currentDate(new Date(val));
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
  //时间戳 转yyyyMMdd hh:mm
  getDateStr_YmdhmByTs(val) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(val);
    let yyyy_MM_dd_hh_mm = dateObj.year + '.' + dateObj.month + '.' + dateObj.day + ' ' + dateObj.hour + ':' + dateObj.minute;
    return yyyy_MM_dd_hh_mm;
  },
  //时间戳 转yyyyMMdd hh:mm:ss
  getDateStr_YmdhmsByTs(val) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(new Date(val));
    let yyyy_MM_dd_hh_mm_ss = dateObj.year + '-' + dateObj.month + '-' + dateObj.day + ' ' + dateObj.hour + ':' + dateObj.minute + ':' + dateObj.second;
    return yyyy_MM_dd_hh_mm_ss;
  },

  //2018-03-05T02:17:40.000+0000 截取后转yyyyMMdd hh:mm:ss
  getFullStrDateByTs2(val) {
    if (typeof (val) != "undefined" && val != null) {
      let year = val.substring(0, 4);
      let month = val.substring(5, 7);
      let day = val.substring(8, 10);
      let hour = parseInt(val.substring(11, 13));
      if (hour > 0) {
        hour = (hour + 8) % 24;//北京时间 = 格林时间+8小时
      }
      if (hour < 10) {
        hour = "0" + hour;//不足2位补零
      }
      let minute = val.substring(14, 16);
      let second = val.substring(17, 19);
      let yyyy_MM_dd_hh_mm_ss = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
      return yyyy_MM_dd_hh_mm_ss;
    }
    return val;
  },
  //yyyy_MM_dd或者yyyy_MM_dd hh:mm:ss 字符串转日期对象
  getDateByStrYmdOrYmdhms(remindTime) {
    let str = remindTime + "";
    str = str.replace("-", "/").replace("-", "/").replace("-", "/");
    // str = str.replace(/-/g, "/");
    let oDate1 = new Date(str);
    return oDate1;
  },
  //yyyy_MM_dd 比较两个字符串日期的大小
  getComStData(stTime, endTime) {
    stTime = stTime.replace("-", "/").replace("-", "/").replace("-", "/");
    endTime = endTime.replace("-", "/").replace("-", "/").replace("-", "/");
    let oDate1 = new Date(stTime);
    let oDate2 = new Date(endTime);
    if (oDate1 > oDate2) {
      return true;
    } else {
      return false;
    }
  },
  //时间戳 获得时间月份
  getValMonth(val) {
    //获取时间的年月日对象
    let addMonth = 0;

    let dateObj = this.currentDate(new Date(val));

    let curDate = this.currentDate(new Date());
    let dataDay = dateObj.day * 1;
    let curDay = curDate.day * 1;
    if (dataDay < curDay) {
      addMonth++;
    }
    let month = 0;
    if (dateObj.year == curDate.year) {
      let month = dateObj.month * 1;
      month = month + addMonth;
      return month;
    } else {
      let curMonth = curDate.month * 1;
      let objMonth = dateObj.month * 1;

      month = (curDate.year - dateObj.year) * 12 - objMonth + curMonth;
      return month + addMonth;
    }
  },
  //减去当前时间一周
  getSubWeek(val) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(val);
    let day = dateObj.day - 7;
    let month = dateObj.month;
    let year = dateObj.year;
    if (day < 1) {
      if (month == 1) {
        month == 12;
        year--;
      } else {
        month--;
      }
      let newDay = this.getMonthDay(year, month);
      day = newDay - 7;
    }
    let yyyy_MM_dd_hh_mm_ss = year + '-' + month + '-' + day + ' ' + dateObj.hour + ':' + dateObj.minute + ':' + dateObj.second;
    return yyyy_MM_dd_hh_mm_ss;
  },
  //减去当前时间的一个月
  getSubMonth(val) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(val);
    if (dateObj.month == 1) {
      dateObj.month = 12;
      dateObj.year--;
    } else {
      dateObj.month--;
    }
    let yyyy_MM_dd_hh_mm_ss = dateObj.year + '-' + dateObj.month + '-' + dateObj.day + ' ' + dateObj.hour + ':' + dateObj.minute + ':' + dateObj.second;
    return yyyy_MM_dd_hh_mm_ss;
  },
  getReqYear(val) {
    let yearText = "";
    let year = this.getDataType(val, 'y');
    let curDateYear = new Date().getFullYear();
    while (1) {
      yearText += year + ",";
      if (curDateYear == year) {
        return yearText;
      } else {
        year++;
      }
    }
  },
  getDataType(val, type) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(new Date(val));
    if (type == "y") {
      return dateObj.year + "";
    } else if (type == "m") {
      return dateObj.month + "";
    } else if (type == "d") {
      return dateObj.day + "";
    } else if (type == "h") {
      return dateObj.hour;
    } else if (type == "mm") {
      return dateObj.minute;
    }
    // let yyyy_MM_dd_hh_mm = dateObj.year + '.' + dateObj.month + '.' + dateObj.day + ' ' + dateObj.hour + ':' + dateObj.minute;
    // return yyyy_MM_dd_hh_mm;
  },
  //获得当月有多少天
  getMonthDay(year, month) {
    var day = new Date(year, month, 0);
    return day.getDate();
  },
  //获得当前天数是星期几-店铺业绩专用
  getWeekDayForShop(value) {
    let date = new Date(value);
    let week = date.getDay();
    if (week == 6) {
      return 0;
    }
    if (week == 0) {
      return 1;
    }
    return week + 1;
  },
  //获得当前天数是星期几,1-7
  getWeekDay(value) {
    let date = new Date(value);
    let week = date.getDay();
    if (week == 0) {
      return 7;
    }
    return week;
  },
  //获取明天日期yyyy_MM_dd
  getDateTomorrow(type) {
    let nowdate = new Date();
    nowdate.setTime(nowdate.getTime() + 24 * 60 * 60 * 1000 * type);
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(nowdate);
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
  getBirAge(age) {
    let nowdate = new Date();
    let dateObj = this.currentDate(nowdate);
    dateObj.year = dateObj.year - Number(age);
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;

    return yyyy_MM_dd;
  },
  //获取明天日期yyyy_MM_dd
  getDateAfter() {
    let nowdate = new Date();
    nowdate.setTime(nowdate.getTime() - 24 * 60 * 60 * 1000);
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(nowdate);
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
  //获取相加N天的日期yyyy_MM_dd
  getDateAfterOfDay(dateParam, dayNum) {
    // 获取dateParam时间格式的时间戳
    let timestamp = Date.parse(new Date(dateParam)) / 1000;
    let beforeDate = new Date();
    beforeDate.setTime(timestamp * 1000);
    //重设加减天数
    // dayNum = 0 + dayNum;
    dayNum = parseInt(dayNum);
    beforeDate.setDate(beforeDate.getDate() + dayNum);
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(beforeDate);
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
  /**
   * subYear  减去的年份
   */
  getDateyMdSubtract(subYear) {
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(new Date());
    let curr_year = (dateObj.year - subYear) + '';
    let yyyy_MM_dd = curr_year + "-" + dateObj.month + "-" + dateObj.day;
    return yyyy_MM_dd;
  },
  /**
   * 时间 / 时间戳 转yyyyMMdd 
   * val  Date
   * subYear  减去的年份
   */
  //JS日期系列：根据出生日期 得到周岁年龄                 
  //参数strBirthday已经是正确格式的2007-02-09这样的日期字符串  
  //后续再增加相关的如日期判断等JS关于日期处理的相关方法  
  getAge(strBirthday) {
    let returnAge;
    let strBirthdayArr = strBirthday.split("-");
    let birthYear = parseInt(strBirthdayArr[0]);
    let birthMonth = parseInt(strBirthdayArr[1]);
    let birthDay = parseInt(strBirthdayArr[2]);
    let d = new Date();
    let nowYear = d.getFullYear();
    let nowMonth = d.getMonth() + 1;
    let nowDay = d.getDate();

    if (nowYear == birthYear) {
      returnAge = 0; //同年 则为0岁  
    } else {
      let ageDiff = nowYear - birthYear; //年之差  
      if (ageDiff > 0) {
        if (nowMonth == birthMonth) {
          let dayDiff = nowDay - birthDay; //日之差  
          if (dayDiff < 0) {
            returnAge = ageDiff - 1;
          } else {
            returnAge = ageDiff;
          }
        } else {
          let monthDiff = nowMonth - birthMonth; //月之差  
          if (monthDiff < 0) {
            returnAge = ageDiff - 1;
          } else {
            returnAge = ageDiff;
          }
        }
      } else {
        returnAge = -1; //返回-1 表示出生日期输入错误 晚于今天  
      }
    }
    return returnAge; //返回周岁年龄  
  },
  /**
   * 获得当前时间{}
   */
  currentDate(dateObject) {
    let currentDate = {
      year: 0,
      month: 0,
      day: 0,
      hour: 0,
      minute: 0,
      second: 0
    };
    currentDate.year = dateObject.getFullYear();
    currentDate.month = dateObject.getMonth() + 1;
    currentDate.day = dateObject.getDate();
    currentDate.hour = dateObject.getHours();
    currentDate.minute = dateObject.getMinutes();
    currentDate.second = dateObject.getSeconds();
    currentDate.month = String(currentDate.month).length < 2 ? '0' + currentDate.month : currentDate.month;
    currentDate.day = String(currentDate.day).length < 2 ? '0' + currentDate.day : currentDate.day;
    currentDate.hour = String(currentDate.hour).length < 2 ? '0' + currentDate.hour : currentDate.hour;
    currentDate.minute = String(currentDate.minute).length < 2 ? '0' + currentDate.minute : currentDate.minute;
    currentDate.second = String(currentDate.second).length < 2 ? '0' + currentDate.second : currentDate.second;
    return currentDate;
  },
  //获得时间组成的数字
  getDataTimeRam() {
    let currentDate = new Date();
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth() + 1;
    let day = currentDate.getDate();
    let hour = currentDate.getHours();
    let minute = currentDate.getMinutes();
    let second = currentDate.getSeconds();
    month = (String(month).length < 2 ? '0' + month : month) + "";
    day = (String(day).length < 2 ? '0' + day : day) + "";
    hour = (String(hour).length < 2 ? '0' + hour : hour) + "";
    minute = (String(minute).length < 2 ? '0' + minute : minute) + '';
    second = (String(second).length < 2 ? '0' + second : second) + '';
    let Num = "";
    for (let i = 0; i < 5; i++) {
      Num += Math.floor(Math.random() * 10);
    }
    let dataString = year + month + day + hour + minute + second + Num;
    return dataString;

  },
  jsGetAge(strBirthday) {
    let returnAge;
    let strBirthdayArr = strBirthday.split("-");
    let birthYear = strBirthdayArr[0];
    let birthMonth = strBirthdayArr[1];
    let birthDay = strBirthdayArr[2];

    let d = new Date();
    let nowYear = d.getFullYear();
    let nowMonth = d.getMonth() + 1;
    let nowDay = d.getDate();

    if (nowYear == birthYear) {
      returnAge = 0; //同年 则为0岁  
    } else {
      let ageDiff = nowYear - birthYear; //年之差  
      if (ageDiff > 0) {
        if (nowMonth == birthMonth) {
          let dayDiff = nowDay - birthDay; //日之差  
          if (dayDiff < 0) {
            returnAge = ageDiff - 1;
          } else {
            returnAge = ageDiff;
          }
        } else {
          let monthDiff = nowMonth - birthMonth; //月之差  
          if (monthDiff < 0) {
            returnAge = ageDiff - 1;
          } else {
            returnAge = ageDiff;
          }
        }
      } else {
        returnAge = -1; //返回-1 表示出生日期输入错误 晚于今天  
      }
    }
    return returnAge; //返回周岁年龄  
  },
  //计算出相差天数,个是必须是yyyy-mm-dd或者yyyy/mm/dd
  DateDiff(sDate1, sDate2) {
    // debugger
    let days;
    try {
      sDate1 = sDate1.replace("-", "/").replace("-", "/").replace("-", "/");
      sDate2 = sDate2.replace("-", "/").replace("-", "/").replace("-", "/");
      //在new Date("yyyy/mm/dd")时，在IOS上日期格式使用/分割，最好不加时分秒
      let time1 = new Date(sDate1).getTime();
      let time2 = new Date(sDate2).getTime();
      //时间差的毫秒数
      var dayAbs = Math.abs(time2 - time1) + 1;
      //计算出相差天数
      days = Math.floor(dayAbs / (24 * 3600 * 1000));
      // if (days == 364) {
      //   days = 365;
      // }
    } catch (e) {

    }
    return days;
  },
  //计算出相差天数,个是必须是yyyy-mm-dd或者yyyy/mm/dd
  getDateInsure(strBirthday) {
    let strBirthdayArr = strBirthday.split("-");
    let birthYear = parseInt(strBirthdayArr[0]) + 1;
    let birthMonth = strBirthdayArr[1];
    let birthDay = strBirthdayArr[2];
    let dataString = birthYear + "-" + birthMonth + "-" + birthDay;
    let nowdate = new Date(dataString);
    nowdate.setTime(nowdate.getTime() - 24 * 60 * 60 * 1000);
    //获取时间的年月日时分秒对象
    let dateObj = this.currentDate(nowdate);
    let yyyy_MM_dd = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
    return yyyy_MM_dd;
  },
}
