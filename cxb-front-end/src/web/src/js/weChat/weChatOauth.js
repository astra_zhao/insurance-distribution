import { RequestUrl } from 'src/common/url';
import { RouteUrl, Mutations, WE_CHAT, Url_Key } from 'src/common/const';
export default {
    /**
     * 微信授权重定向接口返回信息处理
     */
    weChatOauth(_this, isGoUrl_MY, oauthInfo) {
        let ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) != 'micromessenger') {
            return;
        }
        if (!_this.isWeAuth) {
            return;
        }
        let url = window.location.href;
        let uuid = sessionStorage["SESSION_SHARE_ID"];

        let shareid = "henghua";
        if (_this.$common.isNotEmpty(uuid) && uuid.length > 10 && uuid.indexOf("http") == -1) {
            shareid = uuid;
        } else {
            let shareUuid = _this.$common.getShareUrlKey(url, Url_Key.SHARE_UUID);
            if (_this.$common.isNotEmpty(shareUuid)) {
                shareid = shareUuid;
            }
        }

        if (_this.saveInsure.refereeData != false && _this.saveInsure.refShareUUid != "") {
            shareid = _this.saveInsure.refShareUUid;
        }
        //授权类型或用户code信息
        if (_this.$common.isNotEmpty(oauthInfo)) {
            shareid = shareid + "," + oauthInfo;
        }
        // let pageUrl = location.href.split('#')[0].toString();
        //openId有效或者已登录
        if (url.indexOf("openid") > 0) {
            //微信用户ID
            //let openId = parameterArr[1];
            let openId = _this.$common.getShareUrlKey(url, "openid");
            // let openId = pageUrl.split('?')[1].split("=")[1];
            let initData = { TYPE: "FORM", openId: openId };
            //获取微信账号
            _this.$http.post(RequestUrl.FIND_USER_BY_OPEN_ID, initData).then(function (res) {
                if (res.state == 0) {
                    //保存USER_INFO信息
                    _this.$common.storeCommit(_this, Mutations.SAVE_USER_INFO, res.result);
                    if (isGoUrl_MY) {
                        _this.$common.goUrl(_this, RouteUrl.MY);
                    }

                    //判断是否有抽奖信息
                    _this.$common.storeCommit(_this, Mutations.IS_SHOW_RED, _this);
                    return;
                }
                if (res.result.indexOf("不存在") >= 0) {
                    this.sendRequest(shareid);
                }
            })
            return;
        }
        //未登录&不是微信系统重定向
        this.sendRequest(shareid);
    },
    //授权请求
    sendRequest(params) {
        let url = RequestUrl.WECHAT_OAUTH2_URI
            + "?appid=" + WE_CHAT.ID
            + "&redirect_uri=" + RequestUrl.WECHAT_OAUTH2_REDIRECT_URI + "?url=" + RequestUrl.WECHAT_OAUTH2_AFTER_URL
            + "&response_type=code&scope=snsapi_userinfo"
            + "&state=" + params + "#wechat_redirect";
        window.location.href = url;
    }
}