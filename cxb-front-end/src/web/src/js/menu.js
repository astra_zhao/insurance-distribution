import { PopState, FooterIndex, TO_TYPE, Mutations, FooterShow, RouteUrl, BaseService, Url_Key } from 'src/common/const';
import Validation from "src/common/util/validation";
export default {
    goMenu(data) {
        // debugger
        let _this = data._this;
        let menu = data.menu;
        _this.$store.commit(Mutations.SET_MENU, menu);
        if (menu.toType == TO_TYPE.INSIDE_JUMP) {
            if (menu.baseService == BaseService.POLICY || menu.baseService == BaseService.INVOICE || menu.baseService == BaseService.FNOL || menu.baseService == BaseService.CLAIM_QUERY || menu.baseService == BaseService.CLAIM_UPLOAD || menu.baseService == BaseService.PAY) {

                let userDto = _this.user.userDto;
                if (_this.user.userDto != null && !_this.$common.isEmpty(userDto.idNumber) && Validation.isIdNumber(userDto.idNumber)) {
                    // if (_this.user.userDto != null && !_this.$common.isEmpty(userDto.idNumber)) {
                    //查询保单并保持数据状态
                    // let userCode = userDto.userCode;
                    let dataQuery = { _this: _this, id: userDto.idNumber, imgCode: null, verCode: null, mobileNo: userDto.mobile, type: true, baseService: menu.baseService, userCode: userDto.userCode }
                    _this.$common.storeCommit(_this, Mutations.QUERY_POLICYS_SERVICE, dataQuery);
                    // if (menu.baseService == BaseService.PAY) {
                    //     _this.$common.storeCommit(_this, Mutations.QUERY_POLICYS_SERVICE, dataQuery);
                    //     _this.$common.storeCommit(_this, Mutations.GET_PAY_DATA, _this);
                    //     return;
                    // }
                    // else if (menu.baseService == BaseService.FNOL || menu.baseService == BaseService.POLICY || menu.baseService == BaseService.INVOICE) {
                    //     _this.$common.storeCommit(_this, Mutations.QUERY_POLICYS_SERVICE, dataQuery);
                    // } else if (menu.baseService == BaseService.CLAIM_QUERY || menu.baseService == BaseService.CLAIM_UPLOAD) {
                    //     _this.$common.storeCommit(_this, Mutations.QUERY_POLICYS_SERVICE, dataQuery);
                    //     _this.$common.storeCommit(_this, Mutations.QUERY_REPORTS_SERVICE, dataQuery);
                    // }
                } else {
                    _this.$common.goUrl(_this, menu.serviceUrl);
                }
            } else {
                _this.$common.storeCommit(_this, Mutations.SET_IS_GET_INDEX, false);
                _this.$common.goUrl(_this, menu.serviceUrl);
            }
        } else if (menu.toType == TO_TYPE.EXTERNAL_JUMP) {
            let iframe = {
                isIframe: true,
                iframeUrl: menu.serviceUrl,
                iframeName: menu.serviceCname,
                _this: data._this
            };
            _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.FALSE);
            _this.$store.commit(Mutations.SET_IFRAME_DATA, iframe);
            _this.$common.goUrl(_this, RouteUrl.IFRAME);
        } else if (menu.toType == TO_TYPE.PHONE_NUMBER) {
            window.location.href = "tel:" + menu.serviceUrl;
        } else if (menu.toType == TO_TYPE.INT_RENDER_DISPLAY) {
            let activity = {
                activityDiv: menu.detail,
                activityName: menu.serviceCname,
            }
            // _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.FALSE);
            _this.$store.commit(Mutations.AD_ACTIVITY, activity);
            _this.$common.goUrl(_this, RouteUrl.AD_ACTIVITY);
        } else if (menu.toType == TO_TYPE.PRODUCT) {
            let parmList = [];
            // let urlParm = {
            //   key: Url_Key.SHARE_UUID,
            //   value: this.shareUuid
            // }
            let urlPro = {
                key: Url_Key.PRODUCT_ID,
                value: menu.productId
            }
            parmList.push(urlPro);
            // parmList.push(urlParm);
            //车险地址跳转
            if (typeof (_this.seleValue.productList[menu.productId]) != "undefined") {
                let proData = _this.seleValue.productList[menu.productId];
                if (proData.isCar == "1") {
                    _this.$common.goUrl(_this, RouteUrl.CAR_INSURE, _this.$common.setShareUrl(parmList));
                    return;
                }
            }

            // let shareUrl = RequestUrl.BASE_URL + RouteUrl.DETAILS + this.$common.setShareUrl(parmList);
            _this.$common.storeCommit(_this, Mutations.SET_IS_GET_INDEX, false);
            //组合URI并跳转
            _this.$common.goUrl(_this, RouteUrl.DETAILS, _this.$common.setShareUrl(parmList));



            // let urlParm = {
            //     isLogin: data.isLogin,
            //     id: menu.productId,
            //     shareId: data.shareId
            // };

            // _this.$common.storeCommit(_this, Mutations.SET_IS_GET_INDEX, false);
            // let addUrl = _this.$common.getShareUrl(_this, urlParm);
            // //组合URI并跳转
            // _this.$common.goUrl(_this, RouteUrl.DETAILS, addUrl);
        }
    }
}